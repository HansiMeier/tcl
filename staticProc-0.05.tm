#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval StaticVarsUsedInStaticProcs {
   variable Counter -1
   proc GenSym {} {
      return ::StaticVarsUsedInStaticProcs::[incr ::StaticVarsUsedInStaticProcs::Counter 1]
   }
}

proc staticProc {name arguments staticVarDict body} {
   set codeToPrepend {}
   dict for {var val} $staticVarDict {
      set tmpVar [::StaticVarsUsedInStaticProcs::GenSym]
      set $tmpVar $val
      append codeToPrepend "upvar 0 {$tmpVar} {$var}\n"
   }
   set body ${codeToPrepend}${body}
   tailcall proc $name $arguments $body
}

