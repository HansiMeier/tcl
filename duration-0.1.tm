namespace eval duration {

    variable HoursInDay 24
    variable SecondsInHour 3600
    variable SecondsInMinute 60
    variable SecondsInDay [expr {$SecondsInHour * $HoursInDay}]

    proc toHourMinuteSecondsDescription {seconds} {
        variable SecondsInHour
        variable SecondsInMinute
        variable SecondsInDay

        set wholeDays [expr {$seconds / $SecondsInDay}]
        set wholeHours [expr {($seconds % $SecondsInDay) / $SecondsInHour}]
        set wholeMinutes [expr ($seconds % $SecondsInHour) / $SecondsInMinute]
        set wholeSeconds [expr {$seconds % $SecondsInMinute}]

        if {$wholeDays == 0} {
            set dayString ""
        } else {
            set dayString "${wholeDays} days"
        }

        if {$wholeHours == 0} {
            set hourString ""
        } else {
            set hourString "${wholeHours} h"
        }
        if {$wholeMinutes == 0} {
            set minuteString ""
        } else {
            set minuteString "${wholeMinutes} min"
        }
        if {$wholeSeconds == 0} {
            set secondsString ""
        } else {
            set secondsString "${wholeSeconds} s"
        }

        set description [string trim [join [list $dayString $hourString $minuteString $secondsString] " "]]
        return $description
    }

    proc parseDuration {duration {now -1}} {
        variable SecondsInHour
        variable SecondsInMinute
        variable SecondsInDay
        
        set duration [string tolower [string trim $duration]]
        set regex {^([0-9]+(\.[0-9]+)?)(s|sec|secs|m|min|mins|h|hr|hrs|d|day|days)?}    
        if {[string first : $duration] != -1 || ![regexp $regex $duration wholeMatch numberValue _ unit]} {
            if {$now < 0} {
                set now [clock seconds]
            }

            try {
                set targetTime [clock scan $duration -base $now]
                puts "Target time $targetTime"
            } on error err {
                throw {PARSE_DURATION_FAILURE} "Did not understand duration $duration. Legal values are '10' '5s' '3m' '8.4min' '1h' or an absolute time like '19:40'"
            }
            
            set durationInSeconds [clock add $targetTime -$now seconds]
            if {$durationInSeconds < 0} {
                set targetTime [clock add $targetTime 1 day]
                set durationInSeconds [clock add $targetTime -$now seconds]
            }

            return $durationInSeconds
        }

        if {$unit == ""} {
            set unit s
        }

        switch $unit {
            s -
            secs -
            sec {
                set inSeconds 1
            }
            m -
            mins -
            min {
                set inSeconds $SecondsInMinute
            }
            h -
            hr - 
            hrs {
                set inSeconds $SecondsInHour
            }
            d -
            days -
            day {
                set inSeconds $SecondsInDay
            }
        }

        set durationInSeconds [expr {entier(round($numberValue * $inSeconds))}]
        set remainingString [string range $duration [string length $wholeMatch] end]
        if {[string length $remainingString] > 0} {
            incr durationInSeconds [parseDuration $remainingString]
        }

        return $durationInSeconds
    }

    namespace export {[a-z]*}
}