#!/usr/bin/env tclsh

package require Tcl 8.6-

# Load the dynamic library providing the extension
namespace eval loadLibraryTmp {

    variable sharedLib {}
    variable name sysinfo.tclext[info sharedlibextension]
    variable searchIn [linsert $::env(TCLLIBPATH) 0 [file dirname [info script]]]

    foreach dir $::loadLibraryTmp::searchIn {
        if {[file exists [file join $dir $::loadLibraryTmp::name]]} {
            set ::loadLibraryTmp::sharedLib [file join $dir $::loadLibraryTmp::name]
            break
        }
    }

    if {[string equal $::loadLibraryTmp::sharedLib {}]} {
        throw {NO SHARED LIB} "The library \"$::loadLibraryTmp::name\" has not been found in \"$::loadLibraryTmp::searchIn\"!"
    }

    uplevel #0 [list load $::loadLibraryTmp::sharedLib]
}
namespace delete loadLibraryTmp

namespace eval sysinfo {

    namespace export {[a-z]*}
}

