#!/usr/bin/env tclsh

package require Tcl 8.6- ;# for %b
regexp {^([[:alpha:]]+)-} [file tail [info script]] _ namespaceName

namespace eval $namespaceName {
####################################################################################################
#                                             GET DNF                                              #
####################################################################################################
   
   variable inputMapping {
      + ||
      . &&
      * &&
      - !
      \{ (
      \} )
   }

   proc genAssignmentList numOfVars {
      set assignments {}
      set upper [expr {round(pow(2, $numOfVars))}]

      for {set i 0} {$i != $upper} {incr i 1} {
         lappend assignments [split [format %0${numOfVars}lb $i] {}]
      }

      return $assignments
   }

   proc getMinTerm {vars assignment} {
      set literals {}
      foreach var $vars val $assignment {
         if {$val} {
            lappend literals $var
         } else {
            lappend literals -$var
         }
      }
      return $literals
   }

   proc getDNF formula {
      variable inputMapping
      # Use C-style boolean operators
      set formula [string map $inputMapping $formula]

      # get all used variables
      set vars [lsort -unique [split [regsub -all -- {[^[:alpha:]]} $formula {}] {}]]

      # include sigils
      set replMap {}
      foreach var $vars {
         lappend replMap $var
         lappend replMap \$$var
      }
      set formula [string map $replMap $formula]

      # Make a truth table
      set trueMinTerms {}
      foreach assignment [genAssignmentList [llength $vars]] {
         lassign $assignment {*}$vars
         if $formula {
            lappend trueMinTerms [getMinTerm $vars $assignment]
         }
      }

      return $trueMinTerms
   }

   namespace export getDNF
}

unset namespaceName

