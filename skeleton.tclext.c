#include <tcl.h>

#if __STDC_VERSION__ < 199901L
#error "Requires C99-compliant compiler!"
#endif

#define startErrorMsgFile(msg, filename) \
    Tcl_Obj* errorMessage = Tcl_NewStringObj(msg, sizeof(msg)); \
    Tcl_AppendToObj(errorMessage, filename, strlen(filename)); \
    Tcl_SetObjResult(interp, errorMessage); \
    return TCL_ERROR;

,[comment uses lispy::` syntax to evaluate tcl code -- > see INIT]

/**************************************************************************************************
 *                                              TCL                                               *
 **************************************************************************************************/

int exampleCmd(
    ClientData cdata,
    Tcl_Interp* interp,
    int objc,
    Tcl_Obj* const objv[]) {

    // Exactly one argument needed (objv[0] is the command name)
    if (objc != 2) {
        Tcl_WrongNumArgs(interp, 1, objv, "number");
        return TCL_ERROR;
    }

    // Get the number out of the argument;
    Tcl_WideInt n;
    if (Tcl_GetWideIntFromObj(interp, objv[1], &n) == TCL_ERROR) {
        return TCL_ERROR;
    }

    // Calc
    Tcl_Obj* probablySomething = Tcl_NewBooleanObj(foobar(n));

    // Return
    Tcl_SetObjResult(interp, probablySomething);
    return TCL_OK;
}

/**************************************************************************************************
 *                                              INIT                                              *
 **************************************************************************************************/



int ,[string totitle ,$extName]_Init(Tcl_Interp* interp) {

    #ifdef USE_TCL_STUBS
    if (Tcl_InitStubs(interp, "8.1" , 0) == 0L) {
        return TCL_ERROR;
    }
    #endif

    // Create the namespace

#define NAMESPACE ",$extName"
    Tcl_Namespace* nsPtr;
    nsPtr = Tcl_CreateNamespace(interp, NAMESPACE, NULL, NULL);
    if (!nsPtr) {
        return TCL_ERROR;
    }

    // Declare commands
#define declareCommand(command) \
    Tcl_CreateObjCommand(interp, NAMESPACE "::" #command, &(command ## Cmd), NULL, NULL);

    declareCommand(example);

    // Export all commands
    if (Tcl_Export(interp, nsPtr, "*", 0) == TCL_ERROR) {
        return TCL_ERROR;
    }

    return TCL_OK;
}

// #undefs
,[
foreach macro {NAMESPACE declareCommand startErrorMsgFile} {
    lappend undefs "#undef ,$macro"
}
join ,$undefs ,\n
]
