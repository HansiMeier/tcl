#!/usr/bin/env tclsh

package require Tcl 8.6-

namespace eval numberRange {

   # Read the range given in the rangeString
   # all numbers have to be nonnegative integers
   # commas mean "and" as in 3, 5
   # dashes mean "all numbers between and including"
   # -> 3-5 == 3, 4, 5
   # returns a list of all numbers recognized or an
   # empty list if there were errors
   proc readRange rangeString {
      # strip whitespace
      regsub -all -- {[[:space:]]} $rangeString "" rangeString
      # Are there invalid characters?
      if {[regexp {[^0-9,\-]} $rangeString]} {
         # There are --> Syntax error
         return [list]
      }

      set toProcess [split $rangeString ,]

      set numbers [list]
      foreach thing $toProcess {
         # Count the dashes to find out if $thing's a plain number
         set bounds [split $thing -]

         switch -exact -- [llength $bounds] {
            1 {
               # $thing is a plain number
               lappend numbers $thing
            }

            2 {
               # $thing is a range
               lassign $bounds lower upper

               # $lower <= $upper is intentionally not checked to allow for empty ranges
               if {! ([string is entier $lower] && [string is entier $upper])} {
                  return [list]
               }

               for {set i $lower} {$i <= $upper} {incr i 1} {
                  lappend numbers $i
               }
            }

            default {
               # Syntax error, fail silently
               return [list]
            }
         }
      }

      return $numbers
   }

   namespace export {[a-z]*}
}

