#!/usr/local/bin/tclsh

set thisScript [file normalize [info script]]
if {[file type $thisScript] eq "link"} {
    set thisScript [file readlink $thisScript]
}
set thisScriptDir [file dirname $thisScript]

::tcl::tm::path add $thisScriptDir

package require Tk
package require duration
namespace import duration::*

# hide root window shown by default
wm withdraw .

if {$argc == 0} {
    set timeoutInMinutes 5
    set timeoutInSeconds [expr {$timeoutInMinutes * 60}]
} else {
    set timeoutInSeconds [parseDuration [lindex $argv 0]]
}
set timeoutInMilliseconds [expr {$timeoutInSeconds * 1000}]

set updateTimeInSeconds 10
set updateTimeInMilliseconds [expr {$updateTimeInSeconds * 1000}]

set gracePeriodInMinutes 1

set now [clock seconds]
set shutdownTime [clock add $now $timeoutInSeconds seconds]

proc printTimeLeft {timeLeftInSeconds} {
    return [toHourMinuteSecondsDescription $timeLeftInSeconds]
}

# Set up the timeout; I got the widget name by reading the sources
set handle [after $timeoutInMilliseconds .__tk__messagebox.yes invoke]

set messageFormatString "Shutdown in %s at [clock format $shutdownTime]."

for {set delayInMilliseconds $updateTimeInMilliseconds} {$delayInMilliseconds < $timeoutInMilliseconds} {incr delayInMilliseconds $updateTimeInMilliseconds} {
    lappend delayHandles [after $delayInMilliseconds .__tk__messagebox.msg configure -text [list [format $messageFormatString [printTimeLeft [expr {int(($timeoutInMilliseconds - $delayInMilliseconds)/1000)}]]]]]
}

# This is the internal name of the implementation
set answer [tk::MessageBox -type yesno -message [format $messageFormatString [printTimeLeft $timeoutInSeconds]]]

# Make sure we clear our timeout in case the user *did* pick something
after cancel $handle
foreach delayHandle $delayHandles {
    after cancel $delayHandle
}

if {$answer != "yes"} {
    puts "Not shutting down"
    exit 1
}

puts "Shutting down"
if {![string equal $tcl_platform(os) Darwin]} {
    puts "Platform $tcl_platform(os) not supported"
    exit 2
}

# soft shutdown
exec -ignorestderr -- /usr/bin/osascript -e {tell app "System Events" to shut down} &

# hard shutdown after grace period
exec -ignorestderr -- sudo shutdown -h +$gracePeriodInMinutes &
