#!/usr/local/bin/tclsh

package require Tcl 8.6- ;# for dicts and tailcall
package require httpFollow
package require fileutil ;# for tempfile
package require httpExtra ;# por the url encoding

set URL {http://smbc-comics.com}
set STRIPFILE [::fileutil::tempfile].png

proc fatalError {msg txt} {
   puts stderr $txt
   exit 1
}
interp alias {} signaliseError {} throw


proc getRealStripUrl htmlVar {
   global URL
   upvar $htmlVar html
   set year [clock format [clock seconds] -format %Y]
   set exp [subst -nocommands -nobackslashes {src="([^"]*${year}[^\.]*\.png)"}]
   if {! [regexp -- $exp $html _ realAddress]} {
      fatalError {REGEXP STRIP FAILED} "Looking for the strip location of today's strip failed!\nMaybe it doesn't exist yet."
   }
   puts stderr "Retrieving smbc strip location \"$realAddress\""
   return [::httpExtra::percentEncodeUrl $realAddress]

}

proc openWith file {
   set openProg {}
   foreach prog [list xdg-open open start] {
      set openProg [auto_execok $prog]
      if {$openProg ne ""} {
         break
      }
   }
   if {$openProg eq ""} {
      throw {no openprog} "No opening program found!"
   } elseif {[string match -nocase *start* $openProg]} {
      lappend $openProg {""}
   }
   puts "Opening \"$file\" with \"$openProg\""
   exec {*}$openProg $file &
}


proc downloadToFile {url file} {
   if {[catch {set token [::http::geturl $url -follow 10]}]} {
      signaliseError {GET_FAILED} "Error retrieving \"$url\"!"
   }

   if {[::http::ncode $token] != 200} {
      puts stderr "Failed with HTTP-Code \"[::http::ncode $token]\""
      ::http::cleanup $token
      exit 2
   }


   set html [::http::data $token]
   set realUrl [getRealStripUrl html]
   ::http::cleanup $token

   if {[catch {set token [::http::geturl $realUrl]}]} {
    fatalError {GET_FAILED} "Error retrieving \"$realUrl\"!"
   }

   puts stderr "Downloading \"$realUrl\" to \"$file\""
   set fd [open $file w]
   chan configure $fd -translation binary
   puts -nonewline $fd [::http::data $token]
   close $fd
   ::http::cleanup $token
}

proc main {url file} {
   downloadToFile $url $file
   openWith $file
}

if {!$::tcl_interactive} {
    main $URL $STRIPFILE
}

