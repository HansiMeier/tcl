#!/usr/bin/env tclsh

if {! [regexp {^([[:alpha:]]+)-} [file tail [info script]] _ namespaceName]} {
   throw {NO NAMESPACENAME} "Could not get namespace name out of \"[info script]\".\nIs the format right?"
}

namespace eval $namespaceName {
   proc getDaysInMonth {year month} {
      if {$month == 2} {
         return [expr {28 + [isLeapYear $year]}]
      }

      return [expr {31 - (($month - 1) % 7 % 2)}]
   }

   proc isLeapYear year {
      return [expr {!($year % 400) || !($year % 4) && $year % 100 }]
   }

   # Make sure the number string in every var
   # is interpreted as decimal variable
   proc asDecimal args {
      foreach var $args {
         upvar 1 $var dec
         scan $dec %d dec
      }
      
      return
   }


   # PRE: day, month, year are integers
   proc isValidDate {year month day} {
      return [
         expr {
            1 <= $month && $month <= 12 &&
            1 <= $day   && $day   <= [getDaysInMonth $year $month]
         }
      ]
   }

   proc readISODate {date {yearOutVar ""} {monthOutVar ""} {dayOutVar ""} {sep1OutVar ""} {sep2OutVar ""}} {
      set datePattern {([0-9]+)([^0-9]*)([0-9]{2})([^0-9]*)([0-9]{2})}

      if {![regexp $datePattern $date _ year sep1 month sep2 day]} {
         return 0
      }

      # Prevent TCL from recognizing octals
      asDecimal year month day

      set result [isValidDate $year $month $day]
      if {$result} {
         # Write to the outvars if requested
         foreach var {year month day sep1 sep2} {
            if {[set ${var}OutVar] ne ""} {
               upvar 1 [set ${var}OutVar] write
               set write [set $var]
            }
         }
      }

      return $result
   }
      
         
   namespace export readISODate
}

unset namespaceName

