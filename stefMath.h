/**************************************************************************************************
 *															 STEFMATH							                       *
 **************************************************************************************************/

#ifndef STEFMATH
#define STEFMATH

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h> // for uint64_t
#include <inttypes.h> // for the printing specifiers
#include <stdio.h>
#include <tgmath.h>

typedef intmax_t inum;
typedef uintmax_t unum;
typedef double rnum;

#ifdef __cplusplus
extern "C" {
#endif

// modular exponentiation by squaring
unum expmod(unum base, unum exponent, unum mod);

// binomial coefficient
unum binCoeff(unum n, unum k);

// falling factorial
unum fallingFact(unum n, unum k);

// factorial
unum factorial(unum n);

// prime test by miller rabin
bool millerRabinPrime(const unum n);

// Farey-sequence finding the best rational appro, ximant
// needs pointer to array of two unums, the numerator and denominator
// of the best rational approximant
// the denominantor is always positive: -0.25 -> {-1, 4}
void bestRationalApproximant(rnum toApproximate, const unum maximalDenominator, const rnum relativeTolerance, inum* result);

#ifdef __cplusplus
}
#endif
#endif

