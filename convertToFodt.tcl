#!/usr/bin/env tclsh

set files $argv
set allowedExtensions {.doc .docx .odt}
set transliterate false
foreach file $files {
    set file [file normalize $file]
    set extension [string tolower [file extension $file]]
    if {[lsearch $allowedExtensions $extension] == -1} {
        puts stderr "$file does not have compatible extension $allowedExtensions, skipping"
        continue
    }

    if {$transliterate} {
        set basename [file tail $file]
        set transliteratedBasename [exec iconv -f UTF-8 -t ASCII//TRANSLIT << $basename]
        set transliteratedFileName [file normalize [file join [file dirname $file] $transliteratedBasename]]
        if {![string equal $file $transliteratedFileName]} {
            puts stderr "Renaming $file to $transliteratedFileName"
            file copy $file $transliteratedFileName
        }
    } else {
        set transliteratedFileName $file
    }

    exec -ignorestderr -- soffice --headless --convert-to fodt $transliteratedFileName

    set trashProg [auto_execok trash]
    if {[file executable $trashProg]} {
        foreach toDelete [list $file $transliteratedFileName] {
            if {[file exist $toDelete]} {
                exec -ignorestderr -- $trashProg $toDelete
            }
        }
    }
}