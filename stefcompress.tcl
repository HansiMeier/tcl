#!/usr/bin/env tclsh

package require Tcl 8.5-

# a new compressor $c (for instance $c = "zip" or $c = "tar") consists of
# - an entry in either tarCompressorDict or otherCompressorDict
# - a proc named "$c" which takes the objects to compress (as list, every object exists and is given as absolute path) and
#   the archive name (also absolute path and writable) and does the compression
# - a proc named "${c}AdditionalCheck" which takes no arguments and performs additional checking, like
#   looking for a certain version of the compressor. It is already checked that the compressor given
#   in the dict exists. The proc shall return 1 if the test succeeded and 0 otherwise.

####################################################################################################
#                                            DEBUGGING                                             #
####################################################################################################

if {[info exists ::env(STEFCOMPRESS_DEBUG)]} {
    set debuggingThis $::env(STEFCOMPRESS_DEBUG)
} else {
    set debuggingThis 0
}

proc debugMessage args {
    global debuggingThis
    if {$debuggingThis} {
        puts stderr {*}$args
    }
}

####################################################################################################
#                                         GLOBAL VARIABLES                                         #
####################################################################################################

# Compression programs and extensions
# Syntax of an entry: key   = compression program(s) to be used in descending order of preference
#                     value = extensions for that format (no initial dot)
# If the key begins with a dash '-', it is considered to be a commandline switch to another program
# Example:
# {pxz xz --xz} {txz tar.xz} in tarCompressorDict means that for the extensions txz and tar.xz
# pxz should be used if present. If it doesn't exist xz should be used instead
# and finally the --xz switch to GNU tar if xz doesn't exist either.

# tar-based
set tarCompressorDict [
dict create \
    tar tar \
    --lzip {tar.lz tlz} \
    {pixz pxz --xz} {txz tar.xz} \
    {pbzip2 --bzip2} {tbz tbz2 tar.bz2} \
    {pigz --gzip} {tgz tar.gz} \
    lz4 tar.lz4 \
]

set env(LANG) C
set env(LC_ALL) C

# other
set otherCompressorDict [
dict create \
    {7z zip} zip \
    {7z} {7z ppmd.7z} \
    rar rar \
]

# virtual compressors without accompanying exe
set virtualCompressorDict [
dict create \
    cbz cbz \
]

# Default options for tar
set additionalTarOptions {--no-auto-compress --ignore-failed-read
                          --dereference --hard-dereference
                          --warning=no-ignore-archive --format=posix}

set defaultExt zip ;# The default extension if none was given at the commandline


####################################################################################################
#                                        UTILITY FUNCTIONS                                         #
####################################################################################################

# similar to the commandline 'which' command
# but only returns a boolean if $prog is in the $PATH
# (and executable)
proc isInPATH prog {
    return [expr {[auto_execok $prog] ne ""}]
}

# display help
proc displayUsage programName {
    global defaultExt

    puts stderr "$programName: compresses files and directories into an archive."
    puts stderr "Usage: $programName <file 1 / directory 1> \[file 2 / directory 2\] … <archive>"
    puts stderr "If archive doesn't have an extension, \"$defaultExt\" is chosen."
    puts stderr "This program saves only the basename, not the full path, e.g."
    puts stderr "/home/user/testFile is saved as testFile."
    puts stderr "Directories are saved with their contents (recursively)."

    return
}

# drop-in replacement for throw
proc fatalError {msg text} {
    puts stderr $text
    exit 1
}

# get extension of a file
# roll my own function because
# [file extension archive.tar.gz] returns ".gz"
# whereas I want "tar.gz" (no dot at the beginning)!
proc getExt file {
    set ext {}
    regexp {\.(.+)$} [file tail $file] _ ext ;# get extension
    return $ext
}

# replace every element with its quoted version:
# abc -> \"abc\"
proc inplaceQuoteElems list {
    set listLength [llength $list]
    for {set i 0} {$i != $listLength} {incr i 1} {
        set quotedElem \"[lindex $list $i]\"
        set list [lreplace $list[set list {}] $i $i $quotedElem]
    }

    return $list
}

# dummy proc used by the check procs
interp alias {} noAdditionalCheck {} return -level 0 1

####################################################################################################
#                                         TAR COMPRESSORS                                          #
####################################################################################################

# the main proc for tar-based archives
proc tarCompress {compressor objectsToCompress archive} {
    global additionalTarOptions

    # Check if user even wants any compression
    if {[string equal -nocase $compressor tar]} {
        # Make a plain tar archive
        set compressorArg {}
    } elseif {[string equal [string index $compressor 0] -]} {
        # compressor is a commandline switch -> can use that
        set compressorArg $compressor
    } else {
        # compressor is an external program
        set compressorArg --use-compress-program=$compressor
    }


    set objList {}
    foreach object $objectsToCompress {

        if {[string first \n $object] != -1} {
            puts stderr "\"$object\" contains a newline, which I can't handle, skipping!"
            continue
        }
        set dirname [file dirname $object]
        set basename [file tail $object]

        # Temporarily cd to the directory
        lappend objList "-C \"$dirname\""
        lappend objList $basename
    }


    # Pass files to compress via stdin to avoid having too large argument lists
    exec tar {*}$additionalTarOptions \
        {*}$compressorArg \
        --recursion \
        --create --file=$archive \
        --files-from=- \
        << [join $objList \n]

    return
}

# check that GNU tar is available
proc baseTarCheck {{minimumVersion 0} {compressionName ""}} {
    if {! [isInPATH tar]} {
        fatalError {NO TAR} "\"tar\" not found!"
    }

    set versionText [exec tar --version]
    set firstFewChars [string range $versionText 0 100]
    set firstLine [lindex [split $firstFewChars \n] 0]


    if {! [regexp -- {GNU tar} $firstLine]} {
        puts stderr "\"tar\" found, but non-GNU version!"
        return 0
    }

    if {! [regexp -- {[0-9]\.[0-9]+$} $firstLine versionNumber]} {
        puts stderr "GNU \"tar\" found, but no version number!"
        puts stderr "Version text was \"$firstLine\""
        return 0
    }

    if {[package vsatisfies $versionNumber 1.26-1.26]} {
        puts stderr "GNU \"tar\" found, but version 1.26 which has a bug!"
        return 0
    }

    if {![package vsatisfies $versionNumber 1.14-]} {
        puts stderr "GNU \"tar\" found, but older than version 1.14!"
        puts stderr "This script needs 1.14 to use posix format archives."
        puts stderr "Version text was \"$firstLine\""
        return 0
    }

    if {![package vsatisfies $versionNumber ${minimumVersion}-]} {
        puts stderr "GNU \"tar\" version $versionNumber found, but it's older than version $minimumVersion!"
        puts stderr "This script needs at least version $minimumVersion to use $compressionName compression."
        puts stderr "Version text was \"$firstLine\""
        return 0
    }

    return 1
}

# Make the aliases for the tar-based compressors
# No version checking
dict for {compressorList _} $tarCompressorDict {
    foreach compressor $compressorList {
        interp alias {} $compressor {} tarCompress $compressor
        interp alias {} ${compressor}AdditionalCheck {} baseTarCheck
    }
}

# Only check the version if using the --xz compressor
interp alias {} --xzAdditionalCheck {} baseTarCheck 1.22 xz

# and for lzip compression too
interp alias {} --lzipAdditionalCheck {} baseTarCheck 1.22 lzip


####################################################################################################
#                                        OTHER COMPRESSORS                                         #
####################################################################################################

# ZIP
# This script uses INFO-ZIP syntax
proc zipAdditionalCheck {} {
    set versionText [exec zip --version]
    set firstFewChars [string range $versionText 0 60]

    if {! [regexp -- Info-ZIP $firstFewChars]} {
        puts stderr "\"zip\" found, but not Info-ZIP version!"
        puts stderr "Version text was \"$firstFewChars\"."
        return 0
    }

    return 1
}

proc zip {objectsToCompress archive} {
    set oldWD [pwd]

    foreach object $objectsToCompress {
        cd [file dirname $object]
        # --grow: add [file tail $object] to $archive
        #  works efficiently because the zip format doesn't use solid compression.
        exec zip --recurse-paths --grow $archive [file tail $object]
    }

    cd $oldWD
    return
}

# 7ZIP
proc 7zCompress {7zProg objectsToCompress archive} {
    set ext [getExt $archive] ;# Also handle zip files

    # -l means dereference symlinks
    # -t7z means 7z format
    # -tzip means zip format
    # -mmt=on means enable multithreading
    # -ssc means case sensitive
    # -m0=$method chooses the compression method
    # -mx=$level chooses the compression level
    # -ms=on means use solid compression
    # a means add mode
    if {$ext eq "7z"} {
        set level 9
        set solidCompressionFlag -ms=on
        set numberOfFastBytesFlag -mfb=273
        set dictionarySizeFlag -md=128m
    } else {
        set level 6
        set solidCompressionFlag {}
        set numberOfFastBytesFlag {}
        set dictionarySizeFlag {}
    }
    exec ${7zProg} a -l -t$ext -ssc -mmt=on $solidCompressionFlag $dictionarySizeFlag $numberOfFastBytesFlag -mx=${level} $archive {*}$objectsToCompress
}

foreach 7zProg {7za 7z} {
    interp alias {} ${7zProg} {} 7zCompress ${7zProg}

    # No special checks for 7z
    interp alias {} ${7zProg}AdditionalCheck {} noAdditionalCheck
}

# RAR

proc rar {objectsToCompress archive} {
    # a --> add to archive
    # -ep1 --> flatten paths
    # -r --> recursive
    # -rr10 --> up to 10% of the file can be garbage and the contents can still be read.
    # @ --> read input files from stdin
    #
    exec rar a -ep1 -r -rr10 $archive @ > /dev/null << [join $objectsToCompress \n]
    return
}

proc rarAdditionalCheck {} {
    # Check for version >= 4 as it enables file lists
    set versionText [exec rar]
    set firstFewChars [string range $versionText 0 100]
    if {![regexp -line -- {^[[:space:]]*RAR ([0-9]{1,2}\.[0-9]{1,3})} $firstFewChars _ version]} {
        puts stderr "Could not determine version of \"rar\" program."
        puts stderr "Version text was \"$firstFewChars\"."
        return 0
    }

    if {$version < 4.00} {
        puts stderr "This script needs RAR version >= 4!"
        puts stderr "Version text was \"$firstFewChars\"."
        return 0
    }

    return 1
}

# CBZ
proc cbz {objectsToCompress archive} {
    # CBZ is a renamed zip
    set fileNameWithZip "[file root $archive].zip"
    zip $objectsToCompress $fileNameWithZip
    file rename $fileNameWithZip $archive
}

# No special checks for cbz
interp alias {} cbzAdditionalCheck {} noAdditionalCheck

####################################################################################################
#                                         MAIN PROCESSING                                          #
####################################################################################################

foreach check [info commands --*AdditionalCheck] {
    set oldName ${check}_old
    regexp -- {[^\-]+(?=AdditionalCheck)} $check prog
    rename $check $oldName

    proc $check {} [
    subst -nocommands {
        if {![isInPATH {$prog}]} {
            puts stderr "$prog is needed but not found in \\\$PATH!"
            return 0
        }
        return [{$oldName}]
    }
    ]
}

# determine the compressor proc to use by looking at the extension of the archive
proc getCompressor archiveExt {
    global tarCompressorDict
    global otherCompressorDict
    global virtualCompressorDict
    global defaultExt

    if {! [string length $archiveExt]} {
        set archiveExt $defaultExt
    }

    dict for {compressors extensions} [dict merge $tarCompressorDict $otherCompressorDict $virtualCompressorDict] {
        foreach ext $extensions {
            # Look for a matching extension
            if {[string equal $archiveExt $ext]} {
                # Look for a usable compressor
                foreach compressor $compressors {
                    if {([string equal [string index $compressor 0] -] || [dict exists $virtualCompressorDict $compressor] || [isInPATH $compressor]) \
                    && [${compressor}AdditionalCheck]} {
                        puts stdout "Extension \"$ext\" known.\nUsing compression program \"$compressor\"."
                        return $compressor
                    }
                }
                fatalError {NO COMPR} "No suitable compression program in {$compressors} found for extension \"$archiveExt\"!\nPlease install one."
            }
        }
    }

    # Check for wrongly interpreted extensions like
    # archive.hi.ho.rar --> hi.ho.rar is not a known extension
    # so try ho.rar as "chopped" extension --> still no good
    # finally try rar --> success!
    if {[string first . $archiveExt] != -1} {
        # try again with chopped extension
        puts stdout "Extension \"$archiveExt\" unknown!"
        regsub {[^\.]*\.} $archiveExt {} archiveExt
        puts stdout "Trying chopped extension \"$archiveExt\"."
        return [getCompressor $archiveExt]
    }

    # nothing more to chop
    fatalError {UNKNOWN EXT} "Extension \"$archiveExt\" unknown and nothing to chop!"
}


# Clean up the objects to compress:
# Remove the nonexisting paths from them
# and make sure that the paths are absolute
proc normalizeObjectsToCompress objectsToCompress {
    set normalizedObjects {}
    foreach object $objectsToCompress {
        set absObject [file normalize $object] ;# make path absolute

        if {! [file readable $absObject]} {
            fatalError {NOT READABLE} "\"$absObject\" is not readable but should be added to the archive!"
        }

        lappend normalizedObjects $absObject
    }

    return $normalizedObjects
}

# Clean up the archive:
# Make sure it has an extension and is an absolute path
proc normalizeArchive archive {
    global defaultExt

    set absArchive [file normalize $archive]
    set dirname [file dirname $absArchive]
    set ext [getExt $absArchive]

    if {$ext eq ""} {
        puts stdout "No extension detected, using default \"$defaultExt\""
        set absArchive ${absArchive}.${defaultExt}
        set ext $defaultExt
    }

    regexp {^([^\.]*)\.} [file tail $absArchive] _ rootname

    if {! [file writable $dirname]} {
        fatalError {DIR NOT WRITABLE} "Directory \"$dirname\" not writable!"
    }

    set renamed false
    while {[file exists $absArchive]} {
        append rootname '
        set absArchive [file join $dirname ${rootname}.${ext}]
        set renamed true
    }

    if {$renamed} {
        puts stdout "Renamed archive to \"${rootname}.${ext}\" because of name conflicts"
    }

    return $absArchive
}


proc main {argc argv0 argv} {
    if {$argc < 2 || "-h" in $argv || "--help" in $argv} {
        displayUsage [file tail $argv0]
        exit 1
    }

    set filesAndDirs [list]

    foreach arg [lrange $argv 0 end-1] {
        if {$arg eq "-"} {
            set arg @/dev/stdin
        }

        if {[string first @ $arg] == 0} {
            set resposeFileName [string range $arg 1 end]
            set fd [open $resposeFileName r]
            while {1} {
                set line [gets $fd]
                if {[eof $fd]} {
                    close $fd
                    break
                }
                if {![string is space $line]} {
                    lappend filesAndDirs [string trim $line]
                }
            }
        } else {
            lappend filesAndDirs $arg
        }
    }

    set objectsToCompress [normalizeObjectsToCompress $filesAndDirs]
    set archive [normalizeArchive [lindex $argv end]]

    puts stdout "Compressing [join [inplaceQuoteElems $objectsToCompress] ", "] to \"$archive\""
    [getCompressor [getExt $archive]] $objectsToCompress $archive
    puts stdout "\"$archive\" created."
    return
}

main $::argc $::argv0 $::argv
