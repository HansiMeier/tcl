#!/usr/bin/env tclsh

set ERRORLEVEL 0

package require Tcl 8.5

set romanNumeralValues [
   dict create \
      I 1 \
      V 5 \
      X 10 \
      L 50 \
      C 100 \
      D 500 \
      M 1000
]


proc romanCharValue letter {
   global romanNumeralValues
   dict for {roman arabic} $romanNumeralValues {
      if {$letter eq $roman} {
         return $arabic
      }
   }
   return {}
}

proc romanToArabic roman {
   set numerals [split $roman ""]
   set previousArabic [romanCharValue [lindex $numerals 0]]
   set sum 0
   foreach numeral [lrange $numerals 1 end] {
      set arabic [romanCharValue $numeral]
      if {$previousArabic < $arabic} {
         incr sum [expr {$previousArabic * -1}]
         incr sum $arabic
         set previousArabic 0
      } else {
         incr sum $previousArabic
         set previousArabic $arabic
      }
   }
   incr sum $previousArabic

   return $sum
}


proc main {} {
   global argc
   global argv0
   global argv
   global ERRORLEVEL
   if {$argc == 0} {
      puts stderr "Usage: $argv0 <roman numeral> \[roman numeral 2] ..."
      exit 1
   }
   foreach numberStr $argv {
      set res [romanToArabic [string toupper $numberStr]]
      if {$res} {
         puts $res
      }
   }
   exit $ERRORLEVEL
}

main

