#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval pathManip {

    proc map {commandPrefix path} {
        set mappedComponents [list]
        foreach component [file split $path] {
            lappend mappedComponents [uplevel 1 [list {*}$commandPrefix $component]]
        }

        tailcall file join {*}$mappedComponents
    }
    namespace export {[a-z]*}
}


