#!/usr/bin/env tclsh

package require parseGnuOpts

set packagesToLoad [list fileutil httpFollow]
set namespacesToImport [list fileutil]
set debuggingThis off

set elementVars [list _ e]

proc debugPuts args {
    global debuggingThis
    if {$debuggingThis} {
        puts {*}$args
    }
}

proc substituteBracesForNumber {string number} {
    return [string map [list \{\} $number] $string]
}

proc padNumber {number numberOfPlaces} {
    return [format "%0${numberOfPlaces}d" $number]
}

proc startsWithZero {string} {
    return [expr {[string length $string] > 0 && [string index $string 0] eq "0"}]
}


proc numberOfPlaces {number} {
    return [string length $number]
}

proc fatalError msg {
    puts stderr "ERROR: $msg"
    exit 2
}

proc loadPackagesForEvalTclString {} {
    global packagesToLoad namespacesToImport
    foreach package $packagesToLoad {
        package require $package
    }
    foreach namespace $namespacesToImport {
        namespace import ${namespace}::*
    }
}

proc quoteForShell string {
    if {[string first $string '] >= 0} {
        fatalError "Can't handle single quotes in argument \"$string\"!"
    }

    return "'$string'"
}

proc main {argc argv0 argv} {
    global GEN elementVars

    set optDict [dict create \
        checkForExistenceOpt [dict create \
            shortOpt c \
            longOpt check \
            numberOfArgs 0 \
            required 0 \
            usage "Check if generated strings exist in filesystem" \
        ] \
        beginRangeOpt [dict create \
            shortOpt b \
            longOpt begin \
            numberOfArgs 1 \
            required 1 \
            usage "begin of range" \
        ] \
        endRangeOpt [dict create \
            shortOpt e \
            longOpt end \
            numberOfArgs 1 \
            required 1 \
            usage "end of range (included)" \
        ] \
        tclOpt [dict create \
            shortOpt t \
            longOpt tcl \
            numberOfArgs 1 \
            required 0 \
            usage "tcl command to use the generated list in. \$GEN is the generated list, [join [lmap var $elementVars {return -level 0 "\$$var"}] ", "] are elements of \$GEN which are looped over" \
        ] \
        thingsToRepeatOpt [dict create \
            shortOpt "" \
            longOpt "" \
            numberOfArgs -1 \
            required 1 \
            usage "things to repeat" \
            argsDescription "things to repeat" \
        ]\
    ]

    set generalUsage "[file tail $argv0]: prints repeated things, substituting '{}' for numbers"

    ::parseGnuOpts::processArgs \
        $argv0 \
        $argv \
        $argc \
        $optDict \
        $generalUsage
    # Import the option arrays into the current namespace
    ::parseGnuOpts::importOptArrays

    set doCheckForExistence $checkForExistenceOpt(present)





    set beginRange $beginRangeOpt(passedArgs)
    set endRange $endRangeOpt(passedArgs)
    set thingsToRepeat $thingsToRepeatOpt(passedArgs)

    foreach var {beginRange endRange} {
        set value [set $var]
        if {![string is entier $value]} {
            fatalError "$var (= $value) is not an integer!"
        }
    }

    if {([startsWithZero $beginRange] && ([string length $beginRange] > 1))|| [startsWithZero $endRange]} {
        set padding [expr {max([numberOfPlaces $endRange], [numberOfPlaces $beginRange])}]
    } else {
        set padding 0
    }

    set errorLevel 0
    set substitutes [list]
    for {set number $beginRange} {$number <= $endRange} {incr number 1} {
        set paddedNumber [padNumber $number $padding]
        foreach thing $thingsToRepeat {
            set substitute [substituteBracesForNumber $thing $paddedNumber]
            if {$doCheckForExistence && ![file exists $substitute]} {
                puts stderr "\"$substitute\" does not exist on filesystem"
                set errorLevel 5
            }
            lappend substitutes $substitute
        }
    }


    if {$errorLevel == 0} {
        if {$tclOpt(present)} {
            set GEN $substitutes
            loadPackagesForEvalTclString
            set command [lindex $tclOpt(passedArgs) 0]
            foreach variable $elementVars {
                if {[string first "\$${variable}"  $command] >= 0} {
                    set command [subst -nocommands {foreach $variable \$GEN { $command }}]
                    break
                }
            }
            debugPuts stderr "Executing command \"$command\""
            uplevel #0 $command

        } else {
            puts stdout [join [lmap string $substitutes {quoteForShell $string}] " "]
        }
    }
    exit $errorLevel
}

if {[info script] eq $argv0} {
    main $argc $argv0 $argv
}
