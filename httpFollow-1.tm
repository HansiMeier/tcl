#!/usr/bin/env tclsh

package require Tcl 8.6-
package require http
package require uri
package require tls

if {[info commands ::http::theRealGeturl] eq {}} {
   http::register https 443 [list ::tls::socket -autoservername true]
   rename ::http::geturl ::http::theRealGeturl
   # adds a new -follow parameter to ::http::geturl which indicates how many times to follow redirects
   proc ::http::geturl {url args} {
      set len [llength $args]
      set rounds 1
      set rest {}

      # Scrape the args for our new option, -follow
      for {set i 0} {$i != $len} {incr i 1} {
         set arg [lindex $args $i]
         if {$arg eq "-follow"} {
            incr i 1
            set rounds [expr {[lindex $args $i] + 1}]
            if {!(([string is integer $rounds] && $rounds > 0) || ([string is double $rounds] && $rounds == inf))} {
               throw {invalid follow} "\"[lindex $args $i]\" is an invalid value for -follow, it has to be a integer > 0 or inf!"
            }
         } else {
            lappend rest $arg
         }
      }

      for {set i 0} {$i != $rounds} {incr i 1} {
         set token [::http::theRealGeturl $url {*}$rest]

         set metaDict [dict create {*}[set ${token}(meta)]]
         # a single round means 'do not follow redirects'
         if {!($rounds != 1 \
               && [string match {30[1237]} [::http::ncode $token]] \
               && [dict exists $metaDict Location])} {
            # no redirect or no location header
            # or no following
            return $token
         }

         # redirect and Location available
         # make the url to follow absolute and resolve . or .. (canonicalize)
         set url [::uri::canonicalize [::uri::resolve $url [dict get $metaDict Location]]]
         ::http::cleanup $token
      }

      return $token
   }
}

