#!/usr/bin/env tclsh

####################################################################################################
#                                              MATLAB                                              #
####################################################################################################

# Package for interfacing Tcl with MATLAB via MATLAB's engine API
# Usage:
# - Create an engine
# ::matlab::engine create eng
#
# - Evaluate MATLAB code
# eng eval {plot([1 2 3])}
#
# - Evaluate an m-file
# eng m /path/plotSomething.m
# 
# - Send a matrix description list to MATLAB
# - accessible with the name A in MATLAB
# eng put A {{1 3} 1 2 3}
# - A is a 1x3 row vector with the components 1 2 3
# - A = [1 2 3]
#
# - Get a MATLAB variable A as a matrix description list
# - currently all entries of A are converted to doubles.
# eng get A
# - -> returns {{1 3} 1.0 2.0 3.0}
#
# See the source code for additional procedures especially for
# working with matrix description lists

package require Tcl 8.6-
package require TclOO


# check for /bin/csh on Unix
if {$::tcl_platform(platform) eq "unix" && ![file executable /bin/csh]} {
   throw {no csh} \
      "No executable /bin/csh found, which the MATLAB API needs on unix!\nPlease install a C shell in that location!"
}


namespace eval ::matlab {
   ::oo::class create engine {
      constructor {} {
         variable number [::matlab::OpenEngine]
         return
      }

      destructor {
         variable number
         tailcall ::matlab::CloseEngine $number
      }

      method eval cmd {
         variable number
         tailcall ::matlab::EvalString $number $cmd
      }

      method m mfile {
         set mfile [file normalize $mfile]
         if {![file readable $mfile]} {
            throw {not readable} "M-File \"$mfile\" isn't readable!"
         }
         tailcall my eval run('$mfile')
      }



      method put {name val} {
         variable number
         set len [llength $val]
         switch -- $len {
            0 {
               set val {{0 0}}
            }
            1 {
               set val [list [list $len 1] $val]
            }
         }
         tailcall ::matlab::PutMatrixDescriptionList $number $name $val
      }

      method get name {
         variable number
         tailcall ::matlab::GetMatrixDescriptionList $number $name
      }
   }

   # Number of rows of an MDL
   proc rows mdl {
      return [lindex [lindex $mdl 0] 0]
   }

   # Number of columns of an MDL
   proc cols mdl {
      return [lindex [lindex $mdl 0] 1]
   }

   # Number of entries of an MDL
   proc size mdl {
      return [expr {[rows $mdl] * [cols $mdl]}]
   }

   # Raw list of entries of an MDL
   proc entries mdl {
      return [lrange $mdl 1 end]
   }

   # Check that the dimensions given in an MDL are correct
   proc check mdl {
      return [expr {[size $mdl] == [llength [entries $mdl]]}]
   }

   # Prettyprint matrix description lists
   proc print mdl {
      set entries [entries $mdl]
      set rows [rows $mdl]
      set colsExceptLast [expr {[cols $mdl]-1}]
      set linearPos 0
      set pretty {}
      for {set i 0} {$i != $rows} {incr i 1} {
         for {set j 0} {$j != $colsExceptLast} {incr j 1; incr linearPos 1} {
            append pretty "[lindex $entries $linearPos] "
         }
         append pretty "[lindex $entries $linearPos]\n"
      }

      return $pretty
   }

   namespace export {[a-z]*}
}

# Load the dynamic library providing the extension
namespace eval loadLibraryTmp {

   variable sharedLib {}
   variable name matlab.tclext[info sharedlibextension]
   variable searchIn [linsert $::env(TCLLIBPATH) 0 [file dirname [info script]]]

   foreach dir $::loadLibraryTmp::searchIn {
      if {[file exists [file join $dir $::loadLibraryTmp::name]]} {
         set ::loadLibraryTmp::sharedLib [file join $dir $::loadLibraryTmp::name]
         break
      }
   }

   if {[string equal $::loadLibraryTmp::sharedLib {}]} {
      throw {NO SHARED LIB} "The library \"$::loadLibraryTmp::name\" has not been found in \"$::loadLibraryTmp::searchIn\"!"
   }

   uplevel #0 load $::loadLibraryTmp::sharedLib
}
namespace delete loadLibraryTmp
