#!/usr/bin/env tclsh

proc removeByValue {list elem} {
   set idx [lsearch $list $elem]
   if {$idx == -1} {
      return $list
   }
   return [lreplace $list $idx $idx]
}

# PRE: List ordered ascending
proc lexPerms chars {
   if {[llength $chars] <= 1} {
      return $chars
   }
   set results {}
   foreach c $chars {
      foreach result [lexPerms [removeByValue $chars $c]] {
         lappend results $c$result
      }
   }
   return $results
}



