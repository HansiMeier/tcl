#!/usr/bin/env tclsh

package require Tcl 8.4

namespace eval resolveFileLinks {
    proc resolveFileLinks {filePath} {
        set normalizedPath [file normalize $filePath]
        if {[string equal [file type $normalizedPath] link]} {
            return [file readlink $normalizedPath]
        } else {
            return $normalizedPath
        }
    }
    namespace export {[a-z]*}
}
