#!/usr/bin/env tclsh

package require Tcl 8.6- ;# for tailcall
namespace import ::tcl::mathop::*

namespace eval tclpp {}

set CR \x0d
set LF \x0a



####################################################################################################
#                            USEFUL GLOBAL CONSTANTS FOR USE IN SCRIPTS                            #
####################################################################################################

namespace eval loadConstants {
   proc dateAndTime {} {
      global unixTime
      set unixTime [clock seconds]
      foreach formatString {%Y %m %d %u %H %M %S} variableName {year month day weekday hour minute second} {
         global $variableName
         set $variableName [clock format $unixTime -format $formatString]
      }
   }

   namespace export {[a-z]*}
   namespace ensemble create
}

####################################################################################################
#                                        USEFUL MACRO PROCS                                        #
####################################################################################################

namespace eval macroProcs {

   namespace eval internal {
      variable currentLoopBodyResults
   }

   # get nice quoting mechanisms
   proc s args {
      tailcall subst {*}$args
   }

   # The wonderful identity
   proc id args {
      tailcall join $args " "
   }

   # like #define (the non arguments version)
   proc define {toDefine definition} {
      interp alias {} $toDefine {} string trim $definition
      return $definition
   }
   interp alias {} txt {} define

   # like #include
   # include
   proc include file {
      set fp [open $file r]
      set contents [chan read $fp]
      chan close $fp
      return $contents
   }

   proc numberLines lines {
      set len [llength $lines]
      if {$len == 0} {
         return $lines
      }

      set padNum [expr {1 + entier(log10($len))}]
      set numberedLines [list]
      set lineNo 0
      set formatStr %0${padNum}d
      foreach line $lines {
         incr lineNo 1
         set numberedLine "[format $formatStr $lineNo] $line"
         lappend numberedLines $numberedLine
      }

      return $numberedLine
   }

   proc collectLoopBodyResults args {
      set internal::currentLoopBodyResults [list]
      set body [lindex $args end]
      set loopCommand [lrange $args 0 end-1]
      uplevel 1 [list {*}$loopCommand [subst -nocommands {
         lappend ::macroProcs::internal::currentLoopBodyResults [eval {$body}]
      }]]

      return $internal::currentLoopBodyResults
   }

   namespace export {[a-z]*}
}
namespace import ::macroProcs::*


####################################################################################################
#                                          UTILITY PROCS                                           #
####################################################################################################

proc fatalError {msg txt} {
   puts stderr $txt
   exit 1
}

proc output str {
   tailcall puts -nonewline stdout $str
}

proc lreplaceInplace {listVar from to args} {
   upvar 1 $listVar list
   set list [lreplace $list $from $to {*}$args[set list {}]]
   return $list
}

####################################################################################################
#                                          PREPROCESSING                                           #
####################################################################################################

namespace eval tclpp {

   variable SilentBeginSymbol {§}
   variable SilentEndSymbol {§}
   variable BeginSymbol {°}
   variable EndSymbol {°}
   variable VariableSymbol {µ}
   variable SymbolVariables [list SilentBeginSymbol SilentEndSymbol BeginSymbol EndSymbol VariableSymbol]
   variable DebugOn 0
   variable stdIndent "    "

   proc setNewSymbols {normal silent variable} {
      ImportSymbolVariables

      if {$normal eq $silent && $normal ne ""} {
         fatalError normalissilent "The new silent symbol and the normal symbol are equal"
      } elseif {$normal eq $variable && $normal ne ""} {
         fatalError normalisvariable "The new variable symbol and the normal symbol are equal"
      } elseif {$silent eq $variable && $silent ne ""} {
         fatalError variableissilent "The new silent symbol and the variable symbol are equal"
      }


      if {$silent ne ""} {
         set NewSilentBeginSymbol $silent
         set NewSilentEndSymbol $silent
      }

      if {$normal ne ""} {
         set NewBeginSymbol $normal
         set NewEndSymbol $normal
      }

      if {$variable ne ""} {
         set NewVariableSymbol $variable
      }

   }

   proc ApplyNewSymbols {} {
      ImportSymbolVariables
      foreach variable $::tclpp::SymbolVariables {
         if {[info exists New${variable}]} {
            set $variable [set New${variable}]
            unset New${variable}
         }
      }
   }


   proc setNewNormalSymbol {normal} {
      tailcall setNewSymbols $normal {} {}
   }

   proc setNewSilentSymbol {silent} {
      tailcall setNewSymbols {} $silent {}
   }

   proc setNewVariableSymbol {variable} {
      tailcall setNewSymbols {} {} $variable
   }


   proc ImportSymbolVariables {} {
      foreach var $::tclpp::SymbolVariables {
         uplevel 1 [list variable $var]
         uplevel 1 [list variable New${var}]
      }
      return
   }

   proc GlobalSymbolVariables {} {
      foreach var $::tclpp::SymbolVariables {
         uplevel 1 upvar #0 ::tclpp::${var} ::${var}
      }
   }

   proc DebugProc {name arguments body} {
       if {!$::tclpp::DebugOn} {
           set body {}
       }

       proc $name $arguments $body
   }


   DebugProc DebugVal args {
      foreach var $args {
         upvar 1 $var local
         puts stderr "$var = `$local'"
      }
      return
   }

   proc IsAtBeginningOfLine {string index} {
      # The first char is always at the beginning of a line
      if {$index == 0} {
         return 1
      }

      set charBefore [string index $string [- $index 1]]
      if {$charBefore eq $::LF} {
         # using that instead of \n because \n is CRLF on windows which is two chars
         return 1
      }

      if {[string is space $charBefore]} {
         tailcall IsAtBeginningOfLine $string [- $index 1]
      }

      return 0
   }

   proc FirstBeginSymbolWithIndex {string {startingIndex 0}} {
      set stringLength [string length $string]
      set bestFindIndex $stringLength
      set foundSymbol {}
      foreach symbol [list $::tclpp::SilentBeginSymbol $::tclpp::BeginSymbol $::tclpp::VariableSymbol] {
         set stringToSearchIn [string range $string $startingIndex $bestFindIndex]
         set currentFind [string first $symbol $stringToSearchIn]

         if {$currentFind != -1 && $currentFind < $bestFindIndex} {
            set bestFindIndex $currentFind
            set foundSymbol $symbol
         }
      }
      if {$bestFindIndex == $stringLength} {
         return [list -1 $foundSymbol]
      }
      return [list [expr {$bestFindIndex + $startingIndex}] $foundSymbol]
   }

   proc EndSymbolIndex {string endSymbol {startingIndex 0}} {
      set result [string first $endSymbol $string $startingIndex]
      if {$result == -1} {
         fatalError {noendsym} "Error beginning on line [+ 1 [getLineNo $string $startingIndex]]: End symbol \"$endSymbol\" not found!"
      }
      return $result
   }

   proc GetEndSymbol sym {
      ImportSymbolVariables
      if {$BeginSymbol eq $sym} {
         return $EndSymbol
      } elseif {$SilentBeginSymbol eq $sym} {
         return $SilentEndSymbol
      }
      fatalError {noendsym} "No matching end symbol for symbol \"$sym\"!"
   }

   proc preprocess src {
      ImportSymbolVariables
      set srcLen [string length $src]
      DebugVal src

      set preprocessed ""

      for {set pos 0} {$pos < $srcLen} {} {
         DebugVal pos
         lassign [FirstBeginSymbolWithIndex $src $pos] beginInd beginSym
         DebugVal beginInd
         DebugVal beginSym
         set beginSymLen [string length $beginSym]
         # DebugVal beginInd
         if {$beginInd == -1} {
            # nothing more to preprocess
            append preprocessed [string range $src $pos end]
            DebugVal preprocessed
            return $preprocessed
         }

         append preprocessed [string range $src $pos [- $beginInd 1]]

         # Handle variable substitution
         if {$beginSym eq $VariableSymbol} {

            if {[regexp -indices -start [+ 1 $beginInd] -- {[^[:alpha:]]} $src nextSpaceInd]} {
               set nextSpaceInd [lindex $nextSpaceInd 0]
            } else {
               set nextSpaceInd $srcLen
            }

            DebugVal nextSpaceInd
            # DebugVal nextSpaceInd

            set varName [string range $src [+ $beginInd $beginSymLen] [- $nextSpaceInd 1]]
            # DebugVal varName
            upvar #0 $varName local
            if {! [info exists local]} {
               fatalError nolocal "Error on line [+ 1 [getLineNo $src $beginInd]]: Variable \"$varName\" doesn't exist!"
            }

            set toAppend $local
            DebugVal local
            set endInd [- $nextSpaceInd 1]
         } else {

            set endSym [GetEndSymbol $beginSym]
            set endSymLen [string length $endSym]
            set endInd [EndSymbolIndex $src $endSym [+ $beginInd $beginSymLen]]
            # DebugVal endInd

            set toEval [string range $src [+ $beginInd $beginSymLen] [- $endInd 1]]
            DebugVal toEval
            try {
               set evald [uplevel #0 $toEval]
            } on error errMsg {
               fatalError badeval "Error on lines [+ 1 [getLineNo $src $beginInd]] to [+ 1 [getLineNo $src $endInd]]: Evaluating the code block produced the error:\n$errMsg\n$::errorInfo"
            }
            # DebugVal evald

            incr endInd [- $endSymLen 1]
            if {$beginSym eq $BeginSymbol} {
               set toAppend $evald
            } else {
               # Silent Symbol
               set toAppend ""
               # Skip possible newline after the end silent symbol
               if {[string index $src [+ $endInd 1]] eq "\n"} {
                  incr endInd 1
               }
            }
            unset evald
         }

         # Indent if on beginning of line
         if {$beginSym ne $SilentBeginSymbol && [IsAtBeginningOfLine $src $beginInd]} {
            set split [split $toAppend \n]
            # DebugVal split
            set outdented [outdent $split]
            # DebugVal outdented
            set currentLine [getLine $src $beginInd]
            # DebugVal currentLine
            set indent [getIndent $currentLine]
            # DebugVal indent
            set indented [indent $outdented $indent]
            DebugVal indented
            set firstLineOutdented {*}[outdent [list [lindex $indented 0]]]
            lreplaceInplace indented 0 0 $firstLineOutdented
            set toAppend [join $indented \n]
            DebugVal toAppend

            # DebugVal toAppend
         }

         append preprocessed $toAppend
         set pos [+ $endInd 1]
         ApplyNewSymbols
      }

      return $preprocessed
   }


####################################################################################################
#                                         INDENTING PROCS                                          #
####################################################################################################

   proc indent {codeLines indent} {
      set codeLength [llength $codeLines]
      for {set i 0} {$i != $codeLength} {incr i 1} {
         set line [lindex $codeLines $i]
         lreplaceInplace codeLines $i $i $indent$line
      }

      return $codeLines
   }

   proc getLineNo {string index} {
      set lines [split $string \n]
      set lineNo 0
      set charCount 0
      set newLineLen [string length \n]
      foreach line $lines {
         incr charCount [+ $newLineLen [string length $line]]
         if {$charCount > $index} {
            break
         }
         incr lineNo 1
      }

      return $lineNo
   }

   proc getLine {string index} {
      if {$index == 0} {
         set indexBefore 0
      }

      set indexBefore [- $index 1]
      while {$indexBefore >= 0 && [string index $string $indexBefore] ne $::LF} {
         incr indexBefore -1
      }
      incr indexBefore 1

      set stringLen [string length $string]
      set indexAfter [+ $index 1]
      while {$indexAfter < $stringLen && [string index $string $indexAfter] ni [list $::CR $::LF]} {
         incr indexAfter 1
      }
      incr indexAfter -1

      tailcall string range $string $indexBefore $indexAfter
   }


   proc getIndent line {
      regexp {^([[:space:]]*)([^[:space:]])?} $line _ whiteSpace firstNonSpace
      # Special case for lines with only whitespace as they can be outdented easily
      if {$firstNonSpace eq ""} {
         return emptyLine
      }

      return $whiteSpace
   }

   proc getIndentLength indent {
      set indent [string map "\\t {$::tclpp::stdIndent}" $indent]
      tailcall string length $indent
   }


   # Outdent lines
   proc outdent codeLines {
   # only works if the indenting is either just tabs or just spaces!

      # Look for minimum indent
      set minIndentLength Inf
      set minIndent ""
      foreach line $codeLines {
         set indent [getIndent $line]

         # Lines with only whitespace are ignored
         if {$indent eq "emptyLine"} {
            continue
         }

         set indentLength [getIndentLength $indent]
         if {$indentLength < $minIndentLength} {
            set minIndentLength $indentLength
            set minIndent $indent
         }
      }

      # Outdent all lines by $minIndent
      set codeLength [llength $codeLines]
      for {set i 0} {$i != $codeLength} {incr i 1} {
         set line [lindex $codeLines $i]
         set outdentedLine [regsub "^$minIndent" $line {}]
         lreplaceInplace codeLines $i $i $outdentedLine
      }

      return $codeLines
   }

} ;# namespace tclpp ends here

####################################################################################################
#                                               MAIN                                               #
####################################################################################################

proc IsTclpps file {
   return [string equal -nocase .tclpps [file extension $file]]
}

proc AmCalledAsTclpps argv0 {
    return [string equal -nocase tclpps $argv0]
}

proc main {} {
   global argc argv0 argv

   if {[lindex $argv 0] in {-h --help}} {
      puts stderr "Usage: $argv0 \[infile\] \[outfile\] \[arguments to pass to script\]"
      puts stderr "Preprocesses infile (or stdin if - or nothing was given) and outputs the result to stdout or outfile"
      exit 1
   }

   upvar #0 ::file file


   set listToAppend argvForThis
   set argvForThis [list]
   set argvForScript [list]
   foreach arg $argv {
      if {$arg eq "--" && $listToAppend ne "argvForScript"} {
         set listToAppend argvForScript
         continue
      }
      lappend $listToAppend $arg
   }

   set argcForThis [llength $argvForThis]
   set argcForScript [llength $argvForScript]


   set file [lindex $argvForThis 0]
   if {$file ni {"" "-"}} {
      if {! [file readable $file]} {
         fatalError {FILE NOT READABLE} "Input file \"$file\" isn't readable!"
      }
      set fp [open $file r]
   } else {
      set fp stdin
      set file /dev/stdin
   }

   set argv0ForThis $argv0
   set argv0ForScript $file

   if {$argcForThis > 2} {
      fatalError {WRONG NUM ARGUMENTS} "This script itself needs at most two parameters, infile and outfile, but you have passed $argcForThis.\nHave you forgot a '--' to pass arguments to the script?"
   }



   set outfp stdout
   if {$argcForThis == 2} {
      set outfile [lindex $argvForThis 1]
      set outfileSet 1
      if {$outfile ne "-"} {
         set outfp [open $outfile w]
      }
   }

   ::tclpp::DebugVal argvForThis
   ::tclpp::DebugVal argvForScript


   # Replace argv argv0 argc as if the file was called directly
   set argv $argvForScript
   set argv0 $argv0ForScript
   set argc $argcForScript


   set src [chan read $fp]
   namespace import ::tclpp::outdent
   try {
      set result [::tclpp::preprocess $src]
   } on error err {
      puts stderr $err
      chan close $fp ;# No problem if we close stdin here
      chan close $outfp
      exit 1
   }

   # For use as tclpps script
   # with the shebang #!/usr/bin/env tclpps
   if {[AmCalledAsTclpps $argv0ForThis] || [IsTclpps $file]} {
      # remove shebang
      regsub {^#![^\n]*\n} $result {} result
   }

   ::tclpp::DebugVal result
   puts -nonewline $outfp $result

   chan close $fp ;# No problem if we close stdin here
   chan close $outfp
   exit 0
}

main
