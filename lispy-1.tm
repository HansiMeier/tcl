#!/usr/bin/env tclsh

package require Tcl 8.5
regexp {^([[:alpha:]]+)-} [file tail [info script]] _ namespaceName

namespace eval $namespaceName {
   variable namespaceName [namespace current]

   namespace eval redirectChanTo {
      proc file {fileName mode} {

         set fd [open $fileName $mode]
      
         # Make new throw-away namespace
         # and close the file afterwards --> 1
         makeTranschanNamespace $fd 1

         return [namespace current]::$fd
      }

      proc chan {channelId} {

         # Make throw-away namespace for a transchan but do
         # not close the channel afterwards
         makeTranschanNamespace $channelId 0

         return [namespace current]::$channelId
      }


      proc makeTranschanNamespace {fd close} {
         namespace eval $fd {

            proc initialize {handle mode} {
               return [info procs]
            }

            proc finalize handle {
               variable fd
               variable [namespace parent]::close
               if {$close} {
                  chan close $fd
               }
               namespace delete [namespace current]
               return
            }

            proc write {handle buffer} {
               variable fd
               puts -nonewline $fd $buffer
               return
            }

            proc flush handle {
               chan flush $handle
               return
            }

            proc read {handle buffer} {
               return $buffer
            }

            proc drain handle {
               return $buffer
            }

            namespace export *
            namespace ensemble create
         }

         # Pass the variables to the transchan namespace
         set ${fd}::fd $fd
         set ${fd}::close $close

         return
      }


      namespace export file chan
      namespace ensemble create
   }

   # inspired by Common Lisp's with-open-file
   proc withOpenFile {fdVar file mode body} {
      uplevel 1 [subst -nocommands -nobackslashes {
         set {$fdVar} [open {$file} {$mode}]
         try {
            $body
         } finally {
            chan close [set {$fdVar}]
         }
      }]
   }


   # lisp-style quoting
   # only subst if preceded by a comma
   proc ` txt {
      # \\x because string map subtitutes backslashes in its map
      # and subst does the other backslash substitute
      # \x5C == \ 
      # \x24 == $
      # \x5D == [
      return [
         uplevel 1 \
            "subst {[string map {,$ $ $ \\x24 ,[ [ [ \\x5B ,\\ \\ \\ \\x5C} $txt]}"
      ]
   }

   # this is here because syntax-highlighting surrenders"]]]"
   

   # Haskell-like folds
   proc foldl {cmdPrefix init list} {
      foreach elem $list {
         set init [uplevel 1 "$cmdPrefix {$init} {$elem}"]
      }
      return $init
   }

   proc foldr {cmdPrefix init list} {
      for {set i [expr {[llength $list]-1}]} {$i != -1} {incr i -1} {
         set init [uplevel 1 "$cmdPrefix {$init} {[lindex $list $i]}"]
      }

      return $init
   }


   foreach fold {foldr foldl} {
      proc ${fold}1 {cmdPrefix list} [${namespaceName}::` {
         ,$fold $cmdPrefix [lindex $list 0] [lrange $list 1 end]
      }]
   }
      

   proc capture {{varList {}}} {
      set upNamespace [regsub {::$} [uplevel 1 {namespace current}] {}]

      if {$varList eq {}} {
         set varList [uplevel 1 {info vars}]
      }

      foreach var $varList {
         lappend pairVarNames [list [uplevel 1 "namespace which -variable {$var}"] [namespace tail $var]]
      }

      return $pairVarNames
   }

   proc upvarProc {name arguments varList body} {
      set capturedVars [uplevel 1 "{[namespace current]::capture} \[list $varList]"]
      foreach pairOfVars $capturedVars {
         append upvarCode "upvar 0 {[lindex $pairOfVars 0]} {[lindex $pairOfVars 1]}\n"
      }
      uplevel 1 "
         proc {$name} \[list $arguments] {
            $upvarCode
            $body
         }
      "

      return
   }

   namespace export *
}

unset namespaceName

