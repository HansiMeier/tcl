#include <cpptcl.h>
#include <vector>
#include <string>
#include <sstream>
#include <cstring>

Tcl::interpreter ooInterp (NULL, false);
Tcl_Interp* cInterp;


#define declareCommand(command) \
   ooInterp.def("::cppvec::" #command, command);

/**************************************************************************************************
 *                                   Tcl_ObjType for C++ vector                                   *
 **************************************************************************************************/
static Tcl_ObjType cppvector;
// The new type, a c++ Vector
int setFromAnyProc(Tcl_Interp* interp, Tcl_Obj* objp) {

   static const char* longStr = "long";
   static const char* doubleStr = "double";
   std::stringstream ss;
   ss.str(objp->bytes);
   std::string type;
   ss >> type;
   if (type == "double") {
      double read;
      std::vector<double>* doubleVecp (new std::vector<double>);
      doubleVecp->reserve(20);
      do {
         ss >> read;
         if (ss.fail()) {
            delete doubleVecp;
            throw Tcl::tcl_error 
                  (std::string ("Failed to create vector from string representation {")
                   + std::string (objp->bytes)
                   + std::string ("}!"));
         }
         doubleVecp->push_back(read);
      } while (!ss.eof());
      objp->internalRep.twoPtrValue.ptr1 = (void*) doubleStr;
      objp->internalRep.twoPtrValue.ptr2 = (void*) doubleVecp;
      return TCL_OK;
   } else if (type == "long") {
      long read;
      std::vector<long>* longVecp (new std::vector<long>);
      longVecp->reserve(20);
      do {
         ss >> read;
         if (ss.fail()) {
            delete longVecp;
            throw Tcl::tcl_error 
                  (std::string ("Failed to create vector from string representation {")
                   + std::string (objp->bytes)
                   + std::string ("}!"));
         }
         longVecp->push_back(read);
      } while (!ss.eof());
      objp->internalRep.twoPtrValue.ptr1 = (void*) longStr;
      objp->internalRep.twoPtrValue.ptr2 = (void*) longVecp;
      return TCL_OK;
   }
   throw Tcl::tcl_error(std::string ("Unknown type ") + std::string (type) + std::string("!"));
}

void updateStringProc(Tcl_Obj* objp) {
   std::string type ((char*) objp->internalRep.twoPtrValue.ptr1);
   std::stringstream ss;
   if (type == "double") {
      ss << "double";
      for (double v: *((std::vector<double>*) objp->internalRep.twoPtrValue.ptr2)) {
         ss << " " << v;
      }
   } else if (type == "long") {
      ss << "long";
      for (long v: *((std::vector<long>*) objp->internalRep.twoPtrValue.ptr2)) {
         ss << " " << v;
      }
   }
   std::string s (ss.str());
   objp->bytes = (char*) ckalloc(s.size());
   std::strcpy(objp->bytes, s.c_str());
   return;
}

void dupInternalRepProc(Tcl_Obj* srcp, Tcl_Obj* dupp) {
   std::string type ((char*) srcp->internalRep.twoPtrValue.ptr1);
   if (type == "double") {
      dupp->internalRep.twoPtrValue.ptr1 = (void*) "double";
      dupp->internalRep.twoPtrValue.ptr2 = 
            new std::vector<double> (*((std::vector<double>*) srcp->internalRep.twoPtrValue.ptr2));
      return;
   } else if (type == "long") {
      dupp->internalRep.twoPtrValue.ptr1 = (void*) "long";
      dupp->internalRep.twoPtrValue.ptr2 = 
            new std::vector<long> (*((std::vector<long>*) srcp->internalRep.twoPtrValue.ptr2));
      return;
   }
   throw Tcl::tcl_error ("Couldn't duplicate vector of type " + type + "!");
}

void freeInternalRepProc(Tcl_Obj* objp) {

   std::string type ((char*) objp->internalRep.twoPtrValue.ptr1);
   if (type == "double") {
      delete (std::vector<double>*) objp->internalRep.twoPtrValue.ptr2;
      return;
   } else if (type == "long") {
      delete (std::vector<double>*) objp->internalRep.twoPtrValue.ptr2;
      return;
   } 
   throw Tcl::tcl_error ("Couldn't delete vector of type " + type + "!");
}

/**************************************************************************************************
 *                                           VECTOR API                                           *
 **************************************************************************************************/

std::string vtype(Tcl::object vec) {
   if (Tcl_ConvertToType(cInterp, vec.get_object(), &cppvector) != TCL_OK) {
      throw Tcl::tcl_error ("Could not convert to vector!");
   }
   return std::string ((char*) vec.get_object()->internalRep.twoPtrValue.ptr1);
}

long vsize(Tcl::object vec) {
   std::string type (vtype(vec));
   if (type == "double") {
      return long(((std::vector<double>*) vec.get_object()->internalRep.twoPtrValue.ptr2)->size());
   } else if (type == "long") {
      return long(((std::vector<long>*) vec.get_object()->internalRep.twoPtrValue.ptr2)->size());
   }

   throw Tcl::tcl_error ("Couldn't get size of vector of type " + type + "!");
}

Tcl::object vindex(Tcl::object vec, long index) {
   if (Tcl_ConvertToType(cInterp, vec.get_object(), &cppvector) != TCL_OK) {
      throw Tcl::tcl_error ("Could not convert to vector!");
   }
   std::string type (vtype(vec));
   long size (vsize(vec));
   if (index >= size || index < -size) {
      throw Tcl::tcl_error (
            "Index " + std::to_string(index) + " is out of bounds (size of vector: "
            + std::to_string(size) +")!"
      );
   }

   if (index < 0) {
      index = size - index;
   }

   if (type == "double") {
      return Tcl::object ((*((std::vector<double>*) vec.get_object()->internalRep.twoPtrValue.ptr2))[index]);
   } else if (type == "long") {
      return Tcl::object ((*((std::vector<long>*) vec.get_object()->internalRep.twoPtrValue.ptr2))[index]);
   }


   throw Tcl::tcl_error ("Couldn't get index of vector of type " + type + "!");
}

Tcl::object vset(Tcl::object vec, long index, Tcl::object toSet) {
   if (Tcl_ConvertToType(cInterp, vec.get_object(), &cppvector) != TCL_OK) {
      throw Tcl::tcl_error ("Could not convert to vector!");
   }
   std::string type (vtype(vec));
   long size (vsize(vec));
   if (index >= size || index < -size) {
      throw Tcl::tcl_error (
            "Index " + std::to_string(index) + " is out of bounds (size of vector: "
            + std::to_string(size) +")!"
      );
   }

   if (index < 0) {
      index += size;
   }

   if (type == "double") {
      (*((std::vector<double>*) vec.get_object()->internalRep.twoPtrValue.ptr2))[index] = toSet.get<double>(ooInterp);
      updateStringProc(vec.get_object());
      return vec;
   } else if (type == "long") {
      Tcl::object ((*((std::vector<long>*) vec.get_object()->internalRep.twoPtrValue.ptr2))[index] = toSet.get<long>(ooInterp));
      updateStringProc(vec.get_object());
      return vec;
   }


   throw Tcl::tcl_error ("Couldn't set vector of type " + type + "!");
}

Tcl::object vappend(Tcl::object vec, Tcl::object toAppend) {
   if (Tcl_ConvertToType(cInterp, vec.get_object(), &cppvector) != TCL_OK) {
      throw Tcl::tcl_error ("Could not convert to vector!");
   }
   std::string type (vtype(vec));
   if (type == "double") {
      ((std::vector<double>*) vec.get_object()->internalRep.twoPtrValue.ptr2)->push_back(toAppend.get<double>(ooInterp));
      updateStringProc(vec.get_object());
      return vec;

   } else if (type == "long") {
      ((std::vector<long>*) vec.get_object()->internalRep.twoPtrValue.ptr2)->push_back(toAppend.get<long>(ooInterp));
      updateStringProc(vec.get_object());
      return vec;
   } 

   throw Tcl::tcl_error ("Couldn't append to vector of type " + type + "!");
}




extern "C" {
   int Cppvec_Init(Tcl_Interp* interp) {
      Tcl_InitStubs(interp, "8.6-", 0);
      ooInterp.set(interp);
      cInterp = interp;
      cppvector.name = "C++ vector";
      cppvector.freeIntRepProc = &freeInternalRepProc;
      cppvector.dupIntRepProc = &dupInternalRepProc;
      cppvector.updateStringProc = &updateStringProc;
      cppvector.setFromAnyProc = &setFromAnyProc;
      Tcl_RegisterObjType(&cppvector);


      Tcl_Namespace* nsPtr = ooInterp.create_namespace("cppvec");

      declareCommand(vindex);
      declareCommand(vset);
      declareCommand(vappend);
      declareCommand(vtype);
      declareCommand(vsize);
      ooInterp.export_namespace(nsPtr, "[a-z]*");

      return TCL_OK;
   }
}


