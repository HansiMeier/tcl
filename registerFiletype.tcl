#!/usr/bin/env tclsh

package require Tcl 8.6-
package require json
package require fileutil


####################################################################################################
#                                          UTILITY PROCS                                           #
####################################################################################################

proc signalizeError txt {
   puts stderr $txt
   exit 1
}

proc isOSX {} {
   tailcall string equal $::tcl_platform(os) Darwin
}

proc makeSureWeAreOnOSX {} {
   if {! [isOSX]} {
      signalizeError "This program only runs on OSX!"
   }
}

proc makeSureNeededProgramsExist {} {
   foreach progVar [list ConvertingProgram RegisteringProgram ActivatingProgram] {
      upvar #0 $progVar prog
      set fullPath [auto_execok $prog] 
      if {$fullPath eq ""} {
         signalizeError "\"$prog\" not executable!"
      }
      set prog $fullPath
   }
}

proc displayUsageAndExit argv0 {
   puts stderr "Usage: $argv0 <fileextension> <path to application>"
   puts stderr "Registers the file extension with the given application"
   puts stderr "Example: $argv0 tcl /Applications/MacVim.app"
   exit 1
}

proc cleanUpHandler args {
   catch {
      file delete $::TmpFile
   }
}

trace add execution exit enter ::cleanUpHandler

####################################################################################################
#                                         GLOBAL VARIABLES                                         #
####################################################################################################

set ConvertingProgram plutil
set RegisteringProgram defaults
set ActivatingProgram "/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister"
set TmpFile [::fileutil::tempfile]
set ToJSONCommand [list $ConvertingProgram -convert json -o $TmpFile]



####################################################################################################
#                                            INFO PLIST                                            #
####################################################################################################

proc getInfoPlist appDir {
   if {! [file readable $appDir]} {
      signalizeError "\"$appDir\" isn't readable!"
   }

   set infoPlist "[file join $appDir Contents]/Info.plist"
   if {! [file readable $infoPlist]} {
      signalizeError "\"$infoPlist\" ins't readable!"
   }

   return $infoPlist
}

# $infoPlist has to be readable!
proc convertToJSONTmp infoPlist {
   global ToJSONCommand
   global TmpFile
   exec {*}$ToJSONCommand $infoPlist
   return $TmpFile

}

proc getBundleIdentifier infoPlistInJSONFormat {
   set json [::fileutil::cat $infoPlistInJSONFormat]
   set dict [::json::json2dict $json]
   tailcall dict get $dict CFBundleIdentifier
}

proc addHandler {filetype appId} {
   # The space is necessary so that exec doesn't think it needs to read from a file as in "exec sort < someFile"
   set arrayStr " <dict><key>LSHandlerContentTag</key><string>$filetype</string><key>LSHandlerContentTagClass</key><string>public.filename-extension</string><key>LSHandlerRoleAll</key><string>$appId</string></dict>"
   exec defaults write com.apple.LaunchServices LSHandlers -array-add $arrayStr
   exec $::ActivatingProgram -kill -domain local -domain system -domain user
}

####################################################################################################
#                                               MAIN                                               #
####################################################################################################

proc main {argv0 argc argv} {
   if {$argc != 2} {
      displayUsageAndExit $argv0
   }

   makeSureWeAreOnOSX
   makeSureNeededProgramsExist

   lassign $argv ext appDir
   regsub {^\.+} $ext "" ext

   set infoPlist [convertToJSONTmp [getInfoPlist $appDir]]
   set bundleIdentifier [getBundleIdentifier $infoPlist]
   addHandler $ext $bundleIdentifier

   puts stdout "Registered file extension \"$ext\" with \"$appDir\""
   puts stdout "A logout may be necessary for the changes to take effect."
}

main $argv0 $argc $argv

