#!/usr/bin/env tclsh
set internalFormat tiff
set scanImageOptions [list --format $internalFormat -x 215 -y 297 --resolution 150]
set defaultFormat pdf
set defaultPdfQuality printer
package require parseGnuOpts

proc scanImage {fileName} {
    global scanImageOptions
    set command [list scanimage {*}$scanImageOptions]
    puts $command
    set status [catch {exec -- {*}$command > $fileName} stdErr]
    if {$status != 0} {
        file delete $fileName
        throw SCAN_FAILED $stdErr
    }
}

proc openWith file {
    set openProg {}
    foreach prog [list gnome-open kde-open xdg-open open start] {
        set openProg [auto_execok $prog]
        if {$openProg ne ""} {
            break
        }
    }
    if {$openProg eq ""} {
        throw {no openprog} "No opening program found!"
    } elseif {[string match -nocase *start* $openProg]} {
        lappend $openProg {""}
    }
    exec {*}$openProg $file &
}


proc checkForRequiredPrograms {} {
    foreach prog {scanimage gm} {
        if {[auto_execok $prog] eq ""} {
            throw {MISSING PROGRAM ERROR} "Missing $prog"
        }
    }
}

proc generateFileName {} {
    global internalFormat
    return [clock format [clock seconds] -format "scan_%Y_%m_%d_%H_%M_%S.$internalFormat"]
}

proc convertFormat {input output sourceFormat targetFormat} {
    set options [list]
    if {$targetFormat eq "jpg"} {
        lappend options {-quality} 95
    }
    exec gm convert {*}$options $input $output
}

proc callOCR {inputFileName outputFileName language} {
    set command [list tesseract $inputFileName [file rootname $outputFileName] -l $language PDF]
    puts stderr "Executing command $command"
    set status [catch {set stdoutAndErr [exec -- {*}$command 2>@1]} errorMessage]
    if {$status != 0} {
        throw {OCR FAILED} "OCR FAILED $errorMessage $stdoutAndErr"
    }
}

proc compressPDF {inputFileName outputFileName pdfQuality} {
    set command [list gs -sDEVICE=pdfwrite -dPDFSETTINGS=/$pdfQuality \
         -dNOPAUSE -dQUIET -dBATCH -dSAFER -dEmbedAllFonts=true -dSubsetFonts=true \
         -sOutputFile=$outputFileName $inputFileName]
    set status [catch {set stdoutAndErr [exec -- {*}$command 2>@1]} errorMessage]
    if {$status != 0} {
        throw {PDFCOMPRESS FAILED} "PDFCOMPRESS FAILED $errorMessage $stdoutAndErr"
    }
}

proc main {} {
    global argv argc argv0 defaultFormat defaultPdfQuality
    global internalFormat
    checkForRequiredPrograms
    set optDict [dict create \
        fileNameOpt [dict create \
            shortOpt {} \
            longOpt {} \
            numberOfArgs 1 \
            required 0 \
            usage "file name to output. Defaults to something like [generateFileName]" \
            argsDescription "file name" \
        ] \
        noOpenOpt [dict create \
            shortOpt "n" \
            longOpt "no-open" \
            numberOfArgs 0 \
            required 0 \
            usage "Do not open scanned image" \
        ] \
        formatOpt [dict create \
            shortOpt "f" \
            longOpt "format" \
            numberOfArgs 1 \
            required 0 \
            usage "format of output file (default $defaultFormat)" \
            argsDescription "Format like png or pdf" \
        ] \
        ocrOpt [dict create \
            shortOpt "c" \
            longOpt "ocr" \
            numberOfArgs 1 \
            required 0 \
            usage "Use OCR to create a searchable PDF. The argument is the language. Use deu, fra, ita, eng" \
            argsDescription "OCT language" \
        ] \
        pdfQualityOpt [dict create \
            shortOpt {} \
            longOpt "pdfquality" \
            numberOfArgs 1 \
            required 0 \
            usage "pdf quality to use. One of screen, ebook, printer, prepress" \
            argsDescription "PDF quality" \
        ] \
    ]

    set thisProgramName [file tail $::argv0]


    set usage "Scans a color image"
    ::parseGnuOpts::processArgs $argv0 $argv $argc $optDict $usage
    ::parseGnuOpts::importOptArrays

    if {$help(present)} {
        ::parseGnuOpts::displayUsage
        exit 1
    }

    set format $defaultFormat

    if {$fileNameOpt(present)} {
        set fileName [lindex $fileNameOpt(passedArgs) 0]
        set format [string range [file extension $fileName] 1 end]
        set fileName [file join [file dirname $fileName] "[file rootname $fileName].$internalFormat"]
    } else {
        set fileName [generateFileName]
    }

    set fileName [file normalize $fileName]

    if {$formatOpt(present)} {
        set format [lindex $formatOpt(passedArgs) 0]
    }

    if {$ocrOpt(present)} {
        if {![string equal -nocase $format pdf]} {
            set format pdf
            puts stderr "Using pdf for OCR"
        }
    }

    set pdfQuality $defaultPdfQuality
    if {$pdfQualityOpt(present)} {
        set pdfQuality [lindex $pdfQualityOpt(passedArgs) 0]
    }

    set pdfCompressionEnabled 0
    if {$pdfQuality ne ""} {
        # pdf quality set --> format must be pdf
        set format pdf
        puts stderr "Using pdf quality of $pdfQuality"
        set pdfCompressionEnabled 1
    }

    puts stderr "Chosen format $format"
    puts stderr "Scanning to $fileName"
    scanImage $fileName

    if {![string equal -nocase $format $internalFormat]} {
        set convertedName [file join [file dirname $fileName] "[file rootname $fileName].$format"]
        if {$ocrOpt(present)} {
            puts stderr "Creating searchable PDF with OCR"
            callOCR $fileName $convertedName [lindex $ocrOpt(passedArgs) 0]
        } else {
            puts stderr "Converting $fileName to $convertedName"
            convertFormat $fileName $convertedName $internalFormat $format
        }
        if {$pdfCompressionEnabled} {
            puts stderr "Compressing $convertedName"
            set tmpFile "${convertedName}.compressed"
            try {
                compressPDF $convertedName $tmpFile $pdfQuality
                file rename -force -- $tmpFile $convertedName
            } on error msg {
                puts stderr $msg
                file delete $tmpFile
            }
        }
        file delete $fileName
        set fileToOpen $convertedName
    } else {
        set fileToOpen $fileName
    }

    if {!$noOpenOpt(present)} {
        openWith $fileToOpen
    }
}

main
