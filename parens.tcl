#!/usr/bin/env tclsh

namespace import ::tcl::mathop::*

proc parensRec {opening closing acc} {
   if {! $opening} {
      puts stdout $acc[string repeat ) $closing]
      return
   }

   set closingBefore $closing
   if {$opening} {
      incr openingMinusOne [- $opening 1]
      set closeParens ""
      while {$closing && $closing > $opening} {
         append closeParens )
         incr closing -1
         parensRec $openingMinusOne $closing $acc${closeParens}(
      }
   }

   if {[set closing $closingBefore]} {
      incr closing -1
      set openParens ""
      for {set o 1} {$o <= $opening} {incr o 1} {
         append openParens (
         parensRec [- $opening $o] $closing ${acc}${openParens})
      }
   }


   return
}

proc parens pairs {
   tailcall parensRec $pairs $pairs ""
}

parens [lindex $argv 0]

