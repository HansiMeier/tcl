#!/usr/bin/env tclsh

package require Tcl 8.6-
package require stefCoroutines
package require misc


namespace eval permutations {
   proc Swap {listVar i j} {
      upvar 1 $listVar list
      set eI [lindex $list $i]
      set eJ [lindex $list $j]
      set list [lreplace $list[set list {}] $i $i $eJ]
      set list [lreplace $list[set list {}] $j $j $eI]
      return
   }

   proc Heap'sPermutations {n listVar} {
      
      if {$n == 1} {
         yield [set $listVar]
      } else {
         incr n -1
         for {set i 0} {$i < $n} {incr i 1} {
            ::permutations::Heap'sPermutations $n $listVar
            if {$n % 2 == 1} {
               ::permutations::Swap $listVar $i $n
            } else {
               ::permutations::Swap $listVar 0 $n
            }
         }
         tailcall ::permutations::Heap'sPermutations $n $listVar
      }
   }
   
   proc Heap'sPermutationsCoroutine {n list tmpListVar} {
      set $tmpListVar $list
      yield
      ::permutations::Heap'sPermutations $n $tmpListVar
      unset $tmpListVar
      return -code break ""
   }


   proc generator list {
      set tmpListVar [::misc::genVarName]
      set corProc [::stefCoroutines::gensym]
      coroutine $corProc ::permutations::Heap'sPermutationsCoroutine [llength $list] $list $tmpListVar 
      return $corProc
   }
   namespace export {[a-z]*}
}


