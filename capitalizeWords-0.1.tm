#!/usr/bin/env tclsh

package require Tcl 8.6-

proc capitalizeWords {string} {
    set letters [split $string ""]
    set capitalizedString ""
    set wasNonWordcharBefore true
    set isWordCharWithExtensions {apply {char {
        set extensions [list ']
        return [expr {[string is wordchar $char] || $char in $extensions}]
    }}}
    foreach letter $letters {
        set isWordCharNow [{*}$isWordCharWithExtensions $letter]
        if {$isWordCharNow && $wasNonWordcharBefore} {
            set toAppend [string toupper $letter]
        } else {
            set toAppend $letter
        }

        append capitalizedString $toAppend
        set wasNonWordcharBefore [expr {!$isWordCharNow}]
    }

    return $capitalizedString
}
