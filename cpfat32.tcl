#!/usr/bin/env tclsh

package require Tcl 8.6-

if {[info exists ::env(TCL)]} {
    set FAT32MAP_FILE [file join $::env(TCL) fat32map.txt]
} else {
    set FAT32MAP_FILE [file join [pwd] fat32map.txt]
}

set FAT32MAP_DEFAULT {
    ? _
    : _
    * _
    | _
    \" '
    ~ -
    < (
    > )
    \{ (
    \} )
    [ (
    ] )
}


set DebugOn false

proc pvarval var {
    if {!$::DebugOn} {
        return
    }
    upvar 1 $var local
    puts stderr "$var = \"$local\""
    return
}

proc loadFAT32MAP {file default globalVar} {
    global $globalVar

    if {!([file readable $file] && [string equal [file type $file] file])} {
        set $globalVar $default
        return
    }

    set fd [open $file r]
    set lines [split [chan read $fd [file size $file]] \n]
    set $globalVar {}
    set splitChar /
    foreach line $lines {
        if {[string length $line] == 1} {
            set splitChar $line
            continue
        }
        set toAdd [split $line $splitChar]
        # ignore empty lines
        if {! [llength $toAdd]} {
            continue
        }
        if {[llength $toAdd] != 2} {
            puts stderr "Invalid line while reading \"$file\": line \"$line\", using default map"
            set $globalVar $default
            return
        }

        lappend $globalVar [lindex $toAdd 0]
        lappend $globalVar [lindex $toAdd 1]
    }

    return
}

proc sanitizePath path {
# file normalize makes the path absolute and dereferences all symbolic links in the dirname.
# It happily works on nonexisting paths.
    set path [file normalize $path]

    if {! [file exists $path]} {
        signaliseError {NOT FOUND} "\"$path\" doesn't exist!"
    }
    if {! [file readable $path]} {
        signaliseError {NOT READABLE} "\"$path\" is not readable!"
    }

    set type [file type $path]

    # Dereference links
    while {[string equal $type link]} {
        set deref [file readlink $path]
        if {! [string equal [file pathtype $deref] absolute]} {
            set deref [file join [file dirname $path] $deref]
        }
        set path $deref
        set type [file type $path]
    }

    if {$type ni {directory file}} {
        signaliseError {BAD TYPE} "Bad file type $type, can only handle directories, files and symlinks to them."
    }
    return [list $type $path]
}

proc copyFile {sourceFile targetDir} {
    global FAT32MAP
    pvarval sourceFile
    pvarval targetDir

    # Make $target FAT32 compatible (i.e. replace illegal characters in the basename as
    # the dirname should already exist and be compatible by virtue of makeTargetDir)
    set targetFile [file join $targetDir [string map -nocase $FAT32MAP [file tail $sourceFile]]]
    if {[file readable $targetFile] && [file mtime $targetFile] > [file mtime $sourceFile]} {
        puts stdout "\"$sourceFile\" -> /*NO COPY BECAUSE TARGET NEWER*/"
        return
    }
    try {
        puts stdout "\"$sourceFile\" -> \"$targetFile\"" ;# inform the user that we copy this file
        file copy -force -- $sourceFile $targetFile
        catch {
            file attributes $targetFile -permissions u=rw,g=r,o=r -owner $::env(USER)
        }
    } on error err {
        # fatalError and not signaliseError used because files not being
        # readable is already covered in sanitizePath and
        # everything else (filesystem read-only etc.) is fatal.
        fatalError {COPY FAILED} $err
    }

    return
    }

    proc signaliseError {msg text} {
    # put the error message so that it gets displayed even if the exception is catched.
        puts stderr $text
        throw $msg $text
        return
    }

    # Like signaliseError but this error shall not be recoverable
    # same syntax as signaliseError so that they are interchangable for debugging.
    proc fatalError {ignore text} {
        puts stderr $text
        exit 2
    }

    proc recursivelyCopyFiles {sourceDir targetDir} {
        pvarval targetDir
        foreach object [lsort -dictionary [glob -nocomplain -directory $sourceDir -- *]] {
            pvarval object
            # inform the user if the object is nonexistent or not readable but
            # continue operation
            if {[catch {set sanePathPair [sanitizePath $object]}]} {
                puts "Does not exist, continuing"
                continue
            }

            set type [lindex $sanePathPair 0]
            set sanePath [lindex $sanePathPair 1]

            pvarval type

            switch -nocase -- $type {
                file {
                    copyFile $sanePath $targetDir
                }

                directory {
                    recursivelyCopyFiles $sanePath [makeTargetDir [file join $targetDir [file tail $sanePath]]]
                }

                default {
                    error "Should never be here!"
                }
            }
        }
    }

    proc processPaths {paths targetDir} {
        pvarval targetDir
        foreach path $paths {
            pvarval path
            # inform the user if the object is nonexistent or not readable but
            # continue operation
            if {[catch {set sanePathPair [sanitizePath $path]}]} {
                continue
            }

            set type [lindex $sanePathPair 0]
            set sanePath [lindex $sanePathPair 1]

            if {[string equal $type file]} {
                copyFile $sanePath $targetDir
            } else {
                recursivelyCopyFiles $sanePath [makeTargetDir [file join $targetDir [file tail $sanePath]]]
            }
        }
        return
    }

    proc maybeSyncFilesystems {} {
        if {[string equal -nocase $::tcl_platform(platform) unix] && [auto_execok sync] ne ""} {
            exec sync
        }
    }

    proc makeTargetDir targetDir {
        global FAT32MAP
        # not using sanitizePath because the targetDir may well not exist
        # and I don't want to write a try trap block
        set targetDir [string map -nocase $FAT32MAP [file normalize $targetDir]]
        if {! [file exists $targetDir]} {
            try {
                file mkdir $targetDir
            } on error mkdirError {
            # Catching file mkdir to raise a fatalError and not only a normal exception
                fatalError {CREATING TARGETDIR FAILED} "Couldn't create target directory \"$targetDir\": $mkdirError"
            }
            catch {
                file attributes $targetDir -permissions u=rwx,g=rx,o=rx -owner $::env(USER)
            }
        }


        set type [file type $targetDir]
        if {$type eq "directory"} {
            if {! [file writable $targetDir]} {
                fatalError {TARGETDIR NOT WRITABLE} "Target directory \"$targetDir\" has to be writable!"
            }
        } else {
            fatalError {TARGETDIR IS NO DIRECTORY} "\"$targetDir\" must be a existing target directory or a directory to create, but instead is a $type."
        }

        return $targetDir
    }

    proc displayUsageAndExit argv0 {
        puts stderr "Usage: [file tail $argv0] <directory/file to copy 1> \[directory/file to copy 2] ... <target directory>"
        puts stderr "This program copies files and directories recursively."
        puts stderr "It is intended for copying to/from a Windows file system."
        puts stderr "Therefore, this program replaces some characters in the filename to be Windows compatible."
        puts stderr "The target directory doesn't have to exist, it is created if necessary."
        exit 1
    }

    proc main {argc argv0 argv} {
        if {$argc < 2} {
            displayUsageAndExit $argv0
        }

        global FAT32MAP_DEFAULT
        global FAT32MAP_FILE
        loadFAT32MAP $FAT32MAP_FILE $FAT32MAP_DEFAULT FAT32MAP

        set last [expr {$argc - 1}]
        set target [makeTargetDir [lindex $argv $last]]
        pvarval target
        set paths [lrange $argv 0 [expr {$last - 1}]]
        pvarval paths
        processPaths $paths $target
        maybeSyncFilesystems
        return
    }

    main $argc $argv0 $argv
