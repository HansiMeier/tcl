#include <tcl.h>
#include <stdbool.h>
#include <stdlib.h>
#define FAILSAFE_STEFMATH
#include "stefMath.h"
#undef FAILSAFE_STEFMATH
#include <math.h>
#include <stdio.h>

#define NAMESPACE "::stefMath"
#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************************************
 *                                              TCL                                               *
 **************************************************************************************************/

int millerRabinPrimeCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // Exactly one argument needed (objv[0] is the command name)
   if (objc != 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "number");
      return TCL_ERROR;
   }

   // Get the number out of the argument;
   Tcl_WideInt n;
   if (Tcl_GetWideIntFromObj(interp, objv[1], &n) == TCL_ERROR) {
      return TCL_ERROR;
   }

   // Calc
   Tcl_Obj* probablyPrime = Tcl_NewBooleanObj(millerRabinPrime(n));

   // Return
   Tcl_SetObjResult(interp, probablyPrime);
   return TCL_OK;
}

int erfCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // Exactly one argument needed (objv[0] is the command name)
   if (objc != 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "number");
      return TCL_ERROR;
   }

   // Get the number out of the argument;
   double x;
   if (Tcl_GetDoubleFromObj(interp, objv[1], &x) == TCL_ERROR) {
      return TCL_ERROR;
   }

   // Calc
   Tcl_Obj* value = Tcl_NewDoubleObj(erf(x));

   // Return
   Tcl_SetObjResult(interp, value);
   return TCL_OK;
}

int binCoeffCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // Exactly two arguments needed (objv[0] is the command name)
   if (objc != 3) {
      Tcl_WrongNumArgs(interp, 1, objv, "n k");
      return TCL_ERROR;
   }

   // Get the number out of the argument;
   Tcl_WideInt n;
   Tcl_WideInt k;
   if (Tcl_GetWideIntFromObj(interp, objv[1], &n) == TCL_ERROR) {
      return TCL_ERROR;
   }
   if (Tcl_GetWideIntFromObj(interp, objv[2], &k) == TCL_ERROR) {
      return TCL_ERROR;
   }

   // Calc
   Tcl_Obj* result = Tcl_NewWideIntObj(binCoeff(n, k));

   // Return
   Tcl_SetObjResult(interp, result);
   return TCL_OK;
}

int fallingFactCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // Exactly two arguments needed (objv[0] is the command name)
   if (objc != 3) {
      Tcl_WrongNumArgs(interp, 1, objv, "n k");
      return TCL_ERROR;
   }

   // Get the number out of the argument;
   Tcl_WideInt n;
   Tcl_WideInt k;
   if (Tcl_GetWideIntFromObj(interp, objv[1], &n) == TCL_ERROR) {
      return TCL_ERROR;
   }
   if (Tcl_GetWideIntFromObj(interp, objv[2], &k) == TCL_ERROR) {
      return TCL_ERROR;
   }

   // Calc
   Tcl_Obj* result = Tcl_NewWideIntObj(fallingFact(n, k));

   // Return
   Tcl_SetObjResult(interp, result);
   return TCL_OK;
}


int expmodCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // exactly three args needed (objv[0] is the command name)
   if (objc != 4) {
      Tcl_WrongNumArgs(interp, 1, objv, "base exponent modulus");
      return TCL_ERROR;
   }

   // Get arguments
   Tcl_WideInt base, exponent, modulus;
   if (Tcl_GetWideIntFromObj(interp, objv[1], &base) == TCL_ERROR) {
      return TCL_ERROR;
   }
   if (Tcl_GetWideIntFromObj(interp, objv[2], &exponent) == TCL_ERROR) {
      return TCL_ERROR;
   }
   if (Tcl_GetWideIntFromObj(interp, objv[3], &modulus) == TCL_ERROR) {
      return TCL_ERROR;
   }

   Tcl_SetObjResult(interp, Tcl_NewWideIntObj(expmod(base, exponent, modulus)));
   return TCL_OK;
}

int bestRationalApproximantCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

    // one or two args needed (objv[0] is the command name)
    Tcl_WideInt maximalDenominator = 100000000;
    double relativeTolerance = 1e-5;
    if (objc > 4 || objc < 2) {
        Tcl_WrongNumArgs(interp, 1, objv, "toApproximate ?maximalDenominator? ?relativeTolerance?");
        return TCL_ERROR;
    }


    // Get Arguments
    double toApproximate;
    if (Tcl_GetDoubleFromObj(interp, objv[1], &toApproximate) == TCL_ERROR) {
        return TCL_ERROR;
    }

    if (objc >= 3) {
        if (Tcl_GetWideIntFromObj(interp, objv[2], &maximalDenominator) == TCL_ERROR) {
            return TCL_ERROR;
        }

        if (maximalDenominator <= 0) {
            static char errorMsgForNonPositiveMaxDen[] = "maximalDenominator has to be positive!";
            Tcl_SetResult(interp, errorMsgForNonPositiveMaxDen, NULL);
            return TCL_ERROR;
        }

        if (objc >= 4) {
            if (Tcl_GetDoubleFromObj(interp, objv[3], &relativeTolerance) == TCL_ERROR) {
                return TCL_ERROR;
            }

            if (relativeTolerance >= 1.0 || relativeTolerance <= 0.0) {
                static char errorMsgForBadrelativeTolerance[] = "Relative tolerance has to be in ]0, 1[!";
                Tcl_SetResult(interp, errorMsgForBadrelativeTolerance, NULL);
                return TCL_ERROR;
            }
        }
    }


    inum result[2];
    bestRationalApproximant((rnum) toApproximate, (unum) maximalDenominator, (rnum) relativeTolerance, result);


    Tcl_Obj* numerator = Tcl_NewWideIntObj((Tcl_WideInt) result[0]);
    Tcl_Obj* denominator = Tcl_NewWideIntObj((Tcl_WideInt) result[1]);

    Tcl_Obj* list = Tcl_NewListObj(1, &numerator);
    Tcl_ListObjAppendElement(interp, list, denominator);

    Tcl_SetObjResult(interp, list);
    return TCL_OK;
}





/**************************************************************************************************
 *                                              INIT                                              *
 **************************************************************************************************/

int Stefmath_Init(Tcl_Interp* interp) {
   Tcl_Namespace* nsPtr;

   #ifdef USE_TCL_STUBS
   if (Tcl_InitStubs(interp, "8.6-" , 0) == 0L) {
      return TCL_ERROR;
   }
   #else
   if (Tcl_PkgRequire(interp, "Tcl", "8.6-", 0) == 0L) {
      return TCL_ERROR;
   }
   #endif

   // Create the namespace
   nsPtr = Tcl_CreateNamespace(interp, NAMESPACE, NULL, NULL);
   if (!nsPtr) {
      return TCL_ERROR;
   }

   // Declare commands
   Tcl_CreateObjCommand(interp, NAMESPACE "::isPrime", millerRabinPrimeCmd, NULL, NULL);
   Tcl_CreateObjCommand(interp, NAMESPACE "::expmod", expmodCmd, NULL, NULL);
   Tcl_CreateObjCommand(interp, NAMESPACE "::binCoeff", binCoeffCmd, NULL, NULL);
   Tcl_CreateObjCommand(interp, NAMESPACE "::fallingFact", fallingFactCmd, NULL, NULL);
   Tcl_CreateObjCommand(interp, NAMESPACE "::erf", erfCmd, NULL, NULL);
   Tcl_CreateObjCommand(interp, NAMESPACE "::bestRationalApproximant", bestRationalApproximantCmd, NULL, NULL);

   // Export all commands
   if (Tcl_Export(interp, nsPtr, "*", 0) == TCL_ERROR) {
      return TCL_ERROR;
   }

   return TCL_OK;
}

#ifdef __cplusplus
}
#endif
