#!/usr/bin/env tclsh

package require Tcl 8.6-
package require fileUrl ;# for the actual decoding
package require pathManip


namespace eval httpExtra {

    proc stripProtocolPrefix {url args} {
        switch [llength $args] {
            0 {
                set doWritePrefix 0
            }
            1 {
                set doWritePrefix 1
                set prefixVar [lindex $args 0]
            }
            default {
                throw syntaxerror {Has to be called with 1 or 2 arguments}
            }
        }

        if {$doWritePrefix} {
            upvar 1 $prefixVar protocolPrefix
        }

        set protocolPrefix {}
        regexp {^[^(://)]+://} $url protocolPrefix
        return [string range $url [string length $protocolPrefix] end]
    }






    proc percentEncodeUrl url {
        set urlWithoutPrefix [stripProtocolPrefix $url protocolPrefix]
        set percentEncoded [::pathManip::map ::fileUrl::percentEncode $urlWithoutPrefix]
        return $protocolPrefix$percentEncoded
    }

    proc percentDecodeUrl url {
        set urlWithoutPrefix [stripProtocolPrefix $url protocolPrefix]
        set percentDecoded [::pathManip::map ::fileUrl::percentDecode $urlWithoutPrefix]
        return $protocolPrefix$percentDecoded
    }



    namespace export {[a-z]*}
}
