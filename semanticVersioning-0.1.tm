#!/usr/bin/env tclsh


####################################################################################################
#                                       SEMANTIC VERSIONING                                        #
####################################################################################################

# operate on semantic version strings of the form:
# ${major version}.${minor version}.${patch version} with an optional suffix of the form -${suffix}
# where
# - the versions are non-negative integers without unneccessary leading zeroes
# - suffix is an arbitrary nonempty string
#
# the order of semantic versions is
# - component wise starting from the major version for the three version fields:
#   0.99.0 < 1.0.0
#   0.0.1 < 0.0.2
# - almost lexicographic for the suffix except that the empty suffix is greater than any non-empty suffix:
#   1.0.0-alpha < 1.0.0
#

# This package provides an ensemble command 'semver', which does all the work


# NOTE FOR DEVELOPERS
# as semver has a set and an incr subcommand, remember to write ::set and ::incr in the bodies of the procs
# to avoid namespace clashes!

package require Tcl 8.6-
package require misc ;# for gentleLassign

namespace eval semver {
   # Read a version string corresponding to the syntax defined above:
   # - returns a boolean which is true iff it is a valid semantic versionString
   # - writes the parts of the version string to the outvars if requested
   # if there is no suffix in the $versionString but one should be written into an outvar, write "" to the outvar.
   proc read {versionString args} {
      ::set major 0
      ::set minor 0
      ::set patch 0
      ::set parts [split $versionString .]
      ::set numOfParts [llength $parts]
      ::set argsLen [llength $args]

      if {$argsLen > 4} {
         throw {too many outvars} "Too many outvars: {$args} are $argsLen, but need at most four!"
      }

      # assign all parts
      ::misc::gentleLassign $parts major minor patch
      ::set suffix {}
      regexp -- {^([^\-]+)-(.+)$} $patch _ patch suffix

      ::set vals [list $major $minor $patch]
      foreach val [list $major $minor $patch] {
         # check that the numbers are whole and non-negative
         if {! ([regexp {^[0-9]+$} $val] && $val >= 0)} {
            return 0
         }
         # detect unnecessary leading zeroes
         scan $val %d pureIntVal
         if {[string length $val] != [string length $pureIntVal]} {
            return 0
         }
      }

      # assign to outvars if needed
      lappend vals $suffix
      ::set index 0
      # not using multi-variable syntax of foreach because
      # we do not want to assign values to the variable with the empty
      # name if we run out of variable names
      foreach var $args {
         ::set val [lindex $vals $index]
         ::incr index 1
         upvar 1 $var local
         ::set local $val
      }

      return 1
   }

   proc valid versionString {
      tailcall ::semver::read $versionString
   }


   proc < {sA sB} {
      ::semver::read $sA majorA minorA patchA suffixA
      ::semver::read $sB majorB minorB patchB suffixB
      # Compare versions
      foreach valA [list $majorA $minorA $patchA] valB [list $majorB $minorB $patchB] {
         if {$valA < $valB} {
            return 1
         } elseif {$valA > $valB} {
            return 0
         }
      }

      # compare suffixes
      if {$suffixA eq "" && $suffixB ne ""} {
         return 1
      } elseif {$suffixB eq "" && $suffixA ne ""} {
         return 0
      }

      tailcall ::tcl::mathop::== [string compare $suffixA $suffixB] -1
   }

   proc > {sA sB} {
      tailcall ::semver::< $sB $sA
   }

   proc <= {sA sB} {
      return [expr {![::semver::> $sA $sB]}]
   }

   proc >= {sA sB} {
      tailcall ::semver::<= $sB $sA
   }

   proc == {sA sB} {
      tailcall ::tcl::mathop::eq $sA $sB
   }

   proc compare {sA sB} {
      if {[::semver::< $sA $sB]} {
         return -1
      } elseif {[::semver::> $sA $sB]} {
         return 1
      }

      return 0
   }

   proc build {major minor patch {suffix ""}} {
      if {$suffix ne ""} {
         ::set suffix -$suffix
      }
      ::set candidate ${major}.${minor}.${patch}${suffix}
      if {![::semver::valid $candidate]} {
         throw {invalid version} "the version string \"$candidate\" is invalid!"
      }
      return $candidate
   }


   # $fieldVar must be one of "major", "minor", "patch"
   proc incr {versionString fieldVar {increment 1}} {
      if {$fieldVar ni [list major minor patch]} {
         throw {invalid fieldVar} "fieldVar must be one of \"major\", \"minor\", \"patch\", but is \"$fieldVar\" instead!"
      }
      ::semver::read $versionString major minor patch suffix
      if {$suffix ne ""} {
         ::set suffix -$suffix
      }
      ::incr $fieldVar $increment
      tailcall ::semver::build ${major} ${minor} ${patch} ${suffix}
   }

   # $fieldVar must be one of "major", "minor", "patch" or "suffix"
   proc set {versionString fieldVar value} {
      if {$fieldVar ni [list major minor patch suffix]} {
         throw {invalid fieldVar} "fieldVar must be one of \"major\", \"minor\", \"patch\", \"suffix\" but is \"$fieldVar\" instead!"
      }
      ::semver::read $versionString major minor patch suffix
      ::set $fieldVar $value

      tailcall ::semver::build $major $minor $patch $suffix
   }

   namespace export *
   namespace ensemble create
}



