proc tkcon_connect {} {
        set ch undefined
        if {[catch {set ch [socket localhost 8007]} err_msg]} {
                puts vimerr "Error: couldn't open a connection to TkCon instance, $err_msg"
        } else {
                chan configure $ch -buffering line -blocking no
        }
        return $ch
}

proc tkcon_src ch {
        if {[catch {puts $ch "src [$::vim::current(buffer) name]"} err_msg]} {
                puts vimerr "Error: couldn't send src command to TkCon, $err_msg"
        }
}

foreach sock [chan names sock*] {close $sock}
set ::tkcon_ch [tkcon_connect]
return

