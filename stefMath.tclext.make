UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	SHAREDLIBSUFFIX := dylib
else
	SHAREDLIBSUFFIX := so
endif

ifdef DEBUG
	EXT := debug.$(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -ggdb
else
	EXT := $(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -DNDEBUG
endif

LDFLAGS := -L$(TCLLIB) -ltclstub$(TCLVERSION) -lm 
INCLUDES := -I/usr/local/include -I$(TCLINCLUDE) -I$(CPPTCLINCLUDE)


CPPSTD := -std=gnu++0x
CPPFLAGS := -x c++ -Os -shared -fPIC -DUSE_TCL_STUBS -Wall

ifndef CPPC
	CPPC := g++
endif


stefMath: stefMath.c stefMath.tclext.c
	$(CPPC) $(CPPSTD) $(DEBUGFLAGS) $(CPPFLAGS) $(ADDTL_CPPFLAGS) $(INCLUDES) $(ADDTL_INCLUDES) $(CPPTCLINCLUDE)/cpptcl.cc stefMath.c stefMath.tclext.c -o stefMath.tclext.$(EXT) $(LDFLAGS) $(ADDTL_LDFLAGS)

