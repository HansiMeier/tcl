#!/usr/bin/env tclsh

package require Tcl 8.6-
package require tepam
interp alias {} docProc {} ::tepam::procedure
namespace import ::tcl::mathop::*

namespace eval functional {
   docProc £ {
      -description
         "Given arguments and a body, generates a command prefix with apply."
      -example
         {£ {a b} {return [expr {$a+$b}]}}
      -args {
         {-capture -optional -description "Capture the values of the variables in this list in the scope of the caller. if \"all\" is specified, capture all variables."}
         {arguments -description "The arguments of the lambda"}
         {body -description "The body of the lambda."}
         {namespace -optional -description "The namespace which the lambda should execute in. Default is the namespace in which it was created."}
      }
   } {
      if {![info exists namespace]} {
         set namespace [uplevel 1 {namespace current}]
      }
      if {[info exists capture]} {
         set captureCode {}
         if {$capture eq "all"} {
            set capture [uplevel 1 {info locals}]
         }

         foreach var $capture {
            upvar 1 $var local
            append captureCode "set {$var} {$local}\n"
         }

         set body ${captureCode}${body}
      }

      set command [list $arguments $body $namespace]
      tailcall list apply [list $arguments $body $namespace]
   }

   docProc unpack {
      -description
         "Unpacks a list like lassign but assigns the rest of the list to the last variable name given."
      -example
         "unpack {1 2 3} car cdr"
      -args {
         {list -description "The list to unpack"}
         {vars -multiple -description "The variables to assign."}
      }
   } {
      set cdrVar [lindex $vars end]
      set carVars [lrange $vars 0 end-1]

      uplevel 1 "set {$cdrVar} \[lassign \[list $list\] $carVars\]"
      return
   }

   docProc fmap {
      -description
         "Evaluates one or more command prefixes elementwise on a list and returns the list of lists of results"
      -example
         {fmap {incr puts} {1 2 3}}
      -args {
         {cmds -description "The command prefixes to use."}
         {list -description "The list on which to evaluate the prefixes."}
      }
   } {
      set results [list]
      foreach elem $list {
         foreach cmd $cmds {
            lappend resultsElem [{*}$cmd $elem]
         }
         lappend results $resultsElem
      }
      return $results
   }

   docProc map {
      -description
         "Evaluates a command prefix elementwise on a list and returns the results."
      -example
         "map puts {5 6 7}"
      -args {
         {cmd -description "The command prefix to use."}
         {list -description "The list on which to evaluate the prefix."}
      }
   } {
      tailcall lindex [::functional::fmap $cmd $list] 0
   }


   docProc filter {
      -description "Filter a list keeping only the elements which satisfy the predicate."
      -example
         {filter [£ {x} {return [== [% $x 2] 0]}] {1 2 3 4}}
      -args {
         {predicate -description "A command prefix returning a boolean value."}
         {list -description "The list to filter."}
      }
   } {
      set passed [list]
      foreach elem $list {
         if {[{*}$predicate $elem]} {
            lappend passed $elem
         }
      }
      return $passed
   }


   docProc fold {
      -description "Fold a list using a binary function. And maybe fold from the right or use an accumulator."
      -args {
         {-right -type none -description "Fold from the right instead of from the left. Does not switch positions of the arguments in the binary function like Haskell does."}
         {-accumulator -optional -description "The accumulator to use. Default is the head of the list."}
         {binaryFunction -description "The command prefix taking two arguments to use."}
         {list -description "The list to fold over."}
      }
   } {
      if {! [info exists accumulator]} {
         set accumulator [lindex $list 0]
         set list [lrange $list 1 end]
      }

      set len [llength $list]

      if {$right} {
         set incr -1
         set begin [- $len 1]
         set end -1
      } else {
         set incr 1
         set begin 0
         set end $len
      }

      for {set i $begin} {$i != $end} {incr i $incr} {
         set elem [lindex $list $i]
         set accumulator [{*}$binaryFunction $accumulator $elem]
      }

      return $accumulator
   }

   docProc all {
      -description "Returns true iff all elements in a list satisfy a predicate."
      -args {
         {predicate -description "The predicate to check."}
         {list -description "The list to check."}
      }
   } {
      foreach elem list {
         if {! [{*}$predicate $elem]} {
            return 0
         }
      }
      return 1
   }

   docProc any {
      -description "Returns true iff any element in a list satisfies a predicate."
      -args {
         {predicate -description "The predicate to check."}
         {list -description "The list to check."}
      }
   } {
      foreach elem list {
         if {[{*}$predicate $elem]} {
            return 1
         }
      }
      return 0
   }

   


   namespace export {[a-z]*} £
}


