#include <cpptcl.h>
#include <knapsack.hpp>
#include <vector>
#include <tuple>

Tcl::interpreter ooInterp(NULL);

Tcl::object knapsackCmd(
   const Tcl::object& weightsTcl,
   const Tcl::object& valuesTcl,
   const unsigned weightLimit,
   const double errorMargin) {

   if (! (errorMargin >= 0 && errorMargin < 1)) {
      throw Tcl::tcl_error("The error margin has to be in [0, 1[!");
   }

   const std::vector<unsigned> weights (weightsTcl.make_vector<unsigned>(ooInterp));
   const std::vector<unsigned> values (valuesTcl.make_vector<unsigned>(ooInterp));

   if (weights.size() == 0) {
      throw Tcl::tcl_error("The weights list can't be empty!");
   } else if (values.size() == 0) {
      throw Tcl::tcl_error ("The values list can't be empty!");
   } else if (values.size() != weights.size()) {
      throw Tcl::tcl_error("Weights and values lists must have the same number of entries!");
   }

   return Tcl::object (
      std::get<1>(
         knapsack(
            weights,
            values,
            weightLimit,
            errorMargin
         )
      )
   );

}

extern "C" int Knapsack_Init(Tcl_Interp* cInterp) {
   Tcl_InitStubs(cInterp, "8.6-", 0);
   ooInterp.set(cInterp);

   ooInterp.create_namespace("::knapsack");
   ooInterp.def("::knapsack::KnapsackC++", knapsackCmd);
   return TCL_OK;
}

