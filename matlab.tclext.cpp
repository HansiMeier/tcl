/**************************************************************************************************
 *                                     MATLAB <-> TCL INTEROP                                     *
 **************************************************************************************************/
#include <cpptcl.h>
#include <vector>
#include <mexplus.h>
#include <engine.h>
#include <iostream>
#include <string>

/**************************************************************************************************
 *                                           PROTOTYPES                                           *
 **************************************************************************************************/

// Value conversions
namespace mexplus {
   template <> mxArray* MxArray::from(const ::Tcl::object& obj);
   template <> void MxArray::to(const mxArray* raw, ::Tcl::object* matrixDescriptionListp);
}

// API if created own engine
long OpenEngine();
void CloseEngine(const long engineNumber);
void EvalString(const long engineNumber, const char* cmd);
void PutMatrixDescriptionList(const long engineNumber, const char* name, Tcl::object matrixDescriptionList);
Tcl::object GetMatrixDescriptionList(const long engineNumber, const char* name);

// API using default engine
void eval(const char* cmd);
void put(const char* name, Tcl::object matrixDescriptionList);
Tcl::object get(const char* name);
// make sure default engine is open
void assertDefaultEngine();
void openDefaultEngineNow();

// entry point for tclsh
extern "C" int Matlab_Init(Tcl_Interp* interp);


/**************************************************************************************************
 *                                       VALUE CONVERSIONS                                        *
 **************************************************************************************************/

namespace mexplus {
template <>
   mxArray* MxArray::from(const ::Tcl::object& obj) {

      ::Tcl::interpreter ooInterp (obj.get_interp(), false);
      const size_t length (obj.length(ooInterp));

      if (length < 1) {
         throw ::Tcl::tcl_error ("Matrix-description list wrong!");
      }

      const long matrixSize (length - 1);
      long rows (obj.at(ooInterp, 0).at(ooInterp, 0).get<long>(ooInterp));
      long cols (obj.at(ooInterp, 0).at(ooInterp, 1).get<long>(ooInterp));

      if (cols <= 0 && rows <= 0) {
         throw ::Tcl::tcl_error ("Rows and cols can't be both <= 0!");
      }

      if (cols <= 0) {
         cols = matrixSize/rows;
      }
      if (rows <= 0) {
         rows = matrixSize/cols;
      }

      if (cols * rows != matrixSize) {
         throw ::Tcl::tcl_error ("The dimensions of the matrix don't match the size of the list minus 1!");
      }

      MxArray matrix (MxArray::Numeric<double>(rows, cols));

      long linearPos (1);
      for (long i (0); i != rows; ++i) {
         for (long j (0); j != cols; ++j, ++linearPos) {
            matrix.set(i, j, obj.at(ooInterp, linearPos).get<double>(ooInterp));
         }
      }

      return matrix.release();
   }

template <>
   void MxArray::to(const mxArray* raw, ::Tcl::object* matrixDescriptionListp) {
      ::Tcl::interpreter ooInterp (matrixDescriptionListp->get_interp(), false);

      MxArray matrix (raw);
      const long rows (static_cast<long>(matrix.rows()));
      const long cols (static_cast<long>(matrix.cols()));

      // don't make such a fuss:
      // a 1x1 matrix is treated as a single number
      if (rows == 1 && cols == 1) {
         double doubleVal (matrix.at<double>(mwIndex (0), mwIndex (0)));
         long longVal (doubleVal);

         if (double (longVal) == doubleVal) {
            *matrixDescriptionListp = ::Tcl::object (longVal);
         } else {
            *matrixDescriptionListp = ::Tcl::object (doubleVal);
         }
         return;
      }

      ::Tcl::object dimensions;
      dimensions.append(ooInterp, ::Tcl::object (rows));
      dimensions.append(ooInterp, ::Tcl::object (cols));

      matrixDescriptionListp->append(ooInterp, dimensions);

      for (long i = 0; i != rows; ++i) {
         for (long j = 0; j != cols; ++j) {
            matrixDescriptionListp->append(ooInterp, ::Tcl::object (matrix.at<double>(i, j)));
         }
      }

      return;
   }
}


/**************************************************************************************************
 *                                       BASIC TCL COMMANDS                                       *
 **************************************************************************************************/

long defaultEngine (-1);

// holds all open engines
std::vector<Engine*> engines (15, NULL);

Tcl::interpreter ooInterp (NULL, false);

#define declareCommand(command) \
   ooInterp.def("::matlab::" #command, command);


void assertDefaultEngine() {
   if (defaultEngine == -1) {
      defaultEngine = OpenEngine();
   }
   return;
}

void openDefaultEngineNow() {
   return assertDefaultEngine();
}

long OpenEngine() {
   Engine* engp (engOpen(NULL));
   if (!engp) {
      throw Tcl::tcl_error ("Couldn't open MATLAB engine!");
   }

   // use vector as poor man's hashmap
   // with linear hashing
   long numberOfEngines (engines.size());
   for (long i (0); i != numberOfEngines; ++i) {
      if (!engines[i]) {
         engines[i] = engp;
         return i;
      }
   }
   // everything full, add at end
   engines.push_back(engp);
   return numberOfEngines;
}

void CloseEngine(const long engineNumber) {
   if (engClose(engines[engineNumber])) {
      throw Tcl::tcl_error ("Couldn't close MATLAB engine!");
   }
   engines[engineNumber] = NULL;
   return;
}

void EvalString(const long engineNumber, const char* cmd) {
   if (engEvalString(engines[engineNumber], cmd)) {
      throw Tcl::tcl_error("MATLAB engine invalid!");
   }
   return;
}

void eval(const char* cmd) {
   assertDefaultEngine();
   return EvalString(defaultEngine, cmd);
}

Tcl::object GetMatrixDescriptionList(const long engineNumber, const char* name) {
   mxArray* raw (engGetVariable(engines[engineNumber], name));
   if (!raw) {
      throw Tcl::tcl_error (std::string ("Error getting variable ") + name + " from MATLAB!");
   }
   Tcl::object matrixDescriptionList;
   // Here conversion magic happens, see matlabConversion.cpp
   mexplus::MxArray::to(raw, &matrixDescriptionList);
   mxDestroyArray(raw);
   return matrixDescriptionList;
}

Tcl::object get(const char* name) {
   assertDefaultEngine();
   return GetMatrixDescriptionList(defaultEngine, name);
}

void PutMatrixDescriptionList(const long engineNumber, const char* name, Tcl::object matrixDescriptionList) {
   mxArray* raw (mexplus::MxArray::from(matrixDescriptionList));
   if (engPutVariable(engines[engineNumber], name, raw)) {
      throw Tcl::tcl_error (std::string ("Error putting variable ") + name + " to MATLAB!");
   }
   mxDestroyArray(raw);
   return;
}

void put(const char* name, Tcl::object matrixDescriptionList) {
   assertDefaultEngine();
   return PutMatrixDescriptionList(defaultEngine, name, matrixDescriptionList);
}


/**************************************************************************************************
 *                                              INIT                                              *
 **************************************************************************************************/

void tclExitProc(ClientData cdata) {
   // cleanup the default engine gracefully
   if (defaultEngine != -1) {
      CloseEngine(defaultEngine);
   }
}

extern "C" int Matlab_Init(Tcl_Interp* interp) {

   Tcl_InitStubs(interp, "8.6-", 0);
   ooInterp.set(interp);


   Tcl_CreateExitHandler(&tclExitProc, NULL);

   Tcl_Namespace* nsPtr = ooInterp.create_namespace("matlab");


   declareCommand(OpenEngine);
   declareCommand(CloseEngine);
   declareCommand(EvalString);
   declareCommand(GetMatrixDescriptionList);
   declareCommand(PutMatrixDescriptionList);
   declareCommand(openDefaultEngineNow);
   declareCommand(eval);
   declareCommand(put);
   declareCommand(get);
   ooInterp.export_namespace(nsPtr, "[a-z]+");

   return TCL_OK;
}


