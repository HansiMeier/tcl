#!/usr/bin/env tclsh

if {[info commands addThisScriptDirToPackagePaths] eq ""} {
    proc addThisScriptDirToPackagePaths {} {
        set thisScriptDir [file normalize [file dirname [file readlink [info script]]]]
        if {$thisScriptDir ni $::auto_path} {
            lappend ::auto_path $thisScriptDir
        }
        # already checks if present
        ::tcl::tm::path add $thisScriptDir
    }
}
addThisScriptDirToPackagePaths

package require Tcl 8.6-

