#!/usr/bin/env tclsh

package require Tcl 8.6-
package require Thread


namespace eval niceThreadNumbers {
    if {[tsv::names niceThreadNumbers::*] eq ""} {
        tsv::set niceThreadNumbers::threadNumbersDict dict [dict create INVALID_THREAD_ID -1]
        tsv::set niceThreadNumbers::threadNumbersDict mutex [thread::rwmutex create]
    }


    proc startNiceThreadNumbersFrom number {
        tsv::set niceThreadNumbers::threadNumbersDict dict [dict create INVALID_THREAD_ID [tcl::mathop::- $number 1]]
    }

    proc getNiceThreadNumber {} {
        set mutex [tsv::get niceThreadNumbers::threadNumbersDict mutex]
        thread::rwmutex rlock $mutex
        set mappingDict [tsv::get niceThreadNumbers::threadNumbersDict dict]
        set myThreadId [::thread::id]
        
        set niceThreadNumber -1

        if {[dict exist $mappingDict $myThreadId]} {
            set niceThreadNumber [dict get $mappingDict $myThreadId]
        }

        thread::rwmutex unlock $mutex
        if {$niceThreadNumber == -1} {
            thread::rwmutex wlock $mutex
            set niceThreadNumber [tcl::mathop::+ 1 [tcl::mathfunc::max {*}[dict values $mappingDict]]]
            dict append mappingDict $myThreadId $niceThreadNumber
            tsv::set niceThreadNumbers::threadNumbersDict dict $mappingDict
            thread::rwmutex unlock $mutex
        }


        if {$niceThreadNumber == -1} {
            throw {INVALID_NICE_THREAD_NUMBER} "This shouldn't happen, please fix the bug in this file!"
        }

        return $niceThreadNumber
    }


    namespace export {[a-z]*}
}


