#include <cpptcl.h>
#include <vector>

Tcl::interpreter ooInterp(NULL, false);

#define declareCommand(command) \
    ooInterp.def("::,${extName}::" #command, command);

Tcl::object repeatFourTimes(int a) {
    return Tcl::object(std::vector<int> (4, a));
}

extern "C" int ,[string totitle ,$extName]_Init(Tcl_Interp* interp) {
    Tcl_InitStubs(interp, "8.6-", 0);
    ooInterp.set(interp);

    Tcl_Namespace* nsPtr = ooInterp.create_namespace(",${extName}");

    declareCommand(repeatFourTimes);
    ooInterp.export_namespace(nsPtr, "[a-z]*");

    return TCL_OK;
}

