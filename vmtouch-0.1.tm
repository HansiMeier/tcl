#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval vmtouch {
    variable executable [auto_execok vmtouch]

    proc maybeLoadIntoMemory {file} {
        variable executable
        if {$executable eq ""} {
            return
        }
        exec $executable -q -f -t $file
    }

    proc maybeEvictFromMemory {file} {
        variable executable
        if {$executable eq ""} {
            return
        }

        exec $executable -q -f -e $file
    }

    namespace export {[a-z]*}
}


