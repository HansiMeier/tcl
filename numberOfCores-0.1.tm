#!/usr/bin/env tclsh

package require Tcl 8.6-
package require memoize

namespace eval numberOfCores {

    namespace eval internal {
        proc NumberOfCoresViaEnvVar envVarName {
            return $::env($envVarName)
        }

        proc NumberOfCoresViaGetconf getConfName {
            return [exec getconf $getConfName]
        }
    }

    ::memoize::memoizedProc getNumberOfCores {} {
        switch -nocase -glob -- $::tcl_platform(os) {

            DragonFly -
            *BSD {
                return [internal::NumberOfCoresViaGetconf NPROCESSORS_ONLN]
            }

            Darwin -
            Linux {
                return [internal::NumberOfCoresViaGetconf _NPROCESSORS_ONLN]
            }

            {Windows NT} {
                return [internal::NumberOfCoresViaEnvVar NUMBER_OF_PROCESSORS]
            }

            default {
                throw {NOT_IMPLEMENTED} "Not implemented for the os $::tcl_platform(os)"
            }
        }
    }

    namespace export {[a-z]*}
}
