#!/usr/bin/env tclsh


package require Tcl 8.6-
package require Thread
package require niceThreadNumbers
package require numberOfCores
package require fileutil
package require TclOO


oo::class create inParallel {

    method MakeSureThatThreadpoolUndefined {} {
        variable threadPool
        if {$threadPool ne ""} {
            throw {THREADPOOL_ALREADY_DEFINED} "The threadpool for parallelLoop has already been defined. Make sure that you didn't call createThreadpool or useThreadpool twice!"
        }
    }

    method onlyOnMaster {code} {
        if {$thisThreadNumber == 0} {
            tailcall try $code
        }
    }

    constructor {{numberOfCoresToUse UseAllCores} {initializerScript UseThisScriptAsInitializer}} {
        variable numberOfCoresOfSystem [::numberOfCores::getNumberOfCores]
        variable thisThreadNumber [::niceThreadNumbers::getNiceThreadNumber]
        variable jobsToWaitFor [list]
        variable currentBody [list]
        variable threadPool ""

        my MakeSureThatThreadpoolUndefined

        if {[string equal -nocase $numberOfCoresToUse UseAllCores]} {
            set numberOfCoresToUse $numberOfCoresOfSystem
        }

        if {[string equal -nocase $initializerScript UseThisScriptAsInitializer]} {
            if {$::tcl_interactive} {
                set initializerScript {}
            } else {
                set initializerScript [::fileutil::cat -- [uplevel 1 {info script}]]
            }
        }

        set threadPool [::tpool::create -minworkers $numberOfCoresToUse -maxworkers $numberOfCoresToUse -initcmd $initializerScript]
    }

    method parallelLoop {args} {
        variable threadPool
        variable jobsToWaitFor
        variable currentBody


        set jobsToWaitFor [list]

        set currentBody [lindex $args end]
        set commandPrefix [lrange $args 0 end-1]

        if {$threadPool eq ""} {
            throw {NO_THREADPOOL} "There is no threadpool available: Either create one using parallelLoop::createThreadpool or assign an already created one using parallelLoop::useThreadpool!"
        }

        set myNamespace [self namespace]
        set externalThreadPoolVarName ${myNamespace}::threadPool
        set externalCurrentBodyVarName ${myNamespace}::currentBody
        uplevel 1 [list {*}$commandPrefix [subst -nocommands {
            lappend jobsToWaitFor [::tpool::post -nowait [set $externalThreadPoolVarName] [subst [set $externalCurrentBodyVarName]]]
        }]]


        set resultsOfIterations [list]
        set jobsInOrder [lsort -increasing -integer $jobsToWaitFor]

        # Not the same as ::tpool::wait $threadPool $jobToWaitFor because that waits for _any_ job to have finished, not _all_
        foreach job $jobsInOrder {
            ::tpool::wait $threadPool $job
            lappend resultsOfIterations [::tpool::get $threadPool $job]
        }

        set jobsToWaitFor [list]
        set currentBody [list]

        return $resultsOfIterations
    }

    method do args {
        tailcall ::tpool::post $threadPool {*}$args
    }

    method parallelMap {commandPrefix elements} {
        tailcall my parallelLoop foreach element $elements [list {*}$commandPrefix \$element]
    }

    destructor {
        variable threadPool
        ::tpool::release $threadPool
    }

}
