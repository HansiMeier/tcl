#!/usr/local/bin/tclsh
package require Tcl 8.5 ;# for dicts
package require httpFollow
package require fileutil ;# for tempfile

set URL {http://www.stickydillybuns.com/}
set LINUX_VIEWER {eom} ;# needed because xdg-open works strangely on Linux (at least mine)
set STRIPFILE [::fileutil::tempfile]



if {! [info exists ::env(PATH)]} {
   set ::env(PATH) {/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin}
   puts $::env(PATH)
}


proc fatalError {msg txt} {
   puts stderr $txt
   exit 1
}

interp alias {} signaliseError {} throw

# also changes $file!
proc renameAddingExtension fileVar {

   upvar 1 $fileVar file

   if {! [file readable $file]} {
      fatalError {REN NOT READABLE} "Wanted to rename \"$file\", but it isn't readable!"
   }

   try {
      package require fileutil
   } on error _ {
      puts stderr "Package \"fileutil\" not found, not correcting the extension of \"$file\"."
      return
   }

   set recognised [string tolower [join [::fileutil::fileType $file] ""]]
   set newExt ""
   foreach ext {png jpeg gif} {
      if {[string first $ext $recognised] != -1} {
         set newExt $ext
      }
   }

   if {[string equal $newExt ""]} {
      puts stderr "Could not recognise type of \"$file\" for renaming, leaving it be!"
      return
   }

   # Remove any existing file extension from the file
   regsub {\.[^\.]+$} $file {} file
   set newFile $file.$newExt

   puts stdout "Renaming \"$file\" to \"$newFile\"."
   file rename -force $file $newFile
   set file $newFile
   return
}

proc getRealStripUrl htmlVar {
   upvar $htmlVar html
   set isoDate [clock format [clock seconds] -format %Y%m%d]
   set exp [
      subst -nocommands \
         {<img src="(http://www.stickydillybuns.com/comics/sdb$isoDate[^\.]*\.png)"}
   ]
   if {! [regexp -line -- $exp $html _ realAddress]} {
      fatalError {REGEXP STRIP FAILED} "Looking for the strip location of today's strip failed!\nMaybe it doesn't exist yet."
   }
   puts stderr "Retrieving sdb strip location \"$realAddress\""
   return $realAddress
}

proc which {prog} {
   set absProg [auto_execok $prog]
   if {$absProg eq ""} {
      signaliseError {NOT_IN_PATH} "\"$prog\" not in \$PATH"
   }
   return $absProg
}

proc openWith file {
   global OPENPROG \
          LINUX_VIEWER
   if {! [info exists OPENPROG]} {
      if {[catch {set OPENPROG [which $LINUX_VIEWER]}] && [catch {set OPENPROG [which "open"]}]} {
         signaliseError {NO WHICH} "No suitable open program found!"
      }
   }
   puts "Opening \"$file\" with \"$OPENPROG\""
   exec $OPENPROG $file &
}

proc killViewerOnLinux {} {
   global LINUX_VIEWER \
          tcl_platform
   if {! [string equal $tcl_platform(os) {Linux}]} {
      return
   }
   puts "We're on Linux, killing \"$LINUX_VIEWER\" if running!"
   catch {exec killall $LINUX_VIEWER}
}

proc closeXeeWindows {} {
   global tcl_platform
   if {! [string equal $tcl_platform(os) Darwin]} {
      return
   }

   # No space after Xee³ because iTerm already outputs one (bug?)
   puts stderr {We're on OSX, use ugly GUI AppleScript to close windows of Xee³if any are open.}

   exec osascript -e {
      if application "Xee³" is running then
         tell application "System Events"
            set allWindows to name of window of process "Xee³"
            repeat with i in allWindows
               click button 1 of window i of process "Xee³"
            end repeat
         end tell
      end if
   } >& /dev/null
   return
}

proc downloadToFile {url file} {
   if {[catch {set token [::http::geturl $url -follow 10]}]} {
    signaliseError {GET_FAILED} "Error retrieving \"$url\"!"
   }

   if {[::http::ncode $token] != 200} {
      throw {HTTP NOT OK} "Error retrieving \"$url\", code [::http::ncode $token]!"
   }

   set html [::http::data $token]
   set realUrl [getRealStripUrl html]
   ::http::cleanup $token

   if {[catch {set token [::http::geturl $realUrl]}]} {
    fatalError {GET_FAILED} "Error retrieving \"$realUrl\"!"
   }

   puts stderr "Downloading \"$realUrl\" to \"$file\""
   set fd [open $file w]
   chan configure $fd -translation binary
   puts -nonewline $fd [::http::data $token]
   close $fd
   ::http::cleanup $token
}

proc main {url file} {
   downloadToFile $url $file
   renameAddingExtension file ;# also changes $file variable
   killViewerOnLinux
   closeXeeWindows
   openWith $file
}
main $URL $STRIPFILE

