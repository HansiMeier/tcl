#!/usr/bin/env tclsh

# MODULE DESCRIPTION
# This Module parses GNU-style options like -s, --longWithArgument arg
# or even --longWithArg=arg. Several short options can be concatenated
# using only one dash but only the last may get any arguments.
# "--" is used to denote the end of switches.
# On syntax errors it prints a nice usage message.
# A default -h/--help switch is added automatically if none is provided.
#
# USAGE
#
# Option lists
# Options can be defined in lists, a single option consisting of
# {optionName shortOpt ?longOpt? ?numberOfArgs? ?required? ?Usage? ?argsDescription?}
# for instance {verbosity v verbose 0 0 "Print out every detail" ""}
# or {inputFile f file 1 1} or {help h}
#
# Thus, a valid option list is
# {{verbosity v verbose 0 0 "Print out every detail"} {inputFile f file 1 1}}

#
# shortOpt and longOpt are given without dashes and can be empty.
#
# defaults
# longOpt {}
# numberOfArgs 0
# required 0
# usage {}
#
# special values
# numberOfArgs < 0: unlimited arguments, can be used just once
# there can be one option without longOpt or shortOpt:
# all arguments which can't be assigned to any other option are assigned to this one.
# If such an option doesn't exists an error is thrown.
#
# Option dicts
# Alternative to option lists, with the same rules, see:
#
# set optDict [dict create \
#	verbosity [dict create \
#		shortOpt v \
#		longOpt verbose] \
#	inFiles [dict create \
#		longOpt files
#		shortOpt f
#		numberOfArgs -1]] ;# -1 means unlimited args
#
# Processing arguments
# The only needed proc is processArgs, with syntax
# processArgs ?-noArray? argv0 argv argc optSomething ?generalUsage?
#
# optSomething is either an option list or an option dict
# generalUsage is used to print usage descriptions (default is quite good)
#
# processArgs returns a modified optDict (and modifies the original one if optSomething is a dict)
# where the following keys are added to each sub dict:
# 'present': bool, was the option given in argv?
# 'passedArgs': list, passed args, (e.g. the input files)
#
# additionally, if -noArray is NOT set:
# creates arrays from the sub dicts in the namespace ::parseGnuOpts::arrays for easier access
# e.g. an array inFiles(present), inFiles(shortOpt) ...
# and exports a function:
# importOptArrays
# which imports the created arrays in the current namespace
# and does a variable command on the name in the current namespace for easy access in a proc.
# (see code)
#
#
# EXAMPLES
#
# package require parseGnuOpts
# namespace import ::parseGnuOpts::*
#
# proc main {argv0 argv argc} {
#	set optDict \
#		[dict create \
#			inFiles \
#				[dict create \
#					shortOpt f \
#					longOpt files \
#					numberOfArgs -1 \
#					required 1 \
#					usage "Input files" \
#					argsDescription [list file1 file2...]]]
#	# or equivalent
#	set optList \
#		{{inFiles f files	-1 1 "Input files"}}
#
#	processArgs \
#		$argv0 \
#		$argv \
#		$argc \
#		$optList \
#		"This program does nothing"
#
#	importOptArrays ;# import nice arrays
#
#	foreach inFile $inFiles(passedArgs) {
#		puts "Input: $inFile"
#	}
# }
#
# proc main2 {argv0 argv argc} {
#	set optList {{inFiles "" "" -1 1}}
#	processArgs \
#		$argv0 \
#		$argv \
#		$argc \
#		$optList \
#		"This program does nothing"
#
#	importOptArrays ;# import nice arrays
#
#	foreach file $inFiles(passedArgs) {
#		puts "Input: $file"
#	}
#	}
#	# call:
#	main2 $argv0 "file1 file2" 2
#	# outputs:
#	# Input: file1
#	# Input: file2

package require Tcl 8.6-

namespace eval parseGnuOpts {

    proc ? {condition ifBranch elseBranch} {
        tailcall \
            if $condition \
            [list subst $ifBranch] \
            else \
            [list subst $elseBranch]
    }

    variable debugOn 0

    # Whether to save single arguments as pure strings
    # Disabled by default as sensitive to spaces
    variable saveSingleArgsAsPureStrings 0

    if {$debugOn} {
        proc debugPuts message {
            puts stderr "$message"
        }
    } else {
        proc debugPuts message {}
    }

    proc signaliseErrorToUser {name description} {
        variable debugOn
        if {$debugOn} {
            throw $name $description
        } else {
            puts stderr "ERROR: $description\n"
            displayUsage
            exit 1
        }
    }

    # list format: {optionName shortOpt ?longOpt? ?numberOfArgs? ?required? ?Usage? ?argsDescription?}
    proc convertOptListToOptDict list {
        set optDict [dict create]

        foreach opt $list {
            set len [llength $opt]

            # Check which options are given,
            # add sensible defaults
            if {$len <= 6} {
                set argsDescription [list]
            } else {
                set argsDescription [lindex $opt 6]
            }
            if {$len <= 5} {
                set usage {}
            } else {
                set usage [lindex $opt 5]
            }
            if {$len <= 4} {
                set required 0
            } else {
                set required [lindex $opt 4]
            }
            if {$len <= 3} {
                set numberOfArgs 0
            } else {
                set numberOfArgs [lindex $opt 3]
            }
            if {$len <= 2} {
                set longOpt {}
            } else {
                set longOpt [lindex $opt 2]
            }
            if {$len <= 1} {
                throw {BAD OPTLIST} "Name and ShortOpt must be given!"
            }

            set name [lindex $opt 0]
            set shortOpt [lindex $opt 1]

            # Compile a dict
            dict set optDict $name [dict create \
                shortOpt $shortOpt \
                longOpt $longOpt \
                numberOfArgs $numberOfArgs \
                usage $usage \
                required $required \
                present 0 \
                passedArgs {} \
                argsDescription $argsDescription]
        }
        return $optDict
    }

    # add sensible defaults to user provided optDicts
    proc addDefaultsToOptDict optDictVar {
        upvar 1 $optDictVar optDict
        foreach pair {{longOpt {}} {numberOfArgs 0} {required 0} {usage {}} {present 0} {passedArgs {}} {argsDescription {}}} {
            set property [lindex $pair 0]
            set defaultValue [lindex $pair 1]

            dict for {name subDict} $optDict {
                if {! [dict exists $subDict $property]} {
                # Add default value
                    dict set optDict $name $property $defaultValue
                }
            }
        }

        # add help if not present
        if {! [dict exists $optDict help]} {
            dict set optDict help [dict create \
                shortOpt h \
                longOpt help \
                numberOfArgs 0 \
                usage {Display this help.} \
                required 0 \
                present 0 \
                argsDescription [list] \
                passedArgs {}]
        }
    }

    # validate the option dict created with the function before
    proc checkOptDict optDict {
        set nameList {}
        set longOptList {}
        set shortOptList {}
        set unlimitedArgs 0
        dict for {name props} $optDict {
            foreach prop {shortOpt longOpt usage required numberOfArgs passedArgs present argsDescription} {
                if {! [dict exists $props $prop]} {
                    throw {NO PROP} "Property \"$prop\" not found!"
                }
                set $prop [dict get $props $prop]
            }

            # Check type errors
            if {[string length $shortOpt] > 1} {
                throw	{LONG SHORTOPT} "The shortOpt of $name, $shortOpt, is longer than a single character!"
            }
            if {! [string is entier $numberOfArgs]} {
                throw {NUMBEROFARGS NO NUMBER} "numberOfArgs of $name, $numberOfArgs, has to be an integer!"
            }

            if {! [string is bool $required]} {
                throw {REQUIRED NO BOOL} "required of $name, $required, has to be a bool!"
            }

            set numberOfArgsDescribed [llength $argsDescription]
            if {$numberOfArgsDescribed > 0 && $numberOfArgsDescribed < $numberOfArgs} {
                throw {TOO FEW ARGS DESCRIBED} "Only $numberOfArgsDescribed of $numberOfArgs arguments have been described in $name!"
            }

            #
            lappend shortOptList $shortOpt
            lappend longOptList $longOpt
            lappend nameList $name
        }


        # Check that options are unique
        set length [llength $nameList] ;# All lengths are equal
        set encounteredNull ""
        for {set i 0} {$i < $length} {incr i} {
            foreach propVar {name shortOpt longOpt} {
                set $propVar [lindex [set ${propVar}List] $i]
                set ${propVar}Len [expr {[set $propVar] ne ""}]
            }

            if {! $nameLen} {
                throw {EMPTY NAME} "Empty names are disallowed!"
            }

            if {! ($shortOptLen || $longOptLen)} {
                set newlyEncounteredNull $name
                if {$encounteredNull ne ""} {
                    throw {MORE THAN ONE DOUBLENULL} "There can be only one argument without shortOpt and longOpt but here we have \"$encounteredNull\" and \"$newlyEncounteredNull\"."
                }
                set encounteredANull $newlyEncounteredNull
            }

            # Take note of every seen name, shortOpt, and longOpt
            # throw if seeing anything for the second time
            foreach propVar {name shortOpt longOpt} {
            # Having either no shortOpt or no longOpt is OK
                if {! [set ${propVar}Len]} {
                    continue
                }
                incr alreadySeen${propVar}([set $propVar]) 1
                if {[set alreadySeen${propVar}([set $propVar])] != 1} {
                    throw {NON-UNIQUE OPT} "[set $propVar], a $propVar, is not unique!"
                }
            }
        }
    }

    # looks up opt in optDictVar, returns a dict with the names of the ones found
    # and the not found options
    proc lookupOpt {opt optDictVar} {
        set notFound {}
        set names {}

        upvar 1 $optDictVar optDict

        if {! [regexp {^--(.*)$} $opt _ opt]} {
            if {! [regexp {^-(.*)$} $opt _ opt]} {
                return [dict create \
                    names {} \
                    notFound {} \
                    noDashes $opt]
            }
            set optType shortOpt
        } else {
            set optType longOpt
        }

        # Handle concatenated short options like -hf correctly
        if {[string equal $optType shortOpt]} {
            set opt [split $opt ""]
        }

        # Lookup Option(s)
        foreach currentOpt $opt {
            set found 0
            dict for {name props} $optDict {
                if {[string equal [dict get $props $optType] $currentOpt]} {
                    dict set optDict $name present 1
                    lappend names $name
                    set found 1
                    break
                }
            }
            if {! $found} {
                lappend notFound $currentOpt
            }
        }

        # only return unique names
        set uniqueNames {}
        foreach name $names {
            if {[lsearch $uniqueNames $name] == -1} {
                lappend uniqueNames $name
            }
        }

        return [dict create \
            names $uniqueNames \
            notFound $notFound \
            noDashes {}]
    }

    proc embellishOption {shortOpt longOpt usage} {
        set delim {}
        if {[llength $shortOpt]} {
            set shortOpt -$shortOpt
            if {[llength $longOpt]} {
                set longOpt "--$longOpt"
                set delim " | "
            }
        } elseif {[llength $longOpt]} {
            set longOpt --$longOpt
        }

        set optionDescription "Unknown option"
        foreach candidate [list ${shortOpt}${delim}${longOpt} $usage] {
            if {$candidate ne ""} {
                set optionDescription $candidate
                break
            }
        }

        return [list $shortOpt $longOpt $optionDescription]
    }

    # check that all options have the correct number of passed arguments
    proc checkOpts optDict {
        set mismatchesList {}
        set requiredList {}
        dict for {name props} $optDict {
            set requiredNumberOfArgs [dict get $props numberOfArgs]
            set numberOfPassedArgs [llength [dict get $props passedArgs]]
            set isRequired [dict get $optDict $name required]
            set isPresent [dict get $optDict $name present]

            # Check for argument mismatch
            if {$isPresent && ($requiredNumberOfArgs >= 0 \
                    && $requiredNumberOfArgs != $numberOfPassedArgs) \
                    || ($requiredNumberOfArgs < 0 \
                && $numberOfPassedArgs < 1)} {
                lappend mismatchesList [list $name $numberOfPassedArgs $requiredNumberOfArgs \
                    [dict get $optDict $name passedArgs]]
            }

            # Check for a required argument not present
            if {$isRequired && ! $isPresent} {
                lappend requiredList $name
            }
        }

        if {[llength $requiredList] || [llength $mismatchesList]} {
            set errors {}
            foreach mismatches $mismatchesList {
                set name [lindex $mismatches 0]
                set numberOfArgs [lindex $mismatches 2]
                if {$numberOfArgs < 0} {
                    set numberOfArgs "at least 1"
                }
                set numberOfPassedArgs [lindex $mismatches 1]
                set passedArgs [lindex $mismatches 3]
                set shortOpt [dict get $optDict $name shortOpt]
                set longOpt [dict get $optDict $name longOpt]
                set usage [dict get $optDict $name usage]

                lassign [embellishOption $shortOpt $longOpt $usage] shortOpt longOpt optionDescription

                lappend errors \
                    "Argument mismatch in option \"$optionDescription\": got arguments \"$passedArgs\", which are $numberOfPassedArgs, but need $numberOfArgs!"
            }

            foreach required $requiredList {
                set shortOpt [dict get $optDict $required shortOpt]
                set longOpt [dict get $optDict $required longOpt]
                set usage [dict get $optDict $required usage]

                lassign [embellishOption $shortOpt $longOpt $usage] shortOpt longOpt optionDescription


                lappend errors \
                    "Option \"${optionDescription}\" is required but not present!"
            }

            set errors [lsort -unique $errors]
            if {![dict get $optDict help present]} {
                foreach error $errors {
                    puts stderr "ERROR: $error"
                }
            }
            return 0
        }
        return 1
    }

    proc parseArgs {argv argc optDictVar} {
        upvar 1 $optDictVar optDict
        variable saveSingleArgsAsPureStrings

        set noDashesList {}
        for {set i 0} {$i < $argc} {incr i 1} {
            set opt [lindex $argv $i]

            # handle -- correctly
            if {$opt eq "--"} {
                set noDashesList [list {*}$noDashesList {*}[lrange $argv [expr {$i+1}] [expr {$argc-1}]]]
                break
            }


            # Handle --long=arg correctly
            if {[regexp {([^= ]+)=(.+)} $opt _ opt args]} {
                set argv [lreplace $argv $i $i $opt $args]
            }

            # Display Help if required
            if {[dict get $optDict help present]} {
                displayUsage
                exit 1
            }

            set res [lookupOpt $opt optDict]

            set noDashes [dict get $res noDashes]

            # Option without Dashes?
            if {[string length $noDashes] > 0} {
                lappend noDashesList $noDashes
                continue
            }

            # Anything not found?
            if {[llength [dict get $res notFound]]} {
                signaliseErrorToUser {UNKNOWN OPT} \
                    "Option(s) [join [dict get $res notFound] {, }] not found!"
            }

            # Check arguments
            set name [lindex [dict get $res names] end]
            set requiredNumberOfArgs [dict get $optDict $name numberOfArgs]


            # handle unlimited number of arguments
            if {$requiredNumberOfArgs < 0} {
            # eat all arguments
                set upper [expr {$argc - 1}]
            } else {
                set upper [expr {$i + $requiredNumberOfArgs}]
            }

            set nextArgList {}
            for {set j [expr {$i + 1}]} {$j <= $upper} {incr j} {
                set nextArg [lindex $argv $j]
                set optionRegexp {-[^-]+}
                if {[regexp ^$optionRegexp $nextArg]} {
                    puts stderr "\"$nextArg\" looks like an option, but is used as an argument to an option!"
                    puts stderr "Use \\ to pass an argument to an option satisfying regexp \"$optionRegexp\"!"
                    break
                } elseif {[string equal $nextArg {}]} {
                    break
                }
                if {[regsub ^\u005c\u005c($optionRegexp) $nextArg {\1} nextArg]} {
                     puts stderr {Stripping first backslash}
                }
                lappend nextArgList $nextArg

            }

            # Save single arguments as pure strings
            if {$saveSingleArgsAsPureStrings && [llength $nextArgList] == 1} {
                debugPuts "Single arg"
                dict set optDict $name passedArgs [join $nextArgList ""]
            } else {
                debugPuts "Passed args for $name is \"$nextArgList\""
                dict set optDict $name passedArgs $nextArgList
            }
            set i [expr {$j - 1}]
        }

        if {[llength $noDashesList]} {
            dict for {name props} $optDict {
                if {! [llength [dict get $props shortOpt]]} {
                    debugPuts "Setting $name to \"$noDashesList\" (length [llength $noDashesList])"
                    dict set optDict $name passedArgs $noDashesList
                    dict set optDict $name present 1
                    return
                }
            }
            signaliseErrorToUser {UNKNOWN ARGS} "The arguments [join $noDashesList {, }] have no leading dash but couldn't be assigned to any defined option!"
        }
    }

    proc generateDisplayUsage {generalUsage optDict argv0 additionalText} {
        proc displayUsage {} "
        displayUsageHelper {$generalUsage} {$optDict} {$argv0}
        puts stderr {$additionalText}
        "
    }

    proc displayUsageHelper {generalUsage optDict argv0} {
        puts stderr HELP\n
        if {[llength $generalUsage]} {
            puts stderr $generalUsage\n
        }

        set overviewStr {}
        dict for {name props} $optDict {
            if {[dict get $props required]} {
                set opening <
                set closing >
            } else {
                set opening {[}
                set closing {]}
            }

            foreach propVar {shortOpt longOpt usage argsDescription} {
                set $propVar [dict get $props $propVar]
                set ${propVar}Len [expr {![string equal [set $propVar] {}]}]
            }

            set toAdd {}
            if {$shortOptLen} {
                append toAdd " -$shortOpt"
            }
            if {$shortOptLen && $longOptLen} {
                append toAdd " |"
            }
            if {$longOptLen} {
                append toAdd " --$longOpt"
            }

            set argsDescriptionString [string trim [join $argsDescription " "]]
            if {$argsDescription ne ""} {
                if {$shortOptLen || $longOptLen} {
                    set prefix ": "
                } else {
                    set prefix ""
                }
                set argsDescriptionString "$prefix$argsDescriptionString"
            }
            set overviewStr "$overviewStr $opening[string trim $toAdd]$argsDescriptionString$closing"
        }

        puts stderr "Usage: [file tail $argv0] $overviewStr\n"
        puts stderr OPTIONS\n

        dict for {name props} $optDict {
            foreach propVar {shortOpt longOpt usage required numberOfArgs argsDescription} {
                set $propVar [dict get $props $propVar]
                set ${propVar}Len [expr {![string equal [set $propVar] {}]}]
            }

            set didOutputArgumentsDescription 0
            if {!($shortOptLen || $longOptLen)} {
                set argsDescriptionString [join $argsDescription " "]
                if {$argsDescription ne ""} {
                    puts -nonewline stderr $argsDescriptionString
                }
                set didOutputArgumentsDescription 1
            }
            if {$shortOptLen} {
                puts -nonewline stderr -$shortOpt
            }
            if {$shortOptLen && $longOptLen} {
                puts -nonewline stderr { | }
            }
            if {$longOptLen} {
                puts -nonewline stderr --$longOpt
            }
            if {$usageLen} {
                puts stderr "\n   $usage"
            } else {
                puts stderr {}
            }
            if {$numberOfArgs && !$didOutputArgumentsDescription} {
                set argumentsForm arguments
                if {$numberOfArgs < 0} {
                    set numberOfArgs {at least 1}
                } elseif {$numberOfArgs == 1} {
                    set argumentsForm argument
                }
                set argsDescriptionString [join $argsDescription " "]
                if {$argsDescription ne ""} {
                    set argsDescriptionString ": $argsDescriptionString"
                }
                puts stderr "   Needs $numberOfArgs $argumentsForm$argsDescriptionString."
            }
            if {$required} {
                puts stderr "   Is required."
            } else {
                puts stderr "   Is optional."
            }
            puts stderr {}
        }
    }




    # processArgs ?-noArray? argv0 argv argc optSomething ?generalUsage? ?notesCmd?
    proc processArgs args {

        set len [llength $args]

        # correct number Of Arguments?
        if {$len < 4 || $len > 7} {
            return -code error \
                "wrong # args: processArgs ?-noArray? argv0 argv argc optSomething ?generalUsage? ?notesCmd?
            where optSomething is a list of option descriptions as follows:
            {optionName shortOpt ?longOpt? ?numberOfArgs? ?required? ?Usage?}"
        }

        # -noArray present
        set noArray [string equal -nocase [lindex $args 0] -noArray]
        lassign [lrange $args $noArray end] argv0 argv argc optSomething generalUsage additionalText

        # Check heuristically if optSomething is an optDict
        try {
            set isOptDict [dict exists [dict get $optSomething [lindex [dict keys $optSomething] 0]] shortOpt]
        } on error err {
            set isOptDict 0
        }

        set optDict [? {$isOptDict} {$optSomething} {[convertOptListToOptDict $optSomething]}]
        addDefaultsToOptDict optDict

        checkOptDict $optDict

        # Generate displayUsage
        generateDisplayUsage $generalUsage $optDict $argv0 $additionalText

        # process the arguments
        parseArgs $argv $argc optDict

        # Check if everything is fine
        if {! [checkOpts $optDict]} {
            puts stderr ""
            displayUsage
            exit 1
        }

        # set array for convenience
        # if allowed to do it
        if {!$noArray} {
            namespace eval ::parseGnuOpts::arrays {}
            dict for {name subDict} $optDict {
                dict for {prop val} $subDict {
                    array set ::parseGnuOpts::arrays::$name [list $prop $val]
                }
            }

            # Make all these variables accessible
            proc importOptArrays {} [ subst {
                tailcall foreach key \[list [dict keys $optDict]\] {
                    upvar 0 ::parseGnuOpts::arrays::\$key ::\[namespace current\]::\$key
                    # now accessible in the current namespace as ::namespace::key
                    variable \$key
                    # now accessible in the current procedure simply as key
                }
            }]

            namespace export importOptArrays
            # if already imported processArgs, also import importOptArrays
            set qualProcName [dict get [info frame 0] proc]
            set procName [namespace tail $qualProcName]
            uplevel 1 "
            if {\[lsearch \[namespace import] {$procName}] != -1 \\
                && \[string equal \[namespace origin {$procName}] {$qualProcName}]} {
                namespace import {::parseGnuOpts::importOptArrays}
            }
            "
        }
        return $optDict
    }
    namespace export processArgs
} ;# namespace finished
