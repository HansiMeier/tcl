#!/usr/bin/env tclsh
package require TclOO


namespace eval tee {
    ::oo::class create tee {
        variable fileHandle_M isOpen_M openOption_M toOpen_M

        constructor {args} {
            set numberOfArgs [llength $args]
            set openOption_M w+
            set isOpen_M 0
            set toOpen_M ""


            if {$numberOfArgs ni {1 2}} {
                error "Wrong # args for constructor of tee: ?-append? fileNameOrHandle"
            }

            if {$numberOfArgs == 2} {
                set appendOption [lindex $args 0]
                if {[string first [string tolower $appendOption] {-append}] != -1} {
                    set openOption_M a+
                } else {
                    error "Illegal option $appendOption: must be \"-append\""
                }
            }

            set fileNameOrHandle [lindex $args end]


            if {$fileNameOrHandle in [chan names $fileNameOrHandle]} {
                set fileHandle_M $fileNameOrHandle
                set isOpen_M 1
            } else {
                set toOpen_M $fileNameOrHandle
            }

        }

        destructor {
            my finalize dummyArgument
        }

        method makeSureIsOpen {} {
            if {!$isOpen_M} {
                set fileHandle_M [open $toOpen_M $openOption_M]
                set isOpen_M 1
            }
        }

        ####################################################################################################
        #                                   GENERAL TRANSCHAN OPERATIONS                                   #
        ####################################################################################################

        method finalize handle {
            if {$isOpen_M} {
                catch {chan close $fileHandle_M}
                set isOpen_M 0
            }
        }

        method initialize {handle mode} {
            return {finalize initialize read flush write}
        }

        ####################################################################################################
        #                                    READ TRANSCHAN OPERATIONS                                     #
        ####################################################################################################

        method read {handle buffer} {
            my makeSureIsOpen
            return $buffer
        }

        ####################################################################################################
        #                                    WRITE TRANSCHAN OPERATIONS                                    #
        ####################################################################################################

        method flush handle {
            my makeSureIsOpen
            chan flush $fileHandle_M
        }

        method write {handle buffer} {
            my makeSureIsOpen
            chan puts -nonewline $fileHandle_M $buffer
            return $buffer
        }
    }
}
