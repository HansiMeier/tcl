package require Tcl 8.6-

# using this cumbersome glob to work around OSX's decomposed unicode,
# which tcl can't handle
if {[string equal -nocase $tcl_platform(os) Darwin]} {
   package require unicode ;# to handle OSX's decomposed unicode in osxGlob

   namespace eval osxGlob {

      if {[info commands ::osxGlob::theRealGlob] eq {}} {
         rename glob ::osxGlob::theRealGlob
         proc osxGlob args {
            # decompose the pattern to make theRealGlob work on HFS+
            set decomposedPattern [::unicode::normalizeS D [lindex $args end]]
            set results [::osxGlob::theRealGlob {*}[lrange $args 0 end-1] $decomposedPattern]

            # Compose the decomposed results, so that TCL doesn't choke on them
            return [lmap decomposedResult $results {::unicode::normalizeS C $decomposedResult}]
         }
         rename ::osxGlob::osxGlob ::glob
      }
   }

}
