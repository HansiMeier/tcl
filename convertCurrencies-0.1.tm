#!/usr/bin/env tclsh

package require Tcl 8.6-
package require http
package require json

namespace eval currencylayer {
   variable ApiKey {32e46d42d2e1acfe695602f5f95cd739}
   variable ApiEntry "http://apilayer.net/api/live?access_key=$ApiKey"
   variable BaseCurrency USD
   variable Cache [dict create $BaseCurrency 1]

   proc SaveRates to {
      variable ApiEntry
      variable FromCurrency
      variable Cache
      variable BaseCurrency

      set cleanTo [list]
      foreach c $to {
         lappend cleanTo [string toupper $c]
      }
      set to $cleanTo

      set token [::http::geturl ${ApiEntry}&currencies=[join $to ,]]
      set httpCode [::http::ncode $token]
      if {$httpCode != 200} {
         throw {invalid http code} "Failed with HTTP Code $httpCode"
      }
      
      set resp [::json::json2dict [::http::data $token]]

      if {![dict get $resp success]} {
         throw {invalid response} "API failed with error \"[dict get $resp error info]\""
      }

      if {$BaseCurrency ne [dict get $resp source]} {
         throw {invalid source currency} "The Base currency has changed!"
      }

      set quotes [dict get $resp quotes]
      foreach t $to {
         dict set Cache $t [dict get $quotes ${BaseCurrency}${t}]
      }
      ::http::cleanup $token
      return
   }

   proc GetRate to {
      variable Cache
      if {![dict exists $Cache $to]} {
         ::currencylayer::SaveRates $to
      }

      tailcall dict get $Cache $to
   }
}

proc convertCurrencies {from to amount} {
   set from->base [::currencylayer::GetRate $from]
   set base->to [::currencylayer::GetRate $to]

   tailcall expr [list double(${base->to}) / ${from->base} * $amount]
}
