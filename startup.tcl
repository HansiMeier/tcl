# things to export to any interactive shell

namespace eval Interactive {
    variable ReloadScript {}
    variable Packages [package names]


    proc reload {} {
        uplevel 1  $::Interactive::ReloadScript
    }
    interp alias {} ? {} ::Interactive::reload

    proc mySource thing {
        if {[file readable $thing]} {
            set ::Interactive::ReloadScript [list source $thing]
        } else {
            set ::Interactive::ReloadScript [list package require $thing]
        }
        tailcall ::Interactive::reload
    }
    interp alias {} s {} ::Interactive::mySource

    proc whichReload {} {
        puts stdout $::Interactive::ReloadScript
        return
    }
    interp alias {} ?? {} ::Interactive::whichReload
}

cd "$::env(HOME)/tcl"
interp alias {} q {} exit
namespace import ::tcl::mathfunc::*
namespace import ::tcl::mathop::*
