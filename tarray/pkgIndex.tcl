if {![package vsatisfies [package provide Tcl] 8.5]} {return}
package ifneeded tarray 0.7 [list ::apply {dir {
    source [file join $dir critcl-rt.tcl]
    set path [file join $dir [::critcl::runtime::MapPlatform]]
    set ext [info sharedlibextension]
    set lib [file join $path "tarray$ext"]
    load $lib Tarray
    ::critcl::runtime::Fetch $dir tarray.tcl
    package provide tarray 0.7
}} $dir]
