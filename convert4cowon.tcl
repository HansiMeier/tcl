#!/usr/bin/env tclsh

####################################################################################################
#                                      LOAD STANDARD PACKAGES                                      #
####################################################################################################

set requiredStandardPackages [list {Tcl 8.6-} Thread]
foreach packageDescription $requiredStandardPackages {
    package require {*}$packageDescription
}


####################################################################################################
#                        DETERMINE WHETHER THIS THREAD IS THE MASTER THREAD                        #
####################################################################################################

if {![info exists iAmTheMaster]} {
    set iAmTheMaster 1
}


####################################################################################################
#                                       LOAD OTHER PACKAGES                                        #
####################################################################################################


# Add the directory of this script to the package paths
if {$iAmTheMaster} {
    # Worker threads have no access to info script
    set myself [info script]
    if {[file type $myself] eq "link"} {
        set myself [file readlink $myself]
    }
    set thisScriptDir [file normalize [file dirname $myself]]
    ::tsv::set masterScriptDir dir $thisScriptDir
    ::tsv::set error anyError 0
} else {
    set thisScriptDir [::tsv::get masterScriptDir dir]
}

lappend ::auto_path $thisScriptDir
::tcl::tm::path add $thisScriptDir


set additionalRequiredPackages [list osxGlob fileutil parseGnuOpts numberOfCores niceThreadNumbers semanticVersioning capitalizeWords]
foreach packageDescription $additionalRequiredPackages {
    package require {*}$packageDescription
}


####################################################################################################
#                                         GLOBAL VARIABLES                                         #
####################################################################################################

array set defaultTarget {format mp3 mediainfoType "MPEG Audio" mimeType audio/mpeg bitrateInKbps 128 bitrateMode VBR VBRQuality 5 ffmpegEncoder libmp3lame okTags {Performer Track Album}
additionalFfmpegFlags {-map_metadata 0 -id3v2_version 3}}

if {$iAmTheMaster} {
    array set target [array get defaultTarget]
    # target is modified according to commnd line options
    # copied to tsv in main routine
} else {
    # load target options from master
    array set target [::tsv::array get masterTarget]
}

set debugOn 0

set extensionsOfFilesToConvert [list mp3 ogg flac m4a wma aac]
set requiredPrograms [list mp3gain ffmpeg eyeD3]

set finalDir [file join $env(HOME) konvertiert]

set niceThreadNumber [::niceThreadNumbers::getNiceThreadNumber]

set mediainfoTagsToId3 [dict create Performer TPE1 Track TIT2 Album TALB]


####################################################################################################
#                                          UTILITY PROCS                                           #
####################################################################################################

proc debugVal var {
    global niceThreadNumber
    if {$::debugOn} {
        upvar 1 $var toOutput
        tsv::lock ioLock [list puts stderr "#$niceThreadNumber: $var = `$toOutput'"]
    }
}

proc log {message} {
    global niceThreadNumber
    tsv::lock ioLock [list puts stderr "#$niceThreadNumber: $message"]
}

proc fatalError {message} {
    log $message
    exit 1
}



proc execAndLog {args} {

    set shouldNeverFail 0
    set didFail 0
    set errorWas {}
    if {[string equal -nocase [lindex $args 0] "-neverfail"]} {
        set shouldNeverFail 1
        set args [lrange $args 1 end]
    }
    set execResult ""
    log "Executing command \"$args\":"
    try {
        set execResult [exec -ignorestderr {*}$args]
    } on error errorMessage {
        set errorWas $errorMessage
        set didFail 1
        if {!$shouldNeverFail} {
            fatalError "Error executing external command \"$args\" with error message \"$errorMessage\""
        }
    }

    if {$shouldNeverFail && $didFail} {
        log "Recovering from error \"$errorWas\""
    } else {
        log "Result was \"$execResult\""
    }
    return $execResult
}

proc requirePrograms {args} {
    set programsNotPresent [list]
    foreach prog $args {
        if {[auto_execok $prog] eq ""} {
            lappend programsNotPresent $prog
        }
    }

    if {[llength $programsNotPresent] != 0} {
        fatalError "Required programs [join $programsNotPresent ", "] not found!"
    }

}

proc createDirectoryIfNotPresent {dir} {
    if {![file isdirectory $dir]} {
        log "Creating directory \"$dir\""

        if {[file exists $dir]} {
            error "Directory \"$dir\" exists but is not a directory!"
        }
        file mkdir $dir
    }
}

proc versionCheckOfRequiredPrograms {} {
    # eyeD3
    set eyeD3MinimumVersion 0.6
    set eyeD3MaximumVersion 0.6.18
    set firstFewVersionCharsOfEyeD3 [string range [exec eyeD3 --version 2>@1] 0 100]
    if {![regexp {^eyeD3 ([0-9]+\.[0-9]+\.[0-9]+)} $firstFewVersionCharsOfEyeD3 _ eyeD3Version]} {
        fatalError "Couldn't parse version text of eyeD3. The first few charcters were \n\"$firstFewVersionCharsOfEyeD3\""
    }

    if {![semver valid $eyeD3Version]} {
        fatalError "EyeD3 version $eyeD3Version is not in a good version format!"
    }

    if {[semver < $eyeD3Version $eyeD3MinimumVersion]} {
        fatalError "EyeD3 version $eyeD3Version is too old, need at least $eyeD3MinimumVersion!"
    }

    if {[semver > $eyeD3Version $eyeD3MaximumVersion]} {
        fatalError "EyeD3 version $eyeD3MaximumVersion is too new, need at most $eyeD3MaximumVersion, because the commandline interface changed afterwards!"
    }
    log "EyeD3 version $eyeD3Version is fine."
}

####################################################################################################
#                                           CONVERT FILE                                           #
####################################################################################################


proc isAlreadyInTargetFormat {file} {
    global target
    log "Getting media format of file \"$file\""

    set checkProperty {
        {property inputPropertyValue file} {
            global target
            set targetPropertyValue $target($property)
            if {![string equal -nocase $targetPropertyValue $inputPropertyValue]} {
                log "Not skipping conversion of file \"$file\" because target $property $targetPropertyValue != input $property $inputPropertyValue"
                return -level 2 0
            }
        }
    }

    apply $checkProperty mediainfoType [execAndLog -neverfail mediainfo {--inform=Audio;%Format%} $file] $file

    set actualBitrate [execAndLog -neverfail mediainfo {--inform=Audio;%BitRate%} $file]
    set targetBitrate [expr {$target(bitrateInKbps)*1000}]

    if {$actualBitrate <= $targetBitrate} {
        log "Skipping conversion of file  \"$file\" as its bitrate of $actualBitrate bit/s is less or equal than the target of $targetBitrate bit/s"
        return 1
    } elseif {$actualBitrate > $targetBitrate} {
        log "Not skipping conversion of file  \"$file\" as its bitrate of $actualBitrate bit/s is greater than the target of $targetBitrate bit/s"
        return 0
    }
}



proc convertFile {inputFile outputFile} {
    global target
    if {[file exists $outputFile]} {
        log "Skipping conversion of file \"$inputFile\" as it's already in target format"
        return
    }
    if {[isAlreadyInTargetFormat $inputFile]} {
        file copy $inputFile $outputFile
    } else {
        debugVal target(bitrateMode)
        if {$target(bitrateMode) eq "CBR"} {
            set bitrateFlags [list -b:a $target(bitrateInKbps)k]
            set bitrateLogPart "$target(bitrateInKbps) kb/s $target(bitrateMode)"
        } elseif {$target(bitrateMode) eq "VBR"} {
            # https://trac.ffmpeg.org/wiki/Encode/MP3
            set bitrateFlags [list -q:a $target(VBRQuality)]
            set bitrateLogPart "quality $target(VBRQuality) (lower is better) VBR"
        } elseif {$target(bitrateMode) eq "ABR"} {
            set bitrateFlags [list -b:a $target(bitrateInKbps)k -abr 1]
            set bitrateLogPart "$target(bitrateInKbps) kb/s ABR"
        } else {
            fatalError "Unknown bitrate mode $target(bitrateMode)"
        }
        log "Converting file \"$inputFile\" to output file \"$outputFile\" in format $target(format) with $bitrateLogPart"
        execAndLog -neverfail ffmpeg -threads 1 -loglevel error -hide_banner -y -i $inputFile  -c:a $target(ffmpegEncoder) {*}$bitrateFlags \
            -map a -f $target(format) {*}${target(additionalFfmpegFlags)} $outputFile
    }
}

####################################################################################################
#                                          NORMALIZE FILE                                          #
####################################################################################################


proc normalizeLoudnessOfFile {file} {
    log "Normalizing loudness of file \"$file\""
    execAndLog mp3gain -q -k -r -T -s s $file
}

####################################################################################################
#                                       STRIP UNWANTED TAGS                                        #
####################################################################################################

proc getTag {tag file} {
    execAndLog mediainfo --inform=General\;%$tag% $file
}

proc getValuesOfOkTags {file} {
    set okTags $::target(okTags)
    global mediainfoTagsToId3
    log "Getting values for the ok tags [join $okTags ", "] from file \"$file\""


    set okTagsAndValues [dict create]

    foreach tag $okTags {
        set tagValue [getTag $tag $file]
        set properTagValue [string trim [capitalizeWords $tagValue]]
        dict append okTagsAndValues [dict get $mediainfoTagsToId3 $tag] $properTagValue
    }

    return $okTagsAndValues
}


proc stripUnwantedTags {file} {
    set okTagsAndValues [getValuesOfOkTags $file]
    set taggingOptions [list]
    dict for {tag valueOfTag} $okTagsAndValues {
        log "Found ok tag ${tag} = ${valueOfTag} for \"$file\""
        lappend taggingOptions "--set-text-frame=${tag}:${valueOfTag}"
    }
    log "Stripping unwanted tags from file \"$file\" inplace"
    execAndLog -neverfail eyeD3 --no-color --no-tagging-time-frame --remove-all $file
    execAndLog -neverfail eyeD3 --no-color --to-v2.3 --set-encoding=latin1 --force-update --no-tagging-time-frame {*}$taggingOptions $file
}

####################################################################################################
#                                             RENAMING                                             #
####################################################################################################

proc getConvertedFilenameFor {file} {
    global target
    set file [file normalize $file]

    set convertedFile "[::fileutil::tempfile].$target(format)"
    return $convertedFile
}

proc renameToFinalForm {file finalDir} {
    foreach tagName {Performer Album Track} {
        set tags($tagName) [string map {
            / _
            ? _
            : _
            * _
            | _
            \" '
            ~ -
            < (
            > )
            \{ (
            \} )
            [ (
            ] )
        } [getTag $tagName $file]]
    }

    set destinationDir [file join $finalDir $tags(Performer) $tags(Album)]
    createDirectoryIfNotPresent $destinationDir
    set destinationName [file join $destinationDir "$tags(Track).$::target(format)"]
    log "Renaming $file to final destination $destinationName"
    file rename $file $destinationName
}


####################################################################################################
#                                     GETTING FILES TO CONVERT                                     #
####################################################################################################

proc getFilesToConvert {dir} {
    global extensionsOfFilesToConvert
    set globPatternsToLookFor [list]
    foreach ext $extensionsOfFilesToConvert {
        lappend globPatternsToLookFor "*.$ext"
    }
    log "Looking for files with glob patterns \"[join $globPatternsToLookFor ", "]\""

    set filesToConvert [::fileutil::findByPattern [file normalize $dir] -glob -- $globPatternsToLookFor]

    set normalizedFilesToConvert [list]
    foreach file $filesToConvert {
        lappend normalizedFilesToConvert [file normalize $file]
    }


    log "Files to convert:\n\t[join $normalizedFilesToConvert \n\t]"
    return $normalizedFilesToConvert
}


####################################################################################################
#                                      MULTITHREADING SUPPORT                                      #
####################################################################################################

set threadPool [list]

proc createThreadPool {numberOfThreads} {
    global threadPool

    log "Creating thread pool with at most $numberOfThreads threads"
    set thisScript [fileutil::cat -- [info script]]

    set initializeCommandForThreads [subst -nocommands -nobackslashes {
        set iAmTheMaster 0
        $thisScript
    }]

    set threadPool [tpool::create -minworkers $numberOfThreads -maxworkers $numberOfThreads -initcmd $initializeCommandForThreads]

}


proc joinThreadPool {jobsPosted} {
    global threadPool

    set jobsStillPending dummyJobId
    while {$jobsStillPending ne ""} {
        tpool::wait $threadPool $jobsPosted jobsStillPending
    }
    tpool::release $threadPool
}


proc asynchronousEval {scriptToExecute} {
    global threadPool
    set jobId [tpool::post -nowait $threadPool $scriptToExecute]
    return $jobId
}



####################################################################################################
#                                           MAIN PROGRAM                                           #
####################################################################################################

proc processFile {file fileNameOfConvertedFile finalDir} {
    log "Beginning to process file $file"
    convertFile $file $fileNameOfConvertedFile
    if {![file exist $fileNameOfConvertedFile] || [file size $fileNameOfConvertedFile] < 10} {
        log "Failed to convert file $file, continuing"
        ::tsv::set error anyError 1
        return
    }
    normalizeLoudnessOfFile $fileNameOfConvertedFile
    stripUnwantedTags $fileNameOfConvertedFile
    renameToFinalForm $fileNameOfConvertedFile $finalDir
    log "Finished processing file $file"
}

proc processSingleFile {file finalDir} {
    set file [file normalize $file]
    set convertedFile [getConvertedFilenameFor $file]
    set jobId [asynchronousEval [list processFile $file $convertedFile $finalDir]]
}

proc processFilesBelow {dir finalDir} {
    log "Processing dir \"$dir\""
    set filesToConvert [getFilesToConvert $dir]
    set convertedFiles [list]
    set jobsPosted [list]

    foreach file $filesToConvert {
        set jobId [processSingleFile $file $finalDir]
        lappend jobsPosted $jobId
    }

    return $jobsPosted
}


proc main {} {

    global argv argc argv0 target finalDir requiredPrograms

    set optDict [dict create \
        outputDirOpt [dict create \
            shortOpt "o" \
            longOpt "outputdir" \
            numberOfArgs 1 \
            required 0 \
            usage "The directory to put the converted files to; defaults to \"$finalDir\"" \
            argsDescription "a directory" \
        ] \
        inDirsOpt [dict create \
            shortOpt "" \
            longOpt "" \
            numberOfArgs -1 \
            required 1 \
            usage "Directories to search for files or just files" \
            argsDescription "directories or files..." \
        ] \
        parallelJobsOpt [dict create \
            shortOpt "j" \
            longOpt "jobs" \
            numberOfArgs 1 \
            required 0 \
            usage "How many conversion jobs to run in parallel" \
            argsDescription "number of jobs" \
        ] \
        qualityOpt [dict create \
            shortOpt "q" \
            longOpt "quality" \
            numberOfArgs 2 \
            required 0 \
            usage "Quality e.g. vbr 0 or cbr 128 or abr 256" \
            argsDescription [list "mode (ABR, CBR or VBR)" "bitrate in kbits/s or quality"] \
        ]
    ]

    set thisProgramName [file tail $::argv0]

    if {[string equal -nocase $target(bitrateMode) VBR]} {
        set bitrateModeQualityPart "-q:a $target(VBRQuality) $target(bitrateMode)"
    } else {
        set bitrateModeQualityPart "$target(bitrateInKbps) kb/s $target(bitrateMode)"
    }

    set usage "Converts all files to $target(format) $bitrateModeQualityPart it finds"
    ::parseGnuOpts::processArgs $argv0 $argv $argc $optDict $usage
    ::parseGnuOpts::importOptArrays

    if {$help(present)} {
        ::parseGnuOpts::displayUsage
        exit 1
    }


    requirePrograms {*}$requiredPrograms
    versionCheckOfRequiredPrograms

    if {$outputDirOpt(present)} {
        set finalDir [file normalize [lindex $outputDirOpt(passedArgs) 0]]
    }
    
    # handle quality options
    if {$qualityOpt(present)} {
        lassign $qualityOpt(passedArgs) bitrateMode numericValue
        set target(bitrateMode) [string toupper $bitrateMode]
        debugVal target(bitrateMode)
        debugVal numericValue
        if {$target(bitrateMode) eq "VBR"} {
            set target(VBRQuality) $numericValue
        } else {
            set target(bitrateInKbps) $numericValue
        }
    }

    # validate quality options
    if {!($target(bitrateMode) in {ABR CBR VBR})} {
        fatalError "bitrate mode $target(bitrateMode) must be one of ABR, CBR or VBR"
    }
    if {!([string is entier $target(VBRQuality)] && $target(VBRQuality) >= 0 && $target(VBRQuality) <= 9)} {
        fatalError "VBR quality $target(VBRQuality) must be an integer between 0 and 9 inclusive"
    }
    if {!([string is entier $target(bitrateInKbps)] && $target(bitrateInKbps) >= 32)} {
        fatalError "Bitrate in kb/s $target(bitrateInKbps) must be an integer and at least 32 kb/s"
    }

    # propagate to other threads from master
    ::tsv::array set masterTarget [array get target]

    # Handle how many jobs to do in parallel
    set maximumNumberOfThreads [::numberOfCores::getNumberOfCores]
    if {$parallelJobsOpt(present)} {
        set numberOfThreads $parallelJobsOpt(passedArgs)
    } else {
        set numberOfThreads $maximumNumberOfThreads
    }

    if {![string is entier $numberOfThreads]} {
        fatalError "Requested number of concurrent jobs \"$numberOfThreads\" is not a valid integer!"
    } elseif {$numberOfThreads < 1 || $numberOfThreads > $maximumNumberOfThreads} {
        fatalError "Number of concurrent jobs must be >= 1 and <= $maximumNumberOfThreads (maximum number of threads), but is $numberOfThreads!"
    }

    log "Doing $numberOfThreads concurrent jobs"
    createThreadPool $numberOfThreads


    log "Output directory is \"$finalDir\""

    set jobsPosted [list]
    foreach dirOrFile $inDirsOpt(passedArgs) {
        set dirsToProcess [list]
        set filesToProcess [list]

        if {![file exist $dirOrFile]} {
            log "\"$dir\" does not exist, skipping"
            continue
        }

        if {[file type $dirOrFile] eq "file" && [file extension $dirOrFile] eq ".txt"} {
            log "Reading directories from $dirOrFile"
            set fd [open $dirOrFile]
            set lines [split [read $fd] "\n"]
            close $fd
            foreach dir $lines {
                if {[file exist $dir] && [file type $dir] eq "directory"} {
                    lappend dirsToProcess $dir
                }
            }
        } elseif {[file type $dirOrFile] eq "directory"} {
            lappend dirsToProcess $dirOrFile
        } elseif {[file type $dirOrFile] eq "file"} {
            lappend filesToProcess $dirOrFile
        } else {
            log "\"$dir\" is no directory or file, skipping"
            continue
        }
        foreach dir $dirsToProcess {
            set newJobsPosted [processFilesBelow $dir $finalDir]
            set jobsPosted [list {*}$jobsPosted {*}$newJobsPosted]
        }
        foreach file $filesToProcess {
            set newJobPosted [processSingleFile $file $finalDir]
            set jobsPosted [list {*}$jobsPosted $newJobPosted]
        }
    }

    joinThreadPool $jobsPosted
    if {[::tsv::get error anyError]} {
        exit 2
    }
}

if {$iAmTheMaster} {
    main
}
