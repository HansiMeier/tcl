#!/usr/bin/env tclsh

package require Tcl 8.6-
package require tepam


namespace eval collect {
   variable Acc
   variable Var
}

# collects certain things during each execution of a loop:
# any loop can be used, in fact any command where the last
# argument is a body to be evalued, even eval!
# either the values of the variables given by -variables
# as in
#
# collect -variables {x y} {
#    for {set i 0} {$i != 3} {incr i 1} {
#       set x [expr {2*$i}]
#       set y [expr {5*$i}]
#    }
# }
# which returns a list {0 0 2 5 4 10}
#
# or the evaluated values of eval given by -eval
# as in 
# collect -eval {{set $x} {set $y}} {
#    for {set i 0} {$i != 3} {incr i 1} {
#       set x [expr {2*$i}]
#       set y [expr {5*$i}]
#    }
# }
# which is equivalent to the snippet before
#
# or simply the value of the loop body
# as in
# collect {
#    foreach x {1 2 3} {
#       return -level 0 9
#    }
# }
#
# which returns {9 9 9}

::tepam::procedure collect {
   -return "Collected items"
   -short_description "Execute body and collect values"
   -description "Executes body which should be a command taking a script as last parameter, typically a loop like foreach, and collects things in a list during each execution of the loop "
   -args {
      {-variables -optional -description "List of variables to collect"}
      {-into -optional -description "Where to save the collected list"}
      {-eval -optional -description "List of scripts to evaluate and whose results are collected"}
      {body -description "Function to execute"}
   }
} {


   set ::collect::Acc {}
   set controlStructureStuff [lrange $body 0 end-1]
   set loopBody [lindex $body end]
   set variables? [info exists variables]
   set eval? [info exists eval]
   set into? [info exists into]



   if {${eval?}} {
      append loopBody \n[subst -nocommands {
         foreach ::collect::Var [list $eval] {
            lappend ::collect::Acc [{*}\$::collect::Var]
         }
      }]
   }
   
   if {${variables?}} {
      append loopBody \n[subst -nocommands {
         foreach ::collect::Var [list $variables] {
            lappend ::collect::Acc [set \$::collect::Var]
         }
      }]
   }

   if {! (${variables?} || ${eval?})} {
      # just collect the value of evaluating $loopBody
      set loopBody "lappend ::collect::Acc \[$loopBody]"
   }



   tailcall eval [subst {
      $controlStructureStuff {
         $loopBody
      }
      [if {${into?}} {
             return -level 0 "set {$into} \$::collect::Acc"
          } else {
             return -level 0 {set ::collect::Acc}
          }]
   }]
}
