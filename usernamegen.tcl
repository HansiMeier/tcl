#!/usr/bin/env tclsh

package require Tcl 8.6-
namespace import ::tcl::mathop::*
namespace import ::tcl::mathfunc::*

set debugOn 0

set maxConsecutiveConsonants 1
set maxConsecutiveVowels 1

set Vowels [list a e i o u]
set VowelsLen [llength $Vowels]
set VowelsLastInd [- $VowelsLen 1]

set Consonants [list b c d f g h j k l m n p r s t v w x y z] ;# no q!
set ConsonantsLen [llength $Consonants]
set ConsonantsLastInd [- $ConsonantsLen 1]

proc debug txt {
   if {$::debugOn} {
      puts stderr $txt
   }
   return
}

proc intRand {lower upper} {
   tailcall + $lower [int [* [+ 1 [- $upper $lower]] [rand]]]
}

proc pickVowel {} {
   set randIndex [intRand 0 $::VowelsLastInd]
   tailcall lindex $::Vowels $randIndex
}

proc pickConsonant {} {
   set randIndex [intRand 0 $::ConsonantsLastInd]
   tailcall lindex $::Consonants $randIndex
}

proc pickLetter {} {
   if {[intRand 0 1] == 1} {
      tailcall pickVowel
   } else {
      tailcall pickLetter
   }
}

proc isConsonant char {
   tailcall ni $char $::Vowels
}

proc pickUsername {{len 7}} {
   set username ""
   set consecutiveConsonants 0
   set consecutiveVowels 0
   for {set i 0} {$i < $len} {incr i 1} {
      set picked [pickLetter]
      if {[isConsonant $picked]} {
         debug "consonant $picked"
         incr consecutiveConsonants 1
         set consecutiveVowels 0
         if {$consecutiveConsonants > $::maxConsecutiveConsonants} {
            set picked [pickVowel]
            debug "Instead vowel $picked"
            set consecutiveVowels 1
            set consecutiveConsonants 0
         }
      } else {
         incr consecutiveVowels 1
         set consecutiveConsonants 0
         debug "vowel $picked"
         if {$consecutiveVowels > $::maxConsecutiveVowels} {
            set picked [pickConsonant]
            debug "Instead consonant $picked"
            set consecutiveConsonants 1
            set consecutiveVowels 0
         }
      }
      append username $picked
   }
   return $username
}

proc displayUsageAndExit argv0 {
   foreach line [list \
      "Usage: $argv0 \[length of username = 7\]" \
      "Generates a username with:" \
      "   - no more than $::maxConsecutiveVowels consecutive vowels" \
      "   - no more than $::maxConsecutiveConsonants consecutive consonants" \
      "   - only ascii lowercase letters" \
   ] {
      puts stdout $line
   }
   exit 1
}


proc main {argc argv argv0} {
   if {$argc > 1} {
      displayUsageAndExit $argv0
   }

   set len $argv
   if {$len eq ""} {
      set len 7
   }
   if {![string is entier $len] && $len > 0} {
      puts stderr "\"$len\" must be a natural number!"
      displayUsageAndExit $argv0
   }
   puts stdout [pickUsername {*}$len]
   return
}

main $argc $argv $argv0

