#define _BSD_SOURCE
#include <cpptcl.h>
#include <string>
#include <stdlib.h>

Tcl::interpreter ooInterp(NULL, false);

#define declareCommand(command) \
    ooInterp.def("::sysinfo::" #command, command);

Tcl::object getloadavgCmd(const Tcl::object& args) {
    int numberOfAverages;


    static double loadAverages[3];

    switch (args.length(ooInterp))
    {
        case 0: {
            numberOfAverages = 3;
            break;
        }

        case 1: {
           numberOfAverages = args.get<int>(ooInterp);
           break;
        }

        default: {
            throw Tcl::tcl_error("Wrong # args: should be \"getloadavg ?number of averages?\"");
        }
    }

    if (numberOfAverages > 3 || numberOfAverages < 1)
    {
        throw Tcl::tcl_error("Number of averages must be an integer between 1 and 3, but is " + std::to_string(numberOfAverages) + " instead!");
    }

    if (getloadavg(loadAverages, numberOfAverages) != numberOfAverages)
    {
        throw Tcl::tcl_error("Failed getting load averages!");
    }

    return Tcl::object::make_object_list(loadAverages, loadAverages + numberOfAverages);
}


extern "C" int Sysinfo_Init(Tcl_Interp* interp) {
    Tcl_InitStubs(interp, "8.6-", 0);
    ooInterp.set(interp);

    Tcl_Namespace* nsPtr = ooInterp.create_namespace("sysinfo");

    ooInterp.def("::sysinfo::getloadavg", getloadavgCmd);
    ooInterp.export_namespace(nsPtr, "[a-z]*");

    return TCL_OK;
}


