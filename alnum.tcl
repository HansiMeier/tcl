#!/usr/bin/env tclsh

namespace import ::tcl::mathop::*

proc alnum args {
   set str {}
   set offset [- [scan a %c] 1]

   foreach num $args {
      append str [format %c [+ $num $offset]]
   }

   return $str
}

proc displayUsage argv0 {
   foreach line [list \
      "Usage: $argv0 num1 num2 num3 ..." \
      "Converts numbers into letters using the scheme:" \
      "1 = a, 2 = b, 3 = c, ..." \
      "And outputs the result" \
   ] {
      puts stderr $line
   }
}

proc main {} {
   global argv0 argc argv
   if {$argc == 0} {
      displayUsage $argv0
      exit 1
   }

   puts stdout [alnum {*}$argv]
   exit 0
}

main

