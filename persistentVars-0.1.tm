#!/usr/bin/env tclsh

package require Tcl 8.6-
package require Mk4tcl


namespace eval persistentVars {
   variable View
   variable Vars
   variable DbName

   proc useDatabaseWithUUID uuid {
      variable View
      variable DbName
      set uuid [string tolower $uuid]
      regsub -all -- {[^[:alnum:]]} $uuid z alnumUuid

      if {[info exists ::env(TCL_PERSISTENTVARS_DBDIR)]} {
         set dbDir $::env(TCL_PERSISTENTVARS_DBDIR)
      } else {
         set dbDir [pwd]
      }

      if {! ([file readable $dbDir] && [file writable $dbDir])} {
         throw {insufficient rights for dbDir} "Insufficient rights for database directory \"$dbDir\": Needs to be both readable and writable!\n Does it even exist?"
      }

      set dbFile [file join $dbDir ${uuid}.metakitdb]
      if {[file exists $dbFile] && ! ([file readable $dbFile] && [file writable $dbFile])} {
         throw {insufficient rights for dbfile} "Insufficient rights for existing database file \"$dbFile\": Needs to be both readable and writable!"
      }

      set DbName $alnumUuid

      ::mk::file open $DbName $dbFile
      set View [::mk::view layout ${DbName}.vars {name:S value:B}]
      trace add execution exit enter ::persistentVars::CleanUpHandler
      return
   }

   proc CleanUpHandler args {
      variable Vars
      variable View
      variable DbName

      catch {
         foreach varName [array names Vars] {
            set cursor $Vars($varName)
            set value [set $varName]
            ::mk::set $cursor name $varName value $value
         }

         ::mk::file close $DbName
      }
   }

   proc persistent {var {initPrefix {}}} {
      variable View
      variable Vars
 
      upvar 1 $var local
      set absVarName [uplevel 1 [list namespace which -variable $var]]
      set indices [::mk::select $View -exact name $absVarName] 

      switch -exact -- [llength $indices] {
         0 {
            if {$initPrefix ne ""} {
               uplevel 1 [list {*}$initPrefix $absVarName]
            } else {
               set local {}
            }
            ::mk::row append $View name {NAME NOT YET FILLED IN} value {VALUE NOT YET FILLED IN}
            ::mk::cursor create cursor $View end
         }

         1 {
            ::mk::cursor create cursor $View $indices
            set local [::mk::get $cursor value]
         }

         default {
            puts stderr "The entry for \"$absVarName\" in the database isn't unique!"
         }
      }

      set Vars($absVarName) $cursor

      return $local
   }

   namespace export {[a-z]*}
}




