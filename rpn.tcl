#!/usr/bin/env tclsh


####################################################################################################
#                                             PREAMBLE                                             #
####################################################################################################

package require tclreadline
package require Tcl 8.6-
package require convertCurrencies ;# to convert the currencies
package require cartesianProduct
package require stefMath
package require misc ;# for normalizeProcname

rename ::exit ::realExit ;# needed for the exit alias to work.

set prompt {-> }
set histFile $::env(HOME)/.[regsub -nocase {\.tcl$} [file tail [info script]] {}]-history
set histLen 200 ;# max. number of lines in $histFile
set delim {#}
set stableDelim {##}

set footInCm 30.48
set inchInCm 2.54

set constants {{euler 2.718281828459045235} {pi 3.14159265358979323846} {clicht 299792458}
               {rerde 6371000.8} {merde 5.9736E27} {gerde 9.80665} {mprot 1.672621777E-27} {elemlad 1.602E-19}
               {melek 9.10938291E-31} {mneutr 1.674927351E-27} {ggrav 6.67384E-11} {eps 2.2204e-16}
               {hplanck 6.626070040e-34} {hquer 1.054571800e-34} {false 0} {true 1} {kginlbs 2.20462262} {kjinkcal 0.2390057361}}

set aliases [dict create e euler l last d dup s swap = var ?? help ? body exit quit \
             c clear q quit sq square \
             ld log10 ln log a+ sum a* product ** power ^ power \
             mean arithmeticMean am arithmeticMean gm geometricMean lb log2 \
             ilb intLog2 fac factor ! factorial \
             arctan atan arcsin asin arccos acos \
             fb frombin fh fromhex \
             degrad toRadian raddeg toDegree torad toRadian todeg toDegree \
             fs fromscientific \
             inv invert \
             tb tobin th tohex \
             ts toscientific \
             from°F to°C \
             from°C to°F \
             fromkg tolbs \
             tokg fromlbs \
             lbskg fromlbs \
             kglbs tolbs \
             ftincm tocm \
             kcalkj tokj \
             kjkcal fromkj \
             tokcal fromkj \
             toftin fromcm \
             cmftin fromcm \
             chr char \
             bash execShellCommand \
             sh execShellCommand \
             ` backtick \
             pft productFromTo def defineProc \
             f€ chfeur €f eurchf f\$ chfusd \$f usdchf rt root p pop \
             exp% expmod ncr binCoeff f! fallingFact cy cycle \
             p? isPrime r. redo u. undo bra bestRationalApproximant torat bestRationalApproximant \
             toascii char fromascii ord]

# Functions and operators which are automatically implemented in the MATHS PROCS section
# using a generic stackProcs template (see defineGenericMathsProcs)
set doubleArgFuncs {hypot fmod atan2 pow}
set operators {+ - * % < > << >> <= >= == && & || |} ;# '/' is implemented on its own to always do double division
set singleArgFuncs {round wide sqrt sin log10 double atan bool rand abs acos entier
                    srand sinh log floor tanh tan isqrt int asin min ceil cos cosh
                    exp max}

# The currencies for which to create converter commands
set currencies [list CHF USD EUR GBP CZK CAD JPY]

# Global variables
set stack [list] ;# has to be empty
set legalStackProcs [list] ;# also add legal normal tcl commands
set numStackProcs 0
set redoStack [list]
set undoStack [list]


set stackProcsAlsoUsedInsideNumbers [list b x e - +]
set braced 0

####################################################################################################
#                                            META PROCS                                            #
####################################################################################################

proc warnAndReturn {txt} {
   puts stderr $txt
   tailcall return
}

proc stackProc {name body} {
   global legalStackProcs
   global numStackProcs
   proc $name {} $body
   lappend legalStackProcs $name
   incr numStackProcs 1
   return
}

proc defineConstants {} {
   global constants
   foreach pair $constants {
      set name [lindex $pair 0]
      set value [lindex $pair 1]
      stackProc $name "push $value"
      uplevel #0 "set $name $value"
   }
   return
}

proc defineAliases {} {
   global aliases
   global numStackProcs
   global legalStackProcs
   # pair: {$aliasToAssign $oldName}
   dict for {new old} $aliases {
      interp alias {} $new {} $old

      incr numStackProcs 1
      lappend legalStackProcs $new
   }
   return
}

proc displayAdvice {} {
   puts stderr "Welcome to the RPN calculator"
   puts stderr "------------------------------------------------------"
   puts stderr "Be sure to distinguish sign (+|-) and operation (+|-)"
   puts stderr "as this program always tries to read a number before"
   puts stderr "trying to execute an operation. \"6 9+8/\" returns a"
   puts stderr "stack of {6 1.125} and not {1.875} (+8 is a number)!"
   puts stderr "\nEnter ?? to see a list of commands.\n"
   return
}

proc customCompleter {word start end line} {
   global legalStackProcs
   global aliases

   set toSearch [list {*}$legalStackProcs {*}$aliases]

   set compl [list]

   if {[llength $word] == 0} {
      set compl $toSearch
   } else {
      foreach p $legalStackProcs {
         if {[string first $word $p] == 0} {
            lappend compl $p
         }
      }
   }

   if {[llength $compl] == 1} {
      return $compl
   } else {
      set compl [list $word {*}[lsort $compl]]
   }
   return $compl
}

proc initialiseReadline {} {
   # use old & buggy tclreadline
   global histFile
   global histLen
   global legalStackProcs

   set tclreadline::historyLength $histLen
   ::tclreadline::Print no
   ::tclreadline::readline initialize $histFile
   ::tclreadline::readline customcompleter customCompleter
   ::tclreadline::readline builtincompleter 0
   return
}

####################################################################################################
#                                        BASIC STACK PROCS                                         #
####################################################################################################

stackProc pop {
   global stack
   set topItem [lindex $stack end]
   if {! [llength $topItem]} {
      throw {POP FROM EMPTY STACK} {Trying to pop from an empty stack; too few arguments?}
   }
   set stack [lreplace $stack[set stack {}] end end]
   return $topItem
} 

proc peek {} {
    global stack
    tailcall lindex $stack end
}

proc popRealNum {} {
    set thing [pop]
    if {[isRationalString $thing]} {
        set numAndDen [split $thing /]
        set double [expr {double([lindex $numAndDen 0]) / double([lindex $numAndDen 1])}]
    } else {
        set double $thing
    }
    return [intIfPossible $double]
}

proc push item {
   global stack
   lappend stack $item
   return $item
}


proc pushNonEmpty item {
   if {[string trim $item] ne ""} {
      push $item
   }
}

proc pushRealNum number {
   set maybeInt [intIfPossible $number]
   if {[string is entier $maybeInt]} {
      set toPush $maybeInt
   } else {
      set toPush [format %g $number]
   }
   push $toPush
}

proc grabUntilDelim {} {
   global stack \
          delim \
          stableDelim

   set stackLen [llength $stack]
   set results [list]
   ::misc::doTimes $stackLen {
      set item [pop]
      if {$item eq $delim} {
         break
      } elseif {$item eq $stableDelim} {
         push $stableDelim
         break
      }
      lappend results $item
   }

   return $results
}


####################################################################################################
#                                           ASCII TABLE                                            #
####################################################################################################

stackProc ord {
    set string [pop]
    binary scan $string c* ords
    foreach num $ords {
        push $num
    }
}

stackProc char {
    set ords [grabUntilDelim]
    set chars [list]
    foreach ord $ords {
        lappend chars [format %c $ord]
    }
    push [join [lreverse $chars] {}]
}


####################################################################################################
#                                      FORTH-LIKE STACK PROCS                                      #
####################################################################################################

stackProc map {
    global stack
    global delim
    set action [pop]
    set operands [grabUntilDelim]
    set oldStack $stack
    set stack $operands

    push $delim
    cycle
    while {[peek] ne $delim} {
        $action
        cycle
    }
    if {[peek] eq $delim} {
        pop
    }

    set stack [list {*}$oldStack {*}[lreverse $stack]]
}

stackProc size {
   puts stdout "The stack contains [llength $::stack] elements"
}

stackProc defineProc {
   set name [pop]
   stackProc $name [grabUntilDelim]
   sortLegalStackProcs
}

stackProc backtick {
   set args [grabUntilDelim]
   pushNonEmpty [exec -- bash -s << [join $args " "]]
}

stackProc execShellCommand {
   set args [grabUntilDelim]
   set toOutput [exec -- bash -s << [join $args " "] 2>@1]
   if {[string trim $toOutput] ne ""} {
      puts $toOutput
   }
}

stackProc tcl {
   global stack
   pushNonEmpty [uplevel 1 {*}[grabUntilDelim]]

}

stackProc last {
   global stack
   set stack [lindex $stack end]
}

stackProc dup {
   push [push [pop]]
}

stackProc swap {
   set a [pop]
   set b [pop]
   push $a
   push $b
}

stackProc findProc {
   set procToFind [::misc::dereferenceAlias [pop] 1]
   foreach proc [info procs] {
      if {[string equal -nocase $proc $procToFind]} {
         push $proc
         return
      }
   }

   throw {UNKNOWN PROC} "Unknown proc $procToFind"
}

stackProc body {
   findProc
   puts [string trim [info body [pop]]]
}

stackProc var {
   set name [pop]
   set value [pop]

   if {[string is entier $value] || [string is double $value]} {
      set value [intIfPossible $value]
   }

   stackProc $name "push $value"

   puts stdout "Created variable $name := $value"

   # Sorting is required after creating a new stackProc,
   # see comment for sortLegalStackProcs

   sortLegalStackProcs
}

stackProc help {
   global legalStackProcs
   puts stdout "Legal commands are: [lsort -nocase $legalStackProcs]"
}

stackProc print {
   global stack
   foreach elem $stack {
      if {[regexp {[[:space:]]} $elem]} {
         puts -nonewline stdout "{$elem} "
      } else {
         puts -nonewline stdout "$elem "
      }
   }
   puts stdout {}
}

# https://code.activestate.com/recipes/146220-inserts-thousand-separators-into-a-number/
proc commify {num {sep ,}} {
    while {[regsub {^([-+]?\d+)(\d\d\d)} $num "\\1$sep\\2" num]} {}
    return $num
}

stackProc display {
   set popped [pop]
   set toPrint $popped
   if {[string is entier $popped]} {
      set toPrint [commify $popped {'}]
   } elseif {[string is double $popped]} {
      set toPrint [format %e $popped]
   }
   puts stdout $toPrint
}

stackProc clear {
   global stack
   set stack {}
}

stackProc quit {
   global histFile
   ::tclreadline::readline write $histFile
   realExit
}

stackProc result {
   global stack
   puts stdout "-> [lindex $stack end]"
}

stackProc cycle {
   global stack
   set last [pop]
   set stack [list $last {*}$stack]
}

stackProc delim {
   global delim
   set oldDelim $delim
   set delim [pop]
   puts "Changed delimiter from \"$oldDelim\" to \"$delim\""
}

stackProc stableDelim {
   global stableDelim
   set oldstableDelim $stableDelim
   set stableDelim [pop]
   puts "Changed stableDelimiter from \"$oldstableDelim\" to \"$stableDelim\""
}

####################################################################################################
#                                           MATHS PROCS                                            #
####################################################################################################

proc intIfPossible number {
   try {
      set intNumber [expr {entier($number)}]
   } trap {ARITH IOVERFLOW} {} {
      return [expr {double($number)}]
   } on error err {
      return $number
   }

   if {$intNumber == $number} {
      return $intNumber
   } else {
      return $number
   }
}

proc isNatural number {
   return [expr {entier($number) == $number && $number >= 0}]
}

proc intExpNotation number {
   catch {
      set number [string tolower $number]
      lassign [split $number e] base exponent
      if {[isNatural $base] && [isNatural $exponent]} {
         set base [expr {entier($base)}]
         set exponent [expr {entier($exponent)}]
         set zeros [string repeat "0" $exponent]
         set number ${base}${zeros}
      }
   }
   return $number
}

stackProc invert {
   push -1
   power
}

stackProc power {
   set exponent [popRealNum]
   set base [popRealNum]

   if {[isNatural $base] && [isNatural $exponent]} {
      set result [expr {$base ** $exponent}]
   } else {
      set result [expr {pow($base, $exponent)}]
   }

   pushRealNum $result
}

stackProc to°C {
    set degFahr [popRealNum]
    pushRealNum [expr {($degFahr - 32)/1.8}]
}

stackProc to°F {
   set degCel [popRealNum]
   pushRealNum [expr {$degCel * 1.8 + 32}]
}

stackProc fromlbs {
   kginlbs
   /
}

stackProc tolbs {
   kginlbs
   *
}

stackProc tokj {
   kjinkcal
   /
}

stackProc fromkj {
   kjinkcal
   *
}



stackProc tocm {
   global footInCm inchInCm
   set inches [popRealNum]
   set feet [popRealNum]
   push [expr {$inches * $inchInCm + $feet * $footInCm}]
}

stackProc fromcm {
   global footInCm inchInCm
   set cm [popRealNum]
   set wholeFeet [expr {int($cm / $footInCm)}]
   set inches [expr {($cm - $wholeFeet * $footInCm) / $inchInCm}]

   push $wholeFeet
   push $inches
}

stackProc bmi {
   set length [popRealNum]
   if {$length >= 3} {
      puts "Length $length looks like its given in cm"
      set length [expr {$length/1e2}]
   }
   set weight [popRealNum]
   set bmi [expr {$weight * pow($length, -2.0)}]
   push $bmi
}

stackProc PhiMuSigma {
   set sigma [popRealNum]
   set mu [popRealNum]
   set x [popRealNum]
   set sqrt2 1.4142135623730951
   pushRealNum [expr {0.5 * (1 + [::stefMath::erf [expr {($x - $mu)/($sigma * $sqrt2)}]])}]
}

stackProc Phi {
   push 0
   push 1
   PhiMuSigma
}

stackProc infix {
    set toEval [pop]
    pushRealNum [expr $toEval]
}


stackProc cot {
   pushRealNum [expr {1.0 / tan([popRealNum])}]
}

stackProc ++ {
   pushRealNum [expr {[popRealNum] + 1}]
}

stackProc -- {
   pushRealNum [expr {[popRealNum] - 1}]
}

stackProc \\ {
   swap
   /
}

stackProc sumsq {
   set nums [grabUntilDelim]
   set sum 0
   foreach num $nums {
      set sum [expr {$sum + $num*$num}]
   }

   pushRealNum $sum
}

stackProc norm {
   sumsq
   sqrt
}


stackProc fallingFact {
   set k [popRealNum]
   set n [popRealNum]
   push [::stefMath::fallingFact $n $k]
}

stackProc erf {
   set x [popRealNum]
   push [::stefMath::erf $x]
}

stackProc binCoeff {
   set k [popRealNum]
   set n [popRealNum]
   push [::stefMath::binCoeff $n $k]
}

stackProc bestRationalApproximant {
    set toApproximate [popRealNum]
    set result [::stefMath::bestRationalApproximant $toApproximate]
    push [join $result /]
}

stackProc tofloat {
    pushRealNum [popRealNum]
}

proc isRationalString {string} {
    tailcall regexp -- {(\+|-)?[0-9]+/[0-9]+} $string
}

stackProc lorentz {
   global clicht
   set v [popRealNum]
   pushRealNum [expr {1/sqrt(1-pow(double($v) / double($clicht),2))}]
}

stackProc square {
   set x [popRealNum]
   pushRealNum [expr {$x * $x}]
}

stackProc sum {
   pushRealNum [expr [join [grabUntilDelim] +]]
}

stackProc root {
   set root [popRealNum]
   set radicand [popRealNum]
   pushRealNum [expr {pow($radicand, 1.0/$root)}]
}

stackProc tohex {
   push [format 0x%x [pop]]
}

stackProc toscientific {
    set num [popRealNum]
    push [format %e $num]
}

stackProc fromscientific {
   set num [popRealNum]
   if {$num == floor($num)} {
      set formatStr %d
   } else {
      set formatStr %f
   }
   push [format $formatStr $num]
}

stackProc fromhex {
   set number [pop]
   if {![string match -nocase 0x* $number]} {
      set number 0x$number
   }
   push [format %d $number]
}

stackProc tobin {
   push [format 0b%b [pop]]
}

stackProc frombin {
   set number [pop]
   regsub -nocase -- {^0b} $number {} number
   push [scan $number %b]
}


stackProc factorial {
   set n [popRealNum]
   set product 1
   for {set i 1} {$i <= $n} {incr i 1} {
      set product [expr {$product * $i}]
   }
   push $product
}

stackProc product {
   pushRealNum [expr [join [grabUntilDelim] *]]
}

stackProc productFromTo {
   set to [popRealNum]
   set from [popRealNum]
   set product 1
   for {set i $from} {$i <= $to} {incr i 1} {
      set product [expr {$i*$product}]
   }
   push $product
}

stackProc arithmeticMean {
   global stack
   set nums [grabUntilDelim]
   set sum [expr [join $nums +]]
   pushRealNum [expr {double($sum)/double([llength $nums])}]
}


stackProc geometricMean {
   global stack
   set nums [grabUntilDelim]
   set product [expr [join $nums *]]
   pushRealNum [expr {pow($product, 1.0/double([llength $nums]))}]
}

stackProc log2 {
   pushRealNum [expr {log([popRealNum])/log(2)}]
}


stackProc intLog2 {
   pushRealNum [expr {int(log([popRealNum])/log(2))}]
}

stackProc factor {
   global stack
   set operand [popRealNum]
   set outputList [split [exec factor $operand] { }]
   set stack [list {*}$stack[set stack {}] {*}[lrange $outputList 1 end]]
}

stackProc expmod {
   set modulus [popRealNum]
   set exponent [popRealNum]
   set base [popRealNum]
   push [::stefMath::expmod $base $exponent $modulus]
}

stackProc isPrime {
   set n [popRealNum]
   push [::stefMath::isPrime $n]
}


stackProc abserr {
    set exact [popRealNum]
    set approximate [popRealNum]

    pushRealNum [expr {abs($exact - $approximate)}]
}

stackProc relerr {
    set exact [popRealNum]
    set approximate [popRealNum]

    pushRealNum [expr {double(abs($exact - $approximate))/double(abs($exact))}]
}

stackProc toDegree {
   pi
   /
   pushRealNum 180
   *
}

stackProc toRadian {
   pushRealNum 180
   /
   pi
   *
}

stackProc gcd {
   set b [popRealNum]
   set a [popRealNum]

   while {$b} {
      set tmp $a
      set a $b
      set b [expr {$tmp % $b}]
   }
   push $a
}

stackProc / {
   set divisor [popRealNum]
   set dividend [popRealNum]
   pushRealNum [expr {double($dividend) / double($divisor)}]
}


# Define all kind of things expr can do as stackProc
# using a generic template implementation
proc defineGenericMathsProcs {} {
   global operators
   global singleArgFuncs
   global doubleArgFuncs

   foreach singleArgFunc $singleArgFuncs {
      stackProc $singleArgFunc [
         subst -nocommands {
            pushRealNum [expr {${singleArgFunc}([popRealNum])}]
         }
      ]
   }
   foreach operator $operators {
      stackProc $operator [
         subst -nocommands {
            set operand2 [popRealNum]
            set operand1 [popRealNum]
            pushRealNum [expr {\$operand1 $operator \$operand2}]
         }
      ]
   }
   foreach doubleArgFunc $doubleArgFuncs {
      stackProc $doubleArgFunc [
         subst -nocommands {
            set argument2 [popRealNum]
            set argument1 [popRealNum]
            pushRealNum [expr {${doubleArgFunc}(\$argument1, \$argument2)}]
         }
      ]
   }
}


####################################################################################################
#                                       CURRENCY CONVERSION                                        #
####################################################################################################

proc defineCurrencyConverters {} {
   global currencies
   set nextCmd [::cartesianProduct::generator $currencies $currencies]

   while 1 {
      # nextCmd causes a 'break' if all pairs have been processed
      # --> no infinite loop
      set pair [$nextCmd]
      set name [string tolower [join $pair ""]]
      set from [lindex $pair 0]
      set to [lindex $pair 1]


      if {$from eq $to} {
         continue
      }

      stackProc $name [subst -nocommands {
         set amount [pop]
         push [::convertCurrencies {$from} {$to} \$amount]
      }]
   }

   return

}

stackProc \$ {
   set amount [pop]
   set to [pop]
   set from [pop]

   push [::convertCurrencies $from $to $amount]
}


####################################################################################################
#                                               UNDO                                               #
####################################################################################################

stackProc undo {
   tailcall recover 1
}

proc recover reallyUndo {
   global stack
   global undoStack
   global redoStack

   if {$reallyUndo} {
      lappend redoStack $stack
      set endIndex end-1
   } else {
      set endIndex end
   }

   set stack [lindex $undoStack $endIndex]
   ::misc::lreplaceInplace undoStack $endIndex end
   return
}

stackProc redo {
   global stack
   global undoStack
   global redoStack

   if {[llength $redoStack] == 0} {
      # Not using throw because this script resets the stack to the value of the last line on errors;
      # if for instance you have a stack {someString} and call + on it,
      # the stack {someString} is restored instead of just being empty because '+' popped the string from it.
      # We don't want this behaviour here because our stack is still fine.
      # That's why we don't fail
      warnAndReturn "Nothing to redo!"
   }

   lappend undoStack $stack
   set stack [lindex $redoStack end]
   ::misc::lreplaceInplace redoStack end end
}

proc snapshotStack {} {
   global stack
   global undoStack

   set lastCommand [lindex $stack end]

   if {![isUndoCommand $lastCommand]} {
      lappend undoStack $stack
   }
   return
}

proc isUndoCommand cmd {
   return [expr {$cmd in {u. r. undo redo}}]
}

####################################################################################################
#                                         MAIN PROCESSING                                          #
####################################################################################################

# startProcIndex is the first index which is searched in $legalStackProcs when looking for
# a stackProc in the token
proc processToken {token {startProcIndex 0}} {
   global legalStackProcs
   global numStackProcs
   global stack
   global braced
   global almostLiteral
   global stackProcsAlsoUsedInsideNumbers

   # Treat ' as quoting character if prepended
   switch [string index $token 0] {
      ' {
         if {$braced} {
            push '
         } else {
            push [string range $token 1 end]
         }
         return 1
      }

      \{ {
         set braced 1
         set token [string range $token 1 end]
         if {[set closeBraceInd [string first \} $token]] != -1} {
            # braced part ends in token
            set toPush [string range $token 0 [expr {$closeBraceInd - 1}]]
            push $toPush
            set braced 0
            return 1
         }
         lappend almostLiteral $token
         return 1
      }


   }

   if {$braced && [set closeBraceInd [string first \} $token]] != -1} {
      set braced 1
      set toPush [string range $token 0 [expr {$closeBraceInd - 1}]]
      lappend almostLiteral $toPush

      push [join $almostLiteral " "]
      set almostLiteral [list]

      set token [string range $token [expr {$closeBraceInd + 1}] end]
   }

   if {[string equal [string trim $token] ""]} {
      return 1
   }

   # Look for stackProcs in the token
   set lowercaseToken [string tolower $token]
   for {set i $startProcIndex} {$i < $numStackProcs} {incr i 1} {
      set proc [lindex $legalStackProcs $i]
      set lowercaseProc [string tolower $proc]

      if {[set first [string first $lowercaseProc $lowercaseToken]] != -1} {
         if {$lowercaseProc in $stackProcsAlsoUsedInsideNumbers} {

            set procNameLength [string length $proc]
            set last [expr {$first + $procNameLength - 1}]
            set afterLast [expr {$last + 1}]

            set charAfterProc [string index $token $afterLast]
            if {![string equal $charAfterProc {}] && ([string is integer $charAfterProc] ||
               ([string equal $lowercaseProc e] && $charAfterProc in {+ -}))} {
                  break
            }
         }

         set pre [string range $token 0 [expr {$first - 1}]]
         if {[string length $pre]} {
            processToken $pre [expr {$i+1}]
         }

         try {
            $proc
         } on error err {
            puts stderr "${proc}: $err"
            recover 0
            return 0
         }

         set post [string range $token [expr {$first + [string length $proc]}] end]
         if {[string length $post]} {
            tailcall processToken $post $i
         }

         return 1
      }
   }


   # Look for numbers in the token
   set bin 0
   set hex 0
   if {([regexp -indices -nocase -- {0b[0-1]+} $token indices] && [set bin 1]) \
       || ([regexp -indices -nocase -- {0x([a-f]|[0-9])+} $token indices] && [set hex 1]) \
       || [regexp -indices -nocase -- {(\+|-)?[0-9]+((\.|,)[0-9]+)?(e(\+|-)?[0-9]+)?} $token indices]} {

      # The variable "indices" stores the index of the first and the last character of a match as a list

      set pre [string range $token 0 [expr {[lindex $indices 0] - 1}]]
      if {$pre ne ""} {
         processToken $pre
      }

      # Process the number
      set number [string range $token {*}$indices]
      if {$hex} {
         # keep hex numbers as is
         push $number
      } elseif {$bin} {
         # Convert binary to decimal
         # because TCL can't handle binary literals
         regsub -nocase -- {^0b} $number {} number
         push [scan $number %b]
      } else {
         # it is a decimal number
         # Accept input with comma but convert it to a point
         # such that tcl does not choke
         set number [string map {, .} $number]
         set number [intExpNotation $number]
         pushRealNum $number
      }

      set post [string range $token [expr {[lindex $indices 1] + 1}] end]
      if {$post ne ""} {
         tailcall processToken $post
      }

      return 1
   }


   # Nothing found, just push it on the stack
   push $token
   return 1
}

# Sort legalStackProcs by length
# such that tcl looks for the longest stackProcs first (i.e by length and then alphabetically) when
# looking up in processToken because the stackProcs aren't prefix-free
proc sortLegalStackProcs {} {
   global legalStackProcs

   # sort by least significant first, i.e.
   # alphabetically; it is possible because lsort is stable
   set legalStackProcs [lsort -dictionary $legalStackProcs[set legalStackProcs {}]]

   # sort by length
   # decorate
   set len [llength $legalStackProcs]
   for {set i 0} {$i < $len} {incr i} {
      set proc [lindex $legalStackProcs $i]
      set legalStackProcs [lreplace $legalStackProcs[set legalStackProcs {}] $i $i [list $proc [string length $proc]]]
   }

   # sort
   set legalStackProcs [lsort -integer -decreasing -index 1 $legalStackProcs[set legalStackProcs {}]]

   # undecorate
   for {set i 0} {$i < $len} {incr i} {
      set proc [lindex [lindex $legalStackProcs $i] 0]
      set legalStackProcs [lreplace $legalStackProcs $i $i $proc[set legalStackProcs {}]]
   }
}



proc main {} {
   global stack
   global prompt

   initialiseReadline
   defineConstants
   defineGenericMathsProcs
   defineCurrencyConverters
   defineAliases
   sortLegalStackProcs
   displayAdvice

   while 1 {
      # display prompt and get input
      try {
         set input [::tclreadline::readline read $prompt]
      } on error err {
         puts "$err"
      }

      if {[string trim $input] eq ""} {
         continue
      }

      snapshotStack
      foreach token [split [string trim [regsub {\s+} $input { }]] { }] {
         if {! [processToken $token]} {
            break
         }
      }
      print ;# automatic print
   }

   return
}

main
