#!/usr/bin/env tclsh

package require Tcl 8.6-
package require stefCoroutines


namespace eval cartesianProduct {
   proc whole {A B} {
      set AxB [list]

      foreach a $A {
         foreach b $B {
            lappend AxB [list $a $b]
         }
      }

      return $AxB
   }
   ::stefCoroutines::generatorProc generator {A B} {
      foreach a $A {
         foreach b $B {
            yield [list $a $b]
         }
      }
   }


   namespace export {[a-z]*}
}
