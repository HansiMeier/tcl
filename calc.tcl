#!/usr/bin/env tclsh
package require tclreadline
package require Tcl 8.6-
package require convertCurrencies ;# to convert the currencies
package require cartesianProduct
package require stefMath
package require misc ;# for normalizeProcname


namespace import ::tcl::mathop::*
namespace import ::tcl::mathfunc::*

namespace eval ::tcl::mathfunc {
   
