#!/usr/bin/env tclsh
package require fileutil

proc checkSyntax {} {
   global argc
   global argv
   global argv0

   if {$argc == 1 || $argc == 2} {
      return
   }

   puts stderr "Wrong arguments \"$argv\""
   puts stderr "Usage: [file tail $argv0] <file with a single number to increment> \[increment, by default 1\]"
   exit 1
}

proc main {argv0 argc argv} {
   checkSyntax
   set increment 1
   if {$argc == 2} {
      set increment [lindex $argv 1]
      if {![string is entier $increment]} {
         throw {not number} "\"$increment\" isn't an integer!"
      }
   }

   set file [lindex $argv 0]

   if {![file exists $file]} {
      puts stderr "File does not exist, assuming -1"
      ::fileutil::writeFile $file -1
   }

   if {![file readable $file]} {
      throw {not readable} "\"$file\" isn't readable"
   }

   set fd [open $file r+]
   set number [read $fd]
   if {![string is entier $number]} {
      throw {not number} "\"$number\" isn't an integer!"
   }
   chan truncate $fd
   chan seek $fd 0 start
   puts $fd [incr number $increment]
   chan close $fd
   puts stdout $number
   return
}

main $argv0 $argc $argv

   
   
