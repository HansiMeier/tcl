#!/usr/bin/env tclsh

package require parseGnuOpts
package require lispy ;# for `

set DEFAULT_EDITOR /usr/local/bin/gvim
set DEFAULT_EXT tcl
set SKELETON "$::env(TCL)/skeleton.tcl"
set CEXT_SKELETON "$::env(TCL)/skeleton.tclext.c"
set CPPEXT_SKELETON "$::env(TCL)/skeleton.tclext.cpp"
set TMEXT_SKELETON "$::env(TCL)/skeleton.tclext.tm"
set TM_SKELETON "$::env(TCL)/skeleton.tm"
set FOUND_EDITOR {}

proc getEditor {} {
   global DEFAULT_EDITOR
   global FOUND_EDITOR
   if {[llength $FOUND_EDITOR]} {
      return $FOUND_EDITOR
   }
   if {[file executable [lindex [split $::env(VISUAL)] 0]]} {
      set FOUND_EDITOR $::env(VISUAL)
   } elseif {[file executable $DEFAULT_EDITOR]} {
      set FOUND_EDITOR $DEFAULT_EDITOR
   } else {
      puts stderr "No viable editor found!"
      exit 1
   }
}

proc openWithEditor file {
   exec {*}[getEditor] $file 2> /dev/null &
}

proc isCExtFile file {
   return [regexp -nocase {\.tclext\.c$} $file]
}

proc isCPPExtFile file {
   return [regexp -nocase {\.tclext\.cpp$} $file]
}

proc isJimFile file {
   return [regexp -nocase {\.jim$} $file]
}

proc isTmFile file {
   return [regexp -nocase {\.tm$} $file]
}

proc isTclFile file {
   return [regexp -nocase {\.tcl$} $file]
}

proc isTclPPSFile file {
   return [regexp -nocase {\.tclpps$} $file]
}



proc meaningfulFile file {
   global env
   global DEFAULT_EXT

   # Beginn with the simplest case
   if {[file exists $file]} {
      return [list 1 $file]
   }
   
   # Assume relative paths are relative to $TCLDIR
   # if $file has no /
   if {[string equal [file pathtype $file] relative]} {
      set file [file join $env(TCL) $file]
      # Glob files
      set filesFound [lsort -dictionary -increasing [glob -nocomplain -types f "$file*"]]
      if {[llength $filesFound]} {
         return [list 1 [lindex $filesFound 0]]
      }
   }

   # Add extension if necessary
   if {[regexp {^[^\.]+$} $file]} {
      set file "$file.$DEFAULT_EXT"
   }
   # Nothing found
   return [list 0 $file]
}

proc makeNewFile {file skeletonNeeded} {
   global SKELETON
   global CEXT_SKELETON
   global CPPEXT_SKELETON
   global TMEXT_SKELETON
   global TM_SKELETON
   global env

   if {$skeletonNeeded} {
      file copy -- $SKELETON $file
      return
   }
   set fd [open $file a]


   if {[isJimFile $file]} {
      puts $fd "#!/usr/bin/env jimsh\n"
   } elseif {[isTclFile $file]} {
      puts $fd "#!/usr/bin/env tclsh\n"
   } elseif {[isTclPPSFile $file]} {
      puts $fd "#!/usr/bin/env tclpps\n"
   } else {
      if {[isCExtFile $file]} {
         set skeleton $CEXT_SKELETON
         set isExt 1
      } elseif {[isCPPExtFile $file]} {
         set skeleton $CPPEXT_SKELETON
         set isExt 1
      } elseif {[isTmFile $file]} {
         set skeleton $TM_SKELETON
         regexp {^[^\-]+} [file tail $file] tmName
         set isExt 0
      }

      regexp {^[^\.]+} [file tail $file] extName

      proc comment args {
         return
      }

      if {! [file readable $skeleton]} {
         puts stderr "\"$skeleton\" not readable!"
         exit 2
      }

      set skeletonFd [open $skeleton r]
      puts -nonewline $fd [::lispy::` [read $skeletonFd]]
      chan close $skeletonFd

      if {$isExt && [file readable $TMEXT_SKELETON]} {
         set tmExtSkelFd [open $TMEXT_SKELETON r]
         set tmExtFd [open [file join $::env(TCL) ${extName}-0.tm] w]
         puts -nonewline $tmExtFd [::lispy::` [read $tmExtSkelFd]]
         chan close $tmExtFd
         chan close $tmExtSkelFd
      }

   }

   puts $fd ""
   chan close $fd
}

proc main {argv0 argv argc} {
   global SKELETON
   # $numberOfArgs == -1 means unlimited number of arguments
   set optDict [dict create \
      skeleton [dict create \
         shortOpt s \
         longOpt skeleton \
         numberOfArgs 0 \
         required 0 \
         usage "Add a skeleton from file $SKELETON to the created file instead of just a shebang." \
      ] \
      files [dict create \
         shortOpt "" \
         longOpt "" \
         numberOfArgs -1 \
         required 1 \
         usage "files to open/create" \
      ] \
   ]

   set generalUsage "[file tail $argv0]: opens or creates a new tcl/tm/jim file."

   ::parseGnuOpts::processArgs \
      $argv0 \
      $argv \
      $argc \
      $optDict \
      $generalUsage 
   # Import the option arrays into the current namespace
   ::parseGnuOpts::importOptArrays
   foreach fileArg $::files(passedArgs) {

      set res [meaningfulFile $fileArg]
      set file [lindex $res 1]
      set exists [lindex $res 0]

      if {! $exists} {
         makeNewFile $file $::skeleton(present)
         if {[isJimFile $file] || [isTclFile $file] || [isTclPPSFile $file]} {
            file attributes $file -permissions a+x
         }
      }
      openWithEditor $file
   }
}

main $argv0 $argv $argc

