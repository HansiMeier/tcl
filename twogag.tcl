#!/usr/bin/env tclsh

package require Tcl 8.6- ;# for dicts and tailcall
package require httpFollow
package require fileutil ;# for tempfile

set URL {http://twogag.com/}
set LINUX_VIEWER {eom} ;# needed because xdg-open works strangely on Linux (at least mine)
set STRIPFILE [::fileutil::tempfile].jpg



if {! [info exists ::env(PATH)]} {
    set ::env(PATH) {/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin}
    puts $::env(PATH)
}

proc fatalError {msg txt} {
    puts stderr $txt
    exit 1
}

interp alias {} signaliseError {} throw

proc getRealStripUrl htmlVar {
    upvar $htmlVar html
    set isoDate [clock format [clock seconds] -format %Y-%m-%d]
    set exp {src="([^"]+\.jpg)" alt=}
    if {! [regexp -- $exp $html _ realAddress]} {
        fatalError {REGEXP STRIP FAILED} "Looking for the strip location of today's strip failed!\nMaybe it doesn't exist yet."
   }
   puts stderr "Retrieving twogag strip location \"$realAddress\""
   return $realAddress
}

proc which {prog} {
    set absProg [auto_execok $prog]
    if {$absProg eq ""} {
        signaliseError {NOT_IN_PATH} "\"$prog\" not in \$PATH"
   }
   return $absProg
}

proc openWith file {
    global OPENPROG \
    LINUX_VIEWER
    if {! [info exists OPENPROG]} {
        if {[catch {set OPENPROG [which $LINUX_VIEWER]}] && [catch {set OPENPROG [which "open"]}]} {
            signaliseError {NO WHICH} "No suitable open program found!"
  }
   }
   puts "Opening \"$file\" with \"$OPENPROG\""
   exec $OPENPROG $file &
}

proc killViewerOnLinux {} {
    global LINUX_VIEWER \
    tcl_platform
    if {! [string equal $tcl_platform(os) {Linux}]} {
        return
   }
   puts "We're on Linux, killing \"$LINUX_VIEWER\" if running!"
   catch {exec killall $LINUX_VIEWER}
}

proc closeXeeWindows {} {
    return
    global tcl_platform
    if {! [string equal $tcl_platform(os) Darwin]} {
        return
   }

   # No space after Xee³ because iTerm already outputs one (bug?)
   puts stderr {We're on OSX, use ugly GUI AppleScript to close windows of Xee³if any are open.}

   exec osascript -e {
       if application "Xee³" is running then
       tell application "System Events"
       set allWindows to name of window of process "Xee³"
       repeat with i in allWindows
       click button 1 of window i of process "Xee³"
       end repeat
       end tell
       end if
   } >& /dev/null
   return
}

proc downloadToFile {url file} {
    if {[catch {set token [::http::geturl $url -follow 10]}]} {
        signaliseError {GET_FAILED} "Error retrieving \"$url\"!"
   }

   if {[::http::ncode $token] != 200} {
       puts stderr "Failed with HTTP-Code \"[::http::ncode $token]\""
       ::http::cleanup $token
       exit 2
   }


   set html [::http::data $token]
   set realUrl [getRealStripUrl html]
   ::http::cleanup $token

   if {[catch {set token [::http::geturl $realUrl]}]} {
       fatalError {GET_FAILED} "Error retrieving \"$realUrl\"!"
   }

   puts stderr "Downloading \"$realUrl\" to \"$file\""
   set fd [open $file w]
   chan configure $fd -translation binary
   puts -nonewline $fd [::http::data $token]
   close $fd
   ::http::cleanup $token
}

proc main {url file} {
    downloadToFile $url $file
    killViewerOnLinux
    closeXeeWindows
    openWith $file
}
main $URL $STRIPFILE

