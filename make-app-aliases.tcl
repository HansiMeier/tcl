#!/usr/bin/env tclsh

set appDirs [list /Applications /System/Applications]
set outFile $::env(HOME)/sh/app-aliases.sh

set blacklist [list vlc clion dictionary]
set blacklistGlobs [list *octave*]

foreach appDir $appDirs {
   foreach app [glob -nocomplain -directory $appDir *.app] {
      set basename [file rootname [file tail $app]]
      # Ignore hidden Apps
      if {[file attributes $app -hidden]} {
         puts stdout "Ignoring \"$basename\" as it's hidden."
         continue
      }
      set lowerName [string tolower $basename]


      if {$lowerName in $blacklist} {
         puts stdout "Skipping \"$app\" because it's on the blacklist!"
         continue
      }

      set matchesAnyGlob false
      set matchingGlob ""
      foreach globExpression $blacklistGlobs {
         if {[string match $globExpression $lowerName]} {
            set matchesAnyGlob true
            set matchingGlob $globExpression
            break
         }
      }

      if {$matchesAnyGlob} {
         puts stdout "Skipping \"$app\" because glob $matchingGlob matches it"
         continue 
      }

      set strippedName [regsub -all -- {[^[:lower:]]} $lowerName {}]
      lappend aliases "alias $strippedName=\"open '$app'\""
      puts stdout "Creating an alias \"$strippedName\" for \"$app\"."
   }
}

set fd [open $outFile w]
puts $fd [join $aliases \n]\n
chan close $fd

puts stderr "Shell aliases for applications in $appDirs have been written into $outFile."
