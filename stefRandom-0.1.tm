#!/usr/bin/env tclsh
namespace eval stefRandom {
    proc generateRandomBytes {numberOfBytes} {
        set randomFile /dev/random

        set randomDevice [open $randomFile rb]
        set randomBytes [read $randomDevice $numberOfBytes]
        chan close $randomDevice

        return $randomBytes
    }

    proc generateRandomAsciiString {numberOfChars} {
        set randomBytes [generateRandomBytes $numberOfChars]
        binary scan $randomBytes A${numberOfChars} randomAsciiChars
        return $randomAsciiChars
    }

    proc generateRandomAlphanumericString {numberOfChars} {
        set numberOfValidAlphanumericChars 0
        set randomString ""
        while {$numberOfValidAlphanumericChars < $numberOfChars} {
            set newChar [generateRandomAsciiString 1]
            if {[regexp -nocase {^[a-z0-9]$} $newChar]} {
                incr numberOfValidAlphanumericChars 1
                append randomString $newChar
            }
        }
        return $randomString
    }

    proc generateSafariStylePassword {} {
        return "[generateRandomAlphanumericString 3]-[generateRandomAlphanumericString 3]-[generateRandomAlphanumericString 3]-[generateRandomAlphanumericString 3]"
    }

    namespace export {[a-z]*}
}
