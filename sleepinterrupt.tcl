#!/usr/bin/env tclsh

package require duration
namespace import duration::*

set exitCodeSuccess 0
set exitCodeInterruptedByFile 1
set exitCodeInterruptedBySignal 2
set exitCodeHelp 255

set signalHandlerInstalled 0
set interruptFileName ""
set globalInterruptFileName ""

set amIDebugging 0

proc debugPuts {args} {
    global amIDebugging
    if {$amIDebugging} {
        puts *$args
    }
}

set now [clock seconds]

proc tryInstallingSignalHandler {} {
    global signalHandlerInstalled
    try {
        package require tclsignal
        proc interruptedBySignal {} {
            global exitCodeInterruptedBySignal
            puts stderr "\nAborted by signal"
            exit $exitCodeInterruptedBySignal
        }

        signal add SIGINT interruptedBySignal -async
        signal print
        set signalHandlerInstalled 1
    } on error _ {
        puts stderr "Signal handler not installed"
    }
}



proc sleepInterrupt {durationInSeconds} {
    global interruptFileName exitCodeSuccess signalHandlerInstalled now
    set sleepIntervalInSeconds [expr {max(1, min(10, $durationInSeconds / 10))}]
    set sleepIntervalInMilliseconds [expr {$sleepIntervalInSeconds * 1000}]
    set durationInMilliseconds [expr {$durationInSeconds * 1000}]

    set targetTimeInSeconds [clock add $now $durationInSeconds seconds]

    puts stderr "Sleeping for [toHourMinuteSecondsDescription $durationInSeconds] until [clock format $targetTimeInSeconds]. Options to abort:"
    puts stderr ""
    puts stderr "- Create file $interruptFileName"
    puts stderr "  The file creation command would be"
    puts stderr "  echo 1 > $interruptFileName"
    if {$signalHandlerInstalled} {
        set pid [pid]

        puts stderr ""
        puts stderr "- send SIGINT to PID $pid"
        puts stderr "  The kill command would be "
        puts stderr "  kill -INT $pid"
    }

    set timeLeftMessage ""

    for {set timePassedInMilliseconds 0} {$timePassedInMilliseconds < $durationInMilliseconds} {incr timePassedInMilliseconds $sleepIntervalInMilliseconds} {
        set timeLeftInSeconds [expr {($durationInMilliseconds - $timePassedInMilliseconds) / 1000}]
        
        set backspacesToClear [string repeat \b [string length $timeLeftMessage]]
        puts -nonewline stderr $backspacesToClear

        set timeLeftMessage "Left [toHourMinuteSecondsDescription $timeLeftInSeconds] to sleep."
        puts -nonewline stderr $timeLeftMessage

        checkIfInterruptedByFile
        after $sleepIntervalInMilliseconds
    }
    
    set backspacesToClear [string repeat \b [string length $timeLeftMessage]]
    puts stderr $backspacesToClear

    puts stderr "Slept all the way."
    exit $exitCodeSuccess
}

proc getTempDir {} {
    set tmpdir {}
    set tdlist {}
    set dlist [list TMPDIR TEMP TMP]
    foreach {d} $dlist {
    if { [info exists ::env($d)] } {
        lappend tdlist $::env($d)
    }
    }
    # add more locations to this list if needed
    lappend tdlist C:/temp c:/tmp /tmp /var/tmp
    foreach {d} $tdlist {
        if { [file exists $d] && [file isdirectory $d] } {
            set tmpdir $d
            break
        }
    }
    return [file normalize $tmpdir]
}

proc checkIfInterruptedByFile {} {
    global interruptFileName globalInterruptFileName exitCodeInterruptedByFile
    if {[file exists $interruptFileName] || [file exists $globalInterruptFileName]} {
        puts stderr "File $interruptFileName or $globalInterruptFileName exists. Aborted."
        if {[file exists $interruptFileName]} {
            file delete $interruptFileName
        }

        if {[file exists $globalInterruptFileName]} {
            file delete $globalInterruptFileName
        }

        exit $exitCodeInterruptedByFile
    }
}

proc getInterruptFileNames {} {
    return [list [file join [getTempDir] sleepinterrupt[pid]] [file join [getTempDir] sleepinterrupt]]
}

proc displayHelpAndExit {argv0} {
    global exitCodeHelp
    set tempDir [getTempDir]
    puts stderr "Usage $argv0 <duration>"
    puts stderr "Duration is as in sleep command. E.g. 10m or 10s, or an absolute time, such as 19:34"
    puts stderr "Sleeps and exits 0. If $tempDir/sleepinterrupt<pid> or $tempDir/sleepinterrupt exists or process is sent SIGINT sleep is interrupted, returns nonzero"
    exit $exitCodeHelp
}

proc main {argc argv0 argv} {
    global interruptFileName globalInterruptFileName now
    if {$argc == 0} {
        displayHelpAndExit $argv0
    }

    tryInstallingSignalHandler
    
    lassign [getInterruptFileNames] interruptFileName globalInterruptFileName
    sleepInterrupt [parseDuration [join $argv " "] $now]
}

main $argc $argv0 $argv
