#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval dirSize {
   proc Ls {dir args} {
      tailcall glob -nocomplain {*}$args -directory $dir *
   }

   proc dirSize dir {
      set count 0

      foreach thing [::dirSize::Ls $dir] {
         if {[file isfile $thing]} {
            incr count [file size $thing]
         } elseif {[file isdirectory $thing]} {
            incr count [dirSize $thing]
         }
      }

      return $count
   }



   namespace export {[a-z]*}
}


