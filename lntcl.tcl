#!/usr/bin/env tclsh

set BIN /usr/local/bin

if {[info exists ::env(TCL)]} {
   set TCLDIR $::env(TCL)
} else {
   set TCLDIR [file join $::env(HOME) tcl]
}

proc displayUsageAndExit {} {
   global argv0
   global BIN
   puts stderr "Usage: $argv0 <tclfile>"
   puts stderr "Symlinks <tclfile> to $BIN/\$(basename <tclfile> .tcl)"
   exit 1
}

proc isGoodFile {file} {
   return [expr {[file isfile $file] && [file readable $file]}]
}

if {! [string equal -nocase $::tcl_platform(user) root]} {
   puts stderr "Need to be root!"
   displayUsageAndExit
}

if {$argc != 1} {
   puts stderr "Wrong number of arguments: $argc"
   displayUsageAndExit
}

set file [lindex $argv 0]

# Sanitize $file
if {! ([file pathtype $file] eq "absolute" || [file isfile [set file [file join [pwd] $file]]])} {
   set file [file join $TCLDIR [file tail $file]]
}
if {! [regexp -nocase {\.tcl$} $file]} {
   append file .tcl
}

if {! [file exists $file]} {
   puts stderr "File \"$file\" does not exist!"
   exit 252
}

if {! [file readable $file]} {
   puts stderr "File \"$file\" isn't readable!"
   exit 253
}

regexp {^(.+[^\.])\.} [file tail $file] _ basename

if {[catch {file attributes $file -permissions a+x}]} {
   puts stderr "Making \"$file\" executable went wrong!"
   exit 254
}

set linkTarget [file join $BIN $basename]

if {[catch {file link -symbolic $linkTarget $file}]} {
   puts stderr "Linking \"$file\" to \"$linkTarget\" went wrong!"
   exit 255
}

