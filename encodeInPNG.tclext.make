UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	SHAREDLIBSUFFIX := dylib
else
	SHAREDLIBSUFFIX := so
endif

ifdef DEBUG
	EXT := debug.$(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -ggdb
else
	EXT := $(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -DNDEBUG
endif

ENCODEINPNGSOURCE := $(HOME)/CCPP/misc

LDFLAGS := -L$(TCLLIB) -ltclstub$(TCLVERSION) -lm 
INCLUDES := -I/usr/local/include -I$(TCLINCLUDE) -I$(CPPTCLINCLUDE) -I$(ENCODEINPNGSOURCE)


CPPSTD := -std=c++11
CPPFLAGS := -Os -shared -fPIC -DUSE_TCL_STUBS -Wall

CSTD := -std=c99
CFLAGS := -Os -shared -fPIC -DUSE_TCL_STUBS -Wall

ifndef CPPC
	CPPC := g++
endif

ifndef CC
	CC := gcc
endif


%.o: %.cc
	$(CPPC) $(CPPSTD) $(DEBUGFLAGS) $(CPPFLAGS) $(ADDTL_CPPFLAGS) $(INCLUDES) $(ADDTL_INCLUDES) -c $<.cc -o $<.o

%.o: %.c
	$(CC) $(CFLAGS) $(DEBUGFLAGS) $(CPPFLAGS) $(ADDTL_CPPFLAGS) $(INCLUDES) $(ADDTL_INCLUDES) -c $<.c -o $<.o

encodeInPNG.$(EXT): $(ENCODEINPNGSOURCE)/encodeInPNG.c