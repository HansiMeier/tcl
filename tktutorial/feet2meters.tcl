#!/usr/bin/env wish
package require Tk
namespace import ::tcl::mathop::*

proc otherIs metersOrFeet {
    set otherDict [dict create feet meters meters feet]
    tailcall dict get $otherDict $metersOrFeet
}

set numberOfDecimalPlaces 7

proc feet2meters feet {
    tailcall * $feet 0.3048
}

proc meters2feet meters {
    tailcall / $meters 0.3048
}

proc roundToDecimalPlaces {number decimalPlaces} {
    set decimal [expr {double(10**$decimalPlaces)}]
    return [expr {round(double($number) * $decimal) / $decimal}]
}

proc updateLength {this otherValue} {
    global $this writingMyself numberOfDecimalPlaces
    set this [string tolower $this]
    set other [otherIs $this]
    if {![string is double $otherValue]} {
        set $this ""
        return 1
    }
    catch {set $this [roundToDecimalPlaces [${other}2${this} $otherValue] $numberOfDecimalPlaces]}
    return 1
}

proc main {} {
    global feet meters

    set feet 0.0
    set meters [feet2meters $feet]

    # The window title
    wm title . "Feet <-> Meters"
    # The main frame
    grid [ttk::frame .c -padding "3 3 12 12"] -column 0 -row 0 -sticky nwes

    # automatically resize
    grid columnconfigure . 0 -weight 1
    grid rowconfigure . 0 -weight 1

    # the entry boxes
    # grid is the layout manager
    set row 1
    set units [list feet meters]
    foreach unit $units {
        set otherUnit [otherIs $unit]
        grid [ttk::label .c.${unit}Label -text "[string totitle $unit]: "] -column 1 -row $row -sticky we
        grid [ttk::entry .c.${unit} -textvariable $unit -validate key -validatecommand [list updateLength $otherUnit %P]] -column 2 -row $row -sticky we
        incr row 1
    }

    foreach w [winfo children .c] {grid configure $w -padx 5 -pady 5}
    focus .c.[lindex $units 0]
}

main
    
