#!/usr/bin/env tclsh

package require Tcl 8.6-

set FAT32MAP_DEFAULT {
   ? _
   : _
   * _
   | _
   \" '
   ~ -
   < (
   > )
   \{ (
   \} )
   [ (
   ] )
}

if {[info exists ::env(TCL)]} {
   set FAT32MAP_FILE [file join $::env(TCL) fat32map.txt]
} else {
   set FAT32MAP_FILE fat32map.txt
}

set DELIMITER / ;# delimiter for FATMAP32_FILE

proc loadFAT32 {} {
   global FAT32MAP \
          DELIMITER \
          FAT32MAP_FILE \
          FAT32MAP_DEFAULT

   if {!([file readable $FAT32MAP_FILE] && [string equal [file type $FAT32MAP_FILE] file])} {
      set FAT32MAP $default
      return
   }

   set fd [open $FAT32MAP_FILE r]
   set lines [split [chan read $fd [file size $FAT32MAP_FILE]] \n]
   set FAT32MAP {}
   foreach line $lines {
      set toAdd [split $line $DELIMITER]
      if {! [llength $toAdd]} {
         continue
      }
      if {[llength $toAdd] != 2} {
         puts stderr "Invalid line while reading \"$FAT32MAP_FILE\": \"$line\", using default map"
         set FAT32MAP $FAT32MAP_DEFAULT
         return
      }
      
      lappend FAT32MAP [lindex $toAdd 0]
      lappend FAT32MAP [lindex $toAdd 1]
   }

   return
}

proc loadPercent {} {
   package require fileUrl
   return
}

proc determineRenamingFunction {} {
   global argv0

   switch -nocase -- [file tail $argv0] {
      renpercentenc {
         loadPercent
         interp alias {} getNewName {} fileUrl::percentEncode
      }

      renpercentdec {
         loadPercent
         interp alias {} getNewName {} fileUrl::percentDecode
      }

      renpercent {
         loadPercent
         proc getNewName file {
            # use a simple heuristic to decide whether to encode or to decode
            if {[string first % $file] == -1} {
               tailcall ::fileUrl::percentEncode $file
            } else {
               tailcall ::fileUrl::percentDecode $file
            }

            return
         }
      }

      renfat32 -
      default {
         loadFAT32
         interp alias {} getNewName {} \
            apply [list \
               tail {
                  global FAT32MAP
                  return [string map $FAT32MAP $tail]
               }
            ]
      }
   }

   return
}

proc sanitisePath path {
   # file normalize makes the path absolute and dereferences all symbolic links in the dirname.
   # It happily works on nonexisting paths.
   set path [file normalize $path]

   if {! [file exists $path]} {
      signaliseError {NOT FOUND} "\"$path\" doesn't exist!"
   }
   if {! [file readable $path]} {
      signaliseError {NOT READABLE} "\"$path\" is not readable!"
   }

   set type [file type $path]

   set linkPath $path
   # determine real type
   while {[string equal $type link]} {
      set linkPath [file readlink $linkPath]
      set type [file type $linkPath]
   }


   if {$type ni {directory file}} {
      signaliseError {BAD TYPE} "Bad file type $type, can only handle directories, files and symlinks to them."
   }
   return [list $type $path]
}

proc renameFile {file dirname tail} {
   global FAT32MAP
   set newFile [file join $dirname [getNewName $tail]]
   if {! [string equal $file $newFile]} {
      file rename $file $newFile
   }

   return
}

proc dispatchCorrectRename objects {
   foreach object $objects {
      # inform the user if the object is nonexistent or not readable but
      # continue operation
      if {[catch {set sanePathPair [sanitisePath $object]}]} {
         continue
      }

      set type [lindex $sanePathPair 0]
      set sanePath [lindex $sanePathPair 1]
      set tail [file tail $sanePath]
      set dirname [file dirname $sanePath]

      if {[string equal $type file]} {
         renameFile $sanePath $dirname $tail
      } else {
         recursivelyRenameFiles $sanePath $dirname $tail
      }
   }
   return
}

proc recursivelyRenameFiles {dir dirname tail} {
   global FAT32MAP
   set newDir [file join $dirname [getNewName $tail]]
   if {! [string equal $dir $newDir]} {
      file rename $dir $newDir
   }

   dispatchCorrectRename [lsort -dictionary [glob -nocomplain -directory $newDir -- *]]
   return
}


   
proc signaliseError {msg text} {
   # put the error message so that it gets displayed even if the exception is catched.
   puts stderr $text
   throw $msg $text
   return
}

# Like signaliseError but this error shall not be recoverable
# same syntax as signaliseError so that they are interchangable for debugging.
proc fatalError {ignore text} {
   puts stderr $text
   exit 2
}


proc displayUsageAndExit argv0 {
   puts stderr "Usage: [file tail $argv0] <directory/file to rename 1> \[directory/file to rename 2\]"
   puts stderr "This program strips some characters from the filename to be windows compatible."
   exit 1
}

proc main {argc argv0 argv} {
   if {$argc < 1} {
      displayUsageAndExit $argv0
   }

   determineRenamingFunction
   dispatchCorrectRename $argv

   return
}

main $argc $argv0 $argv

