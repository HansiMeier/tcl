#!/usr/bin/env tclsh
set packagesToLoad [list fileutil httpFollow]
set namespacesToImport [list fileutil tcl::mathop]

if {$argc < 1 || [lindex $argv 0] in {"-h" "--help"}} {
    puts stderr "Usage: [file tail $argv0] <tcl expression to eval> <arg 1> \[arg 2\] \[arg 3\] …"
    puts stderr "Evals the tcl expression given, printing any results to stdout"
    puts stderr "Beforehand, it loads the packages $packagesToLoad and imports the namespaces $namespacesToImport"
    exit 1
}

foreach package $packagesToLoad {
    package require $package
}

foreach namespace $namespacesToImport {
    namespace import ${namespace}::*
}

set toEval [lindex $argv 0]
set argv [lrange $argv 1 end]
set argc [llength $argv]

try {
    set result [eval $toEval]
} on error err {
    puts stderr $err
    exit 2
}

set result [string trim [join $result " "]]

if {$result ne ""} {
    puts stdout $result
}
