#include <tcl.h>
#include <encodeInPNG.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/stat.h>

#if __STDC_VERSION__ < 199901L
   #error "Requires C99-compliant compiler!"
#endif



// bitmasks normally defined in unistd.h, here for portablility
#ifndef F_OK
#   define F_OK		00
#endif
#ifndef X_OK
#   define X_OK		01
#endif
#ifndef W_OK
#   define W_OK		02
#endif
#ifndef R_OK
#   define R_OK		04
#endif


/**************************************************************************************************
 *                                     TCL UTILITY FUNCTIONS                                      *
 **************************************************************************************************/

#define startErrorMsgFile(msg, toAppend) \
   Tcl_Obj* errorMessage = Tcl_NewStringObj(msg, sizeof(msg)); \
   Tcl_AppendToObj(errorMessage, toAppend, strlen(toAppend)); \
   Tcl_SetObjResult(interp, errorMessage); \
   return TCL_ERROR;

#define myFree(ptr) \
   if (ptr) { \
      free(ptr); \
      ptr = NULL; \
   }

#define myTclFree(ptr) \
   if (ptr) { \
      ckfree(ptr); \
      ptr = NULL; \
   }

#define guard(cond, msg, filename) \
   if (! (cond)) { \
      cleanUp; \
      if (msg) { \
         startErrorMsgFile(msg, filename); \
      } else { \
         return TCL_ERROR; \
      } \
   }

#define fcloseCheck(fp, filename) \
   if (fclose(fp)) { \
      startErrorMsgFile("Could not close: ", filename); \
   }


bool isRegularFile(Tcl_Obj* pathPtr) {
   Tcl_StatBuf* statBufp = Tcl_AllocStatBuf();

   unsigned mode = S_IFDIR;

   Tcl_Obj* myPathPtr = pathPtr;

   for (;;) {

      if (Tcl_FSStat(myPathPtr, statBufp)) {
         break;
      }

      mode = Tcl_GetModeFromStat(statBufp);

      if (!S_ISLNK(mode)) {
         break;
      }

      if (myPathPtr != pathPtr) {
         Tcl_DecrRefCount(myPathPtr);
      }

      myPathPtr = Tcl_FSLink(myPathPtr, NULL, 0);

      if (! myPathPtr) {
         break;
      }
   }

   ckfree(statBufp);

   return(S_ISREG(mode));
}



/**************************************************************************************************
 *                                              TCL                                               *
 **************************************************************************************************/

#define cleanUp \
   if (outFilenameObj) { \
      Tcl_DecrRefCount(outFilenameObj); \
      outFilenameObj = NULL; \
   } \
   myFree(data); \
   myTclFree(remObjv);

int readPNGCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   char* outFilename = NULL;
   Tcl_Obj* outFilenameObj = NULL;
   char* data = NULL;

   const Tcl_ArgvInfo argTable[] = {
      {
         .type = TCL_ARGV_STRING,
         .keyStr = "-outfile",
         .dstPtr = &outFilename,
         .helpStr = "Writes the read bytes directly to a file"
      },
      TCL_ARGV_AUTO_HELP,
      TCL_ARGV_AUTO_REST,
      TCL_ARGV_TABLE_END
   };

   Tcl_Obj** remObjv = NULL;
   if (Tcl_ParseArgsObjv(interp, argTable, &objc, objv, &remObjv) == TCL_ERROR) {
      return TCL_ERROR;
   }

   if (outFilename) {
      outFilenameObj = Tcl_NewStringObj(outFilename, strlen(outFilename));
      Tcl_IncrRefCount(outFilenameObj);
   }
   
   // Exactly one argument needed (remObjv[0] is the command name)
   if (objc != 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "?-outfile outFile? filename");
      return TCL_ERROR;
   }

   // Get the string out of the argument
   const char* pngFilename = Tcl_GetString(remObjv[1]);

   // Tcl_FSAccess has errorlevel semantics
   guard(pngFilename, NULL, "dummy");
   guard(!Tcl_FSAccess(remObjv[1], F_OK), "File doesn't exist: ", pngFilename);
   guard(isRegularFile(remObjv[1]), "File isn't a regular file: ", pngFilename);
   guard(!Tcl_FSAccess(remObjv[1], R_OK), "File isn't readable: ", pngFilename);

   if (outFilenameObj && !Tcl_FSAccess(outFilenameObj, F_OK)) {
      if (isRegularFile(outFilenameObj)) {
         guard(
               !Tcl_FSAccess(outFilenameObj, W_OK),
               "-outfile option exists but isn't writable: ",
               outFilename
            );
      } else {
         cleanUp;
         startErrorMsgFile("-outfile option exists but isn't a file: ", outFilename);
      }
   }

   size_t size = 0;
   const encodeInPNG_error_t err = readDataAsPNG(pngFilename, &size, &data, false);

   if (err != SUCCESS) {
      const char errStart[] = "Could not read data from PNG file \"";

      Tcl_Obj* errorMessage = Tcl_NewStringObj(errStart, sizeof(errStart));
      char errCode[50] = "\", Error ";
      snprintf(errCode + sizeof("\", Error "), 49, "%d.", err);
      Tcl_AppendToObj(errorMessage, pngFilename, strlen(pngFilename));
      Tcl_AppendToObj(errorMessage, errCode, strlen(errCode));
      Tcl_SetObjResult(interp, errorMessage);
      cleanUp;
      return TCL_ERROR;
   }


   if (outFilename) {
      #define FILEGuard(cond, msg) \
         if (!(cond)) { \
            cleanUp; \
            fcloseCheck(fp, outFilename); \
            startErrorMsgFile(msg, outFilename); \
         }
      FILE* fp = fopen(outFilename, "wb");
      FILEGuard(fp, "Could not open file: ");

      fwrite(data, size, 1, fp);
      FILEGuard(!ferror(fp), "Could not write data to: ");
      fcloseCheck(fp, outFilename);

      Tcl_ResetResult(interp);
      #undef FILEGuard
   } else {
      Tcl_SetObjResult(interp, Tcl_NewStringObj(data, size));
   }

   cleanUp;
   return TCL_OK;
}


#undef cleanUp


int readPNGRowCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   // Exactly one argument needed (objv[0] is the command name)
   if (objc != 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "filename");
      return TCL_ERROR;
   }

   // Get the string out of the argument
   const char* filename = Tcl_GetString(objv[1]);
   if (! filename) {
      return TCL_ERROR;
   }

   // Tcl_FSAccess has errorlevel semantics
   if (Tcl_FSAccess(objv[1], R_OK)) {
      startErrorMsgFile("File isn't readable: ", filename);
   }

   if (!isRegularFile(objv[1])) {
      startErrorMsgFile("File isn't a regular file: ", filename);
   }

   static char* data;
   static size_t size;
   const encodeInPNG_error_t err = readDataAsPNG(filename, &size, &data, true);
   switch (err) {
      case SUCCESS: {
         // Return
         Tcl_SetObjResult(interp, Tcl_NewStringObj(data, size));
         return TCL_OK;
         break;
      }

      case FINISHED: {
         // Break
         free(data);
         return TCL_BREAK;
         break;
      }

      default: {
         char errMsg[200];
         snprintf(
               errMsg,
               sizeof(errMsg) - 1,
               "Could not read data from PNG file \"%s\", Error %d",
               filename,
               err);
         Tcl_Obj* errorMessage = Tcl_NewStringObj(errMsg, strlen(errMsg));
         Tcl_SetObjResult(interp, errorMessage);
         return TCL_ERROR;
         break;
      }
   }

}

#define cleanUp \
   if (inFilenameObj) { \
      Tcl_DecrRefCount(inFilenameObj); \
      inFilenameObj = NULL; \
      myFree(data); \
   } \
   myTclFree(remObjv);


int writePNGCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   char* inFilename = NULL;
   Tcl_Obj* inFilenameObj = NULL;
   char* data = NULL;


   const Tcl_ArgvInfo argTable[] = {
      {
         .type = TCL_ARGV_STRING,
         .keyStr = "-infile",
         .dstPtr = &inFilename,
         .helpStr = "Read the data from a file instead."
      },
      TCL_ARGV_AUTO_HELP,
      TCL_ARGV_AUTO_REST,
      TCL_ARGV_TABLE_END
   };

   Tcl_Obj** remObjv = NULL;
   if (Tcl_ParseArgsObjv(interp, argTable, &objc, objv, &remObjv) == TCL_ERROR) {
      return TCL_ERROR;
   }

   if (inFilename) {
      inFilenameObj = Tcl_NewStringObj(inFilename, strlen(inFilename));
      Tcl_IncrRefCount(inFilenameObj);
   }


   // Exactly two arguments needed (objv[0] is the command name)
   if (objc != 3 - (bool) inFilename) {
      Tcl_WrongNumArgs(interp, 1, objv, "outfile (-infile infile | string)");
      return TCL_ERROR;
   }

   // Get the string out of the argument
   int pngFilenameLength;
   const char* pngFilename = Tcl_GetStringFromObj(remObjv[1], &pngFilenameLength);
   if (!pngFilename) {
      return TCL_ERROR;
   }

   // Tcl_FSAccess has errorlevel semantics
   // remObjv[1] is the pngFilename object
   if (!Tcl_FSAccess(remObjv[1], F_OK)) {
      guard(isRegularFile(remObjv[1]), "File isn't a regular file: ", pngFilename);
      guard(!Tcl_FSAccess(remObjv[1], W_OK), "File exists but isn't writable: ", pngFilename);
   }

  
   long size = 0; 
   if (inFilename) {
      guard(!Tcl_FSAccess(inFilenameObj, F_OK), "-infile option doesn't exist: ", inFilename);
      guard(!Tcl_FSAccess(inFilenameObj, R_OK), "-infile option exists but isn't readable: ", inFilename);
      guard(isRegularFile(inFilenameObj), "-infile option exists but isn't a regular file: ", inFilename);

      FILE* fp = fopen(inFilename, "rb");
      guard(fp, "Could not open: ", inFilename);

      // get file size
      fseek(fp, 0, SEEK_END);
      size = ftell(fp);

      #define FILEGuard(cond, msg) \
         if (!(cond)) { \
            cleanUp; \
            fcloseCheck(fp, inFilename); \
            startErrorMsgFile(msg, inFilename); \
         }
         
         
      FILEGuard(size > 0, "Could not get size of: ");
      rewind(fp);

      data = malloc(size);
      FILEGuard(data, "Could not allocate space for reading: ");

      fread((void*) data, size, 1, fp);
      FILEGuard(!ferror(fp), "Error reading from: ");

      fcloseCheck(fp, inFilename);

      #undef FILEGuard
   } else {
      data = Tcl_GetStringFromObj(remObjv[2], (int*) &size);
      if (! data) {
         cleanUp;
         return TCL_ERROR;
      }
   }

   const encodeInPNG_error_t err = writePNG(pngFilename, pngFilenameLength, size, (void*) data);

   if (err != SUCCESS) {
      const char errStart[] = "Could not write data to PNG file \"";
      Tcl_Obj* errorMessage = Tcl_NewStringObj(errStart, sizeof(errStart));
      char errCode[50] = "\", Error ";
      sprintf(errCode + sizeof("\", Error "), "%d.", err);
      Tcl_AppendToObj(errorMessage, pngFilename, pngFilenameLength);
      Tcl_AppendToObj(errorMessage, errCode, strlen(errCode));
      Tcl_SetObjResult(interp, errorMessage);
      cleanUp;
      return TCL_ERROR;
   }

   Tcl_ResetResult(interp);
   cleanUp;
   return TCL_OK;
}

#undef cleanUp

int clearRowBufferCmd(
      ClientData cdata,
      Tcl_Interp* interp,
      int objc,
      Tcl_Obj* const objv[]) {

   if (objc != 1) {
      Tcl_WrongNumArgs(interp, 1, objv, NULL);
      return TCL_ERROR;
   }

   if(readDataAsPNG(NULL, NULL, NULL, false) == CLEANED_UP) {
      // Special cleanup call
      return TCL_OK;
   }
   return TCL_ERROR;
}


#undef guard
#undef myFree
#undef myTclFree
#undef fcloseCheck
#undef startErrorMsgFile

/**************************************************************************************************
 *                                              INIT                                              *
 **************************************************************************************************/

#define NAMESPACE "encodeInPNG"
#define declareCommand(command) \
   Tcl_CreateObjCommand(interp, "::" NAMESPACE "::" #command, &(command ## Cmd), NULL, NULL);
int Encodeinpng_Init(Tcl_Interp* interp) {
   Tcl_Namespace* nsPtr;

   #ifdef USE_TCL_STUBS
   if (Tcl_InitStubs(interp, "8.1" , 0) == 0L) {
      return TCL_ERROR;
   }
   #endif

   // Create the namespace
   nsPtr = Tcl_CreateNamespace(interp, NAMESPACE, NULL, NULL);
   if (!nsPtr) {
      return TCL_ERROR;
   }

   // Declare commands
   declareCommand(readPNG);
   declareCommand(readPNGRow);
   declareCommand(clearRowBuffer);
   declareCommand(writePNG);

   // Export all commands
   if (Tcl_Export(interp, nsPtr, "*", 0) == TCL_ERROR) {
      return TCL_ERROR;
   }

   return TCL_OK;
}

#undef NAMESPACE
#undef declareCommand


