#!/usr/bin/env tclsh


package require logic

foreach formula $argv {

   try {
      set dnf [logic::getDNF $formula]
   } on error _ {
      puts stderr "\"$formula\" is syntactically incorrect!"
      continue
   }

   set len [llength $dnf]

   for {set i 0} {$i != $len} {incr i 1} {
      set dnf [lreplace $dnf $i $i [join [lindex $dnf $i][set dnf {}] .]]
   }

   puts "$formula -> [join $dnf { + }]"
}

