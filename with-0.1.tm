package require Tcl 8.6-

namespace eval with {
    namespace eval Internal {
        proc With {variableName constructor destructor body} {
            set value [uplevel 1 {*}$constructor]
            uplevel 1 [list apply [list $variableName $body] $value]
            tailcall {*}$destructor $value
        }
    }

    proc open {fileName mode AS variableName body} {
        set constructor [list open $fileName $mode]
        set destructor [list chan close]
        tailcall ::with::Internal::With $variableName $constructor $destructor $body
    }

    namespace export *

    namespace ensemble create
}
