#!/usr/bin/env tclsh
# Socket server procedures.

proc server chan {
    if {[chan eof $chan]} {
        close $chan
        puts {SRV: remote connection closed}
    } else {
        set line [gets $chan]
        if {$line eq {}} return
        if {[regexp {^(\w+)\s+(.+)$} $line -> cmd args]} {
            switch $cmd {
                src {
                    if {[file readable $args]} {
                        lassign [list [file dirname $args] [file tail $args]] dir_name file_name
                        set hash [::crc::crc32 -format %X $args]
                        if {[info exists ::tkcon::TABS($hash)] && [winfo exists [set tab_name [lindex [split $::tkcon::TABS($hash) |] 0]]]} {
                            ::tkcon::GotoTab $tab_name
                        } else {
                            ::tkcon::NewTab
                            set ::tkcon::TABS($hash) $::tkcon::PRIV(curtab)|$args
                            $::tkcon::PRIV(curtab) tag configure srvcmd -background #BBFFBB
                            $::tkcon::PRIV(statusbar).file_name configure -text $file_name
                        }
                        tkcon show
                        tkcon console insert output "SOURCE $args\n" srvcmd
                        cd $dir_name
                        if {[catch {tkcon load $file_name} err]} {
                            puts stderr "SRV ERR: command {$line} failed with error:\n\t$err"
                        }
                    } else {
                        puts stderr "SRV ERR: file '$args' is not readable."
                    }
                }
            }
        } else {
            puts stderr "SRV ERR: Unknown command '$line'"
        }
    }
    return
}
# Version display procedures.
proc getver {} {
    lassign [ list $::tcl_platform(os)\ $::tcl_platform(osVersion) unknown ] os arch
    if { [ array exists ::activestate::ActiveTcl ] } {
        lassign [ list $::activestate::ActiveTcl(product)\ $::activestate::ActiveTcl(release) $::activestate::ActiveTcl(arch) ] tcl arch
    } else {
        lassign [ list Tcl\ $::tcl_patchLevel $::tcl_platform(machine) ] tcl arch
    }
    return "$tcl on $os ($arch)"
}
proc addver status_bar {
    grid [label $status_bar.version -text [getver] -foreground darkmagenta -bd 1 -relief sunken -padx 10] -row 0 -column 2 -padx 1 -sticky news
    grid configure $status_bar.cursor -column 3
}
# Start our code.
after idle {
    set status_bar $::tkcon::PRIV(statusbar)
    addver $status_bar
    if {[catch {
        package require crc32
        socket -server {apply {{chan addr port} {chan event $chan readable [list server $chan]}}} 8007
    } err_msg]} {
        puts stderr "SRV ERR: couldn't open the server's socket.\n\t$err_msg"
    } else {
        set col 2
        grid [label $status_bar.file_name -text unknown -foreground darkgreen -bd 1 -relief sunken -padx 10] -row 0 -column $col -padx 1 -sticky news
        foreach widget {version cursor} {grid configure $status_bar.$widget -column [incr col]}
        trace add variable ::tkcon::PRIV(curtab) write [list apply {{status_bar n1 n2 op} {
            upvar $n1 arr
            set curr_tab $arr($n2)
            if {[info exists ::tkcon::TABS]} {
                set data [array get ::tkcon::TABS]
                set text [expr {[set idx [lsearch -glob $data ${curr_tab}|*]] > -1 ? [file tail [lindex [split [lindex $data $idx] |] end]] : {unknown}}]
                $status_bar.file_name configure -text $text
            }
        }} $status_bar]
    }
    unset status_bar
}

