package require cartesianProduct
package require stefCoroutines
namespace import ::stefCoroutines::*

set pairs [::cartesianProduct::generator {1 2 3} {A B C}]

streamProc evenNums {} {set i -2} {
   yield [incr i 2]
}

foreachGenerated p $pairs e evenNums {
   puts "$p $e"
}
