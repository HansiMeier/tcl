#!/usr/bin/env tclsh

package require Tcl 8.5

namespace eval requirePkgFromTcllib {
    proc requirePkgFromTcllib {packageToLoad {version {}}} {
        global auto_path
        set tcllibFinalDestination [file join [pwd] tcllib]
        set tcllibTmpDestination [file join [pwd] tcllibTmp_[pid]]
        set packageRequireCommand [list package require $packageToLoad {*}$version]
        set rmTmpDirCommand [list file delete -force -- $tcllibTmpDestination]

        {*}$rmTmpDirCommand ;# Clean up remnants of old installations
        if {![catch {{*}$packageRequireCommand}]} {
            return
        }

        if {! [file exists $tcllibFinalDestination]} {
            set gitCommand [auto_execok git]
            if {$gitCommand eq {}} {
                error "No git executable found for downloading tcllib, please make a git executable available on \$PATH!"
            }
            set tcllibGitRepo {https://github.com/tcltk/tcllib.git}
            puts stderr "Downloading tcllib via git from $tcllibGitRepo"
            if {[catch {exec -ignorestderr {*}$gitCommand clone --depth 1 ${tcllibGitRepo} ${tcllibTmpDestination}}]} {
                {*}$rmTmpDirCommand
                error "Couldn't download tcllib!"
            }

            set installerCommand [list [info nameofexecutable] [file join ${tcllibTmpDestination} installer.tcl]]
            puts stderr "Installing tcllib into $tcllibFinalDestination"
            set installationOK 1
            if {[catch {exec {*}$installerCommand \
                -no-wait -no-html -no-examples -no-nroff \
                -no-apps -no-gui -pkgs -pkg-path ${tcllibFinalDestination}}]} {
                    set installationOK 0
                }
            {*}$rmTmpDirCommand
            if {!$installationOK} {
                error "Installation failed!"
            }
            puts stderr "Installation successful!"
        }

        if {$tcllibFinalDestination ni $auto_path} {
            lappend auto_path $tcllibFinalDestination
        }

        {*}$packageRequireCommand
    }


    namespace export {[a-z]*}
}
