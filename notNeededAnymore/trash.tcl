#!/usr/bin/env tclsh

set thisScriptDir [file normalize [file dirname [file readlink [info script]]]]
lappend ::auto_path $thisScriptDir
::tcl::tm::path add $thisScriptDir

package require Tcl 8.6
package require parseGnuOpts
namespace import ::parseGnuOpts::*

set exitLevel 0

set doNotDelete [list .. / $::env(HOME)]


try {
    set EUID $::env(EUID)
} on error err {
    set EUID 0
}

proc iAmRoot {} {
    return [expr {$::EUID == 0}]
}

proc pathComponents {path n} {
    set brokenUp [file split $path]
    tailcall file join {*}[lrange $brokenUp 0 $n]
}

proc pathComponent {path n} {
    set brokenUp [file split $path]
    tailcall lindex $brokenUp $n
}

proc checkIfOnOSX {} {
    if {$::tcl_platform(os) ne "Darwin"} {
        throw {not on osx} "Not on OSX!"
    }
    return
}

proc newNameFor file {
    if {![file exists $file]} {
        return $file
    }
    set dirname [file dirname $file]
    set tail [file tail $file]
    set maybeDot ""
    if {[string index $tail 0] eq "."} {
        set maybeDot .
        set tail [string range $tail 1 end]
    }
    set extension [file extension $tail]
    set basename [file rootname $tail]

    return [file join $dirname "$maybeDot$basename'$extension"]
}


proc tellFinderToTrash item {
    set item [file normalize $item]

    

# $item has to be absolute
proc trash item {
    set item [file normalize $item]
    if {![file exists $item]} {
        puts stderr "Skipping item \"$item\" as it doesn't exist!"
        set ::exitLevel 1
        return ""
    }

    if {![file writable $item]} {
        puts stderr "Skipping item \"$item\" as it isn't writable!"
        set ::exitLevel 1
        return ""
    }

    if {[pathComponents $item 1] eq "/Volumes" && [pathComponent $item 2] ne "Macintosh HD"} {
        set volume [pathComponents $item 2]
        set trash [file normalize [file join $volume .Trashes $::EUID]]
        set onExternalVolume 1
    } else {
        set trash [file normalize [file join $::env(HOME) .Trash]]
        set onExternalVolume 0
    }

    lappend ::doNotDelete $trash

    if {$item in $::doNotDelete} {
        puts stderr "Skipping item \"$item\"; I don't want to delete it!"
        set ::exitLevel 1
        return ""
    }

    if {[string first $item $trash] != -1} {
        puts stderr "Skipping item \"$item\"; It's a parent of the trash directory \"$trash\"!"
        set ::exitLevel 1
        return ""
    }

    if {![file isdirectory $trash]} {
        file mkdir $trash
    }

    set basename [file tail $item]
    set target [file join $trash $basename]

    if {$item eq $target} {
        puts stderr "Skipping item \"$item\" as it's already trashed!"
        set ::exitLevel 1
        return $target
    }

    # Rename $target if appropriate
    while {[file exists $target]} {
        set target [newNameFor $target]
    }

    try {
        file rename -- $item $target
        if {!$onExternalVolume && [iAmRoot]} {
            set trashPermissions ugo+rw
            if {[file isdirectory $target]} {
                append trashPermissions x
            }
            file attributes $target -permissions $trashPermissions
        }
    } on error errMsg {
        puts stderr "Error trashing \"$item\""
        puts stderr $errMsg
        set ::exitLevel 1
        return ""
    }

    return $target
}


proc usage {argv0} {
    puts stderr "Usage: $argv0 <stuff> \[more stuff\]"
    puts stderr "Moves all items to the appropriate trash directory on OSX."
    puts stderr "Only works on OSX."
}

proc main {argc argv argv0} {
    set optDict [dict create \
        verbose [dict create \
            shortOpt v \
            longOpt verbose \
            numberOfArgs 0 \
            required 0 \
            usage "Be verbose" \
        ] \
        stuffToTrash [dict create \
            shortOpt "" \
            longOpt "" \
            numberOfArgs -1 \
            required 1 \
            usage "Files or directories to move to trash" \
        ] \
        force [dict create \
            shortOpt f \
            longOpt force \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        recursive1 [dict create \
            shortOpt r \
            longOpt "recursive" \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        recursive2 [dict create \
            shortOpt R \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        interactive [dict create \
            shortOpt i \
            longOpt "interactive" \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        rmdir [dict create \
            shortOpt d \
            longOpt dir \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        overwrite [dict create \
            shortOpt P \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
        undelete [dict create \
            shortOpt W \
            numberOfArgs 0 \
            required 0 \
            usage "Ignored. Here for BSD rm compatibility" \
        ] \
    ]

    processArgs $argv0 $argv $argc $optDict
    importOptArrays

    checkIfOnOSX

    foreach item $stuffToTrash(passedArgs) {
        set saneItem [file normalize $item]
        set trash [trash $saneItem]
        if {$verbose(present) && $trash ne ""} {
            puts stderr "Trashed \"$saneItem\" as \"$trash\"."
        }

    }

    return
}

main $argc $argv $argv0
exit $::exitLevel

