UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	MLINK := /Applications/MATLAB_R2015a.app/bin/maci64
	MINCLUDE := /Applications/MATLAB_R2015a.app/extern/include
	MLIB := /Applications/MATLAB_R2015a.app/bin/maci64
	SHAREDLIBSUFFIX := dylib
else
	MLINK := /opt/MATLAB/bin/glnxa64
	MINCLUDE := /opt/MATLAB/extern/include
	MLIB := /opt/MATLAB/bin/glnxa64
	SHAREDLIBSUFFIX := so
endif

ifdef DEBUG
	EXT := debug.$(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -ggdb
else
	EXT := $(SHAREDLIBSUFFIX)
endif

LDFLAGS := -L$(TCLLIB) -ltclstub$(TCLVERSION) -L$(MLIB) -lmx -lmex -lmat -lm -lstdc++ -leng
INCLUDES := -I$(MINCLUDE) -I/usr/local/include -I$(TCLINCLUDE) -I$(MYCINCLUDE) -I$(CPPTCLINCLUDE) -I$(BOOSTINCLUDE) 


CPPSTD := -std=gnu++0x
CPPFLAGS := -Os -shared -fPIC -DUSE_TCL_STUBS -Wl,-rpath,$(MLINK) -Wall

ifndef CPPC
	CPPC := g++
endif


matlab: matlab.tclext.cpp
	$(CPPC) $(CPPSTD) $(DEBUGFLAGS) $(CPPFLAGS) $(ADDTL_CPPFLAGS) $(INCLUDES) $(ADDTL_INCLUDES) $< $(CPPTCLINCLUDE)/cpptcl.cc -o matlab.tclext.$(EXT) $(LDFLAGS) $(ADDTL_LDFLAGS)

