#!/usr/bin/env tclsh

package require misc ;# for gentleLassign

namespace eval stefCoroutines {

   # Here we put the procs for coroutineProc
   namespace eval TemporaryProcs {
      variable Counter [expr {wide(-1)}]
   }

   proc gensym {} {
      return ::stefCoroutines::TemporaryProcs::[incr ::stefCoroutines::TemporaryProcs::Counter 1]
   }

   # Convenience proc to make a coroutine command
   # where the real proc is not needed. After the body is executed
   # the real proc is deleted and a break signal is returned (useful in loops)
   # e.g.
   # coroutineProc getFruit {} {
   #    # NB no initial yield!
   #    foreach fruit {bananas apples strawberries grapes} {
   #       yield $fruit
   #    }
   # }
   # while 1 {
   #    puts "[getFruit] are in my basket"
   # }
   #

   proc coroutineProc {name arguments initDict body} {
      set tmpProcName [::stefCoroutines::gensym]
      proc $tmpProcName {arguments initDict} [subst -nocommands {
         dict for {key value} \$initDict {
            set \$key \$value
         }
         ::stefCoroutines::yieldUpdateArgsChecked \$arguments
         $body
         rename $tmpProcName {}
         return -code break "Coroutine $tmpProcName has finished"
      }]
      coroutine $name $tmpProcName $arguments $initDict
      return $name
   }

   proc generatorProc {name arguments body} {
      set corProc [::stefCoroutines::gensym]
      set innerLambdaBody [subst -nocommands -nobackslashes {
            yield
            $body
            return -code break "Coroutine \"$corProc\" has finished!"
         }
      ]

      set innerLambda [list $arguments $innerLambdaBody]
      set generatorBody [subst -nocommands {
         coroutine $corProc apply {$innerLambda} {*}\$args; return $corProc
      }]

      tailcall proc $name args $generatorBody
   }
      
   proc yieldm args {
      tailcall yieldto return -level 0 {*}$args
   }

   proc yieldUpdateArgs {argumentsToSet args} {
      tailcall ::misc::gentleLassign [::stefCoroutines::yieldm $args] {*}$argumentsToSet
   }

   proc yieldUpdateArgsChecked {argumentsToSet args} {
      set provided [::stefCoroutines::yieldm $args]
      set providedLen [llength $provided]
      set argumentsToSetLen [llength $argumentsToSet]
      if {$providedLen != $argumentsToSetLen} {
         throw {lengths don't match} "YieldUpdateArgs should update the arguments \"$argumentsToSet\", which are $argumentsToSetLen, but the provided \"$provided\" are only $providedLen!"
      }
      tailcall ::misc::gentleLassign $provided {*}$argumentsToSet
   }

   proc coroutineLambda {arguments initDict body} {
      set lambdaName [::stefCoroutines::gensym]
      tailcall ::stefCoroutines::coroutineProc $lambdaName $arguments $initDict $body
   }

   proc foreachGenerated args {
      set body [lindex $args end]
      set len [llength $args]
      # count body too --> args must have _odd_ number of elements
      if {$len < 3 || $len % 2 == 0} {
         ::misc::wrongNumArgs foreachGenerated [list variableName_1 generatorCmdPrefix_1 ?variableName_2? ?generatorCmdPrefix_2? ... body]
      }
      set init {}
      set numOfPairs [expr {$len - 1}]
      for {set varInd 0; set genInd 1} {$varInd < $numOfPairs} {incr varInd 2; incr genInd 2} {
         append init "set {[lindex $args $varInd]} \[[lindex $args $genInd]\]\n"
      }
      tailcall while 1 [subst -nocommands -nobackslashes {
         $init
         $body
      }]
   }

   
   # Convenience proc to make a stream proc
   # e.g.
   # staticProc getEven {} {
   #    set c -2
   #    puts "Starting to count the even numbers..."
   # } {
   #   yield [incr $c 2]
   # }
   # returns all even numbers
   proc streamProc {name arguments init body} {
      set modifBody [subst -nocommands -nobackslashes {
         $init
         while 1 {
            $body
         }
      }]
      tailcall ::stefCoroutines::coroutineProc $name $arguments {} $modifBody
   }

   proc streamLambda {arguments init body} {
      set lambdaName [::stefCoroutines::gensym]
      tailcall ::stefCoroutines::streamProc $lambdaName $arguments $init $body
   }

   namespace export {[a-z]*}
}

