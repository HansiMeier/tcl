#!/usr/bin/env tclsh

package require Tcl 8.6-

namespace eval neededPackages {

    variable toSource [list]

    proc mySource {args} {
        set fileName [lindex $args end]
        lappend ::neededPackages::toSource $fileName
        tailcall theRealSource {*}$args
    }

    proc outputFindings args {
        puts stderr "FOUND SOURCES\n------------------------------"
        foreach elem $::neededPackages::toSource {
            puts stderr $elem
        }
    }

    proc runScript script {
        global argc argv argv0
        rename ::source ::theRealSource
        rename ::neededPackages::mySource ::source

        incr argc -1
        set argv0 $script
        set argv [lrange $argv 1 end]

        trace add execution ::exit enter ::neededPackages::outputFindings

        uplevel #0 [list ::theRealSource $script]
    }

    proc fail msg {
        puts stderr $msg
        exit 1
    }

    proc usageAndBye argv0 {
        puts stderr "Usage: $argv0 <tcl script> \[arguments for that script\]\nRuns the script outputting the 'package require'd files"
        exit 2
    }


    proc main {} {
        global argc argv0 argv

        if {$argc == 0} {
            usageAndBye $argv0
        }

        set script [lindex $argv 0]

        if {![file readable $script]} {
            fail "\"$script\" isn't readable!"
        }

        runScript $script
    }
}

::neededPackages::main

