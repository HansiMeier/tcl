#!/usr/bin/env tclsh

####################################################################################################
#                                        OCTAVE TCL INTEROP                                        #
####################################################################################################

# This module enables Tcl to interact with octave
#
# Commands in the namespace 'octave'
# ########################################
#
# - eval octaveCode
# evaluates octave code
# returns the result as a tcl value if possible
#
# - get variableName
# gets the value of an octave variable
# if it is a matrix it is returned as a matrix description list
#
# - put variableName value
# sets an octave variable to the provided value
# if a list is provided, interpret it as a matrix description list
#
# - source file
# Sources an octave script (m file)
#
#
# What is a matrix description list (mdl)?
# ########################################
#
# A mdl is a Tcl list consisting of
# - a list of integers of how many elements there are in a dimension
# - the entries themselves as doubles in row major order
#
# Examples:
# The row vector [1 2 3] corresponds to the mdl:
# {{1 3} 1 2 3}
#
# The column vector [5; 6; 7; 8] to
# {{4 1} 5 6 7 8}
#
# The matrix
# [ 1 2;
#   3 4 ]
# to {{2 2} 1 2 3 4}

package require Tcl 8.6-

# Load the dynamic library providing the extension
namespace eval loadLibraryTmp {

   variable sharedLib {}
   variable name octave.tclext[info sharedlibextension]
   variable searchIn [linsert $::env(TCLLIBPATH) 0 [file dirname [info script]]]

   foreach dir $::loadLibraryTmp::searchIn {
      if {[file exists [file join $dir $::loadLibraryTmp::name]]} {
         set ::loadLibraryTmp::sharedLib [file join $dir $::loadLibraryTmp::name]
         break
      }
   }

   if {[string equal $::loadLibraryTmp::sharedLib {}]} {
      throw {NO SHARED LIB} "The library \"$::loadLibraryTmp::name\" has not been found in \"$::loadLibraryTmp::searchIn\"!"
   }

   uplevel #0 load $::loadLibraryTmp::sharedLib
}
namespace delete loadLibraryTmp

namespace eval octave {
   # source a m file
   proc source file {
      if {! [file readable $file]} {
         throw {src not readable} "File \"$file\", which octave should source, isn't readable!"
      }
      tailcall ::octave::eval source(\"$file\")
   }
   namespace export {[a-z]*}
}

