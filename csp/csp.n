'\"
'\" Generated from file '' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2015 SecurityKISS Ltd <open\&.source@securitykiss\&.com> - MIT License - Feedback and bug reports are welcome
'\"
.TH "csp" n 0\&.1\&.0  "Concurrency"
.\" The -*- nroff -*- definitions below are for supplemental macros used
.\" in Tcl/Tk manual entries.
.\"
.\" .AP type name in/out ?indent?
.\"	Start paragraph describing an argument to a library procedure.
.\"	type is type of argument (int, etc.), in/out is either "in", "out",
.\"	or "in/out" to describe whether procedure reads or modifies arg,
.\"	and indent is equivalent to second arg of .IP (shouldn't ever be
.\"	needed;  use .AS below instead)
.\"
.\" .AS ?type? ?name?
.\"	Give maximum sizes of arguments for setting tab stops.  Type and
.\"	name are examples of largest possible arguments that will be passed
.\"	to .AP later.  If args are omitted, default tab stops are used.
.\"
.\" .BS
.\"	Start box enclosure.  From here until next .BE, everything will be
.\"	enclosed in one large box.
.\"
.\" .BE
.\"	End of box enclosure.
.\"
.\" .CS
.\"	Begin code excerpt.
.\"
.\" .CE
.\"	End code excerpt.
.\"
.\" .VS ?version? ?br?
.\"	Begin vertical sidebar, for use in marking newly-changed parts
.\"	of man pages.  The first argument is ignored and used for recording
.\"	the version when the .VS was added, so that the sidebars can be
.\"	found and removed when they reach a certain age.  If another argument
.\"	is present, then a line break is forced before starting the sidebar.
.\"
.\" .VE
.\"	End of vertical sidebar.
.\"
.\" .DS
.\"	Begin an indented unfilled display.
.\"
.\" .DE
.\"	End of indented unfilled display.
.\"
.\" .SO ?manpage?
.\"	Start of list of standard options for a Tk widget. The manpage
.\"	argument defines where to look up the standard options; if
.\"	omitted, defaults to "options". The options follow on successive
.\"	lines, in three columns separated by tabs.
.\"
.\" .SE
.\"	End of list of standard options for a Tk widget.
.\"
.\" .OP cmdName dbName dbClass
.\"	Start of description of a specific option.  cmdName gives the
.\"	option's name as specified in the class command, dbName gives
.\"	the option's name in the option database, and dbClass gives
.\"	the option's class in the option database.
.\"
.\" .UL arg1 arg2
.\"	Print arg1 underlined, then print arg2 normally.
.\"
.\" .QW arg1 ?arg2?
.\"	Print arg1 in quotes, then arg2 normally (for trailing punctuation).
.\"
.\" .PQ arg1 ?arg2?
.\"	Print an open parenthesis, arg1 in quotes, then arg2 normally
.\"	(for trailing punctuation) and then a closing parenthesis.
.\"
.\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
.\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1 \\fI\\$2\\fP (\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
.\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
.\"	# BS - start boxed text
.\"	# ^y = starting y location
.\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
.\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
.\"	# VS - start vertical sidebar
.\"	# ^Y = starting y location
.\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
.\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
.\"	# Special macro to handle page bottom:  finish off current
.\"	# box/sidebar if in box/sidebar mode, then invoked standard
.\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
.\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
.\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.\"	# SO - start of list of standard options
.de SO
'ie '\\$1'' .ds So \\fBoptions\\fR
'el .ds So \\fB\\$1\\fR
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 5.5c 11c
.ft B
..
.\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\*(So manual entry for details on the standard options.
..
.\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
.\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
.\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.\"	# UL - underline word
.de UL
\\$1\l'|0\(ul'\\$2
..
.\"	# QW - apply quotation marks to word
.de QW
.ie '\\*(lq'"' ``\\$1''\\$2
.\"" fix emacs highlighting
.el \\*(lq\\$1\\*(rq\\$2
..
.\"	# PQ - apply parens and quotation marks to word
.de PQ
.ie '\\*(lq'"' (``\\$1''\\$2)\\$3
.\"" fix emacs highlighting
.el (\\*(lq\\$1\\*(rq\\$2)\\$3
..
.\"	# QR - quoted range
.de QR
.ie '\\*(lq'"' ``\\$1''\\-``\\$2''\\$3
.\"" fix emacs highlighting
.el \\*(lq\\$1\\*(rq\\-\\*(lq\\$2\\*(rq\\$3
..
.\"	# MT - "empty" string
.de MT
.QW ""
..
.BS
.SH NAME
csp \- Golang style concurrency library based on Communicating Sequential Processes
.SH SYNOPSIS
package require \fBTcl  8\&.6\fR
.sp
package require \fBcsp  ?0\&.1\&.0?\fR
.sp
\fB::csp::go\fR \fIprocName\fR ?\fIargs\fR?
.sp
\fB::csp::channel\fR \fIchannelVar\fR ?\fIsize\fR?
.sp
\fBchannelObj\fR \fIclose\fR
.sp
\fBchannelObj\fR \fI<-\fR \fImsg\fR
.sp
\fBchannelObj\fR \fI<-!\fR \fImsg\fR
.sp
\fB::csp::<-\fR \fIchannelObj\fR
.sp
\fB::csp::<-!\fR \fIchannelObj\fR
.sp
\fB::csp::select\fR \fIoperation\fR \fIbody\fR
.sp
\fB::csp::range\fR \fIvarName\fR \fIchannelObj\fR \fIbody\fR
.sp
\fB::csp::range!\fR \fIvarName\fR \fIchannelObj\fR \fIbody\fR
.sp
\fB::csp::timer\fR \fIchannelVar\fR \fIinterval\fR
.sp
\fB::csp::ticker\fR \fIchannelVar\fR \fIinterval\fR \fI?closeafter?\fR
.sp
\fB::csp::->\fR \fIchannelVar\fR
.sp
\fB::csp::->>\fR \fIchannelVar\fR ?\fIsize\fR?
.sp
\fB::csp::forward\fR \fIfromChannel\fR \fItoChannel\fR
.sp
.BE
.SH DESCRIPTION
.PP
The \fBcsp\fR package provides two concurrency primitives namely \fIcoroutines\fR and \fIchannels\fR which allow concurrent programming in the style of \fIGolang\fR [https://en\&.wikipedia\&.org/wiki/Go_(programming_language)]\&.
.PP
The concepts originate in Hoare's \fICommunicating Sequential Processes\fR [https://en\&.wikipedia\&.org/wiki/Communicating_sequential_processes] while the syntax mimics the Golang implementation\&.
.PP
The CSP concurrency model may be visualized as a set of independent processes (coroutines) sending and receiving messages to the named channels\&. The control flow in the coroutines is coordinated at the points of sending and receiving messages i\&.e\&. the coroutine may need to wait while trying to send or receive\&.
Since it must work in a single-threaded interpreter, waiting is non-blocking\&. Instead of blocking a waiting coroutine gives way to other coroutines\&.
.PP
This concurrency model may also be seen as a generalization of Unix \fInamed pipes\fR [https://en\&.wikipedia\&.org/wiki/Named_pipe] where processes and pipes correspond to coroutines and channels\&.
.SH CONCEPTS
.TP
\fBchannel\fR
.RS
.TP
There are two types of channels\&.
.RS
.TP
\fIUnbuffered channels\fR
.sp
The unbuffered channel is a single value container that can be imagined as a rendez-vous venue where the sender must wait for the receiver to collect the message\&.
By default channels are unbuffered\&.
.TP
\fIBuffered channels\fR
.sp
The buffered channel behaves like a FIFO queue\&.
.RE
.RE
.sp
.RS
.TP
Whether receiver need to wait while trying to receive from a channel depends on the channel's internal state:
.RS
.TP
\fIready for receive\fR
The buffered channel is ready for receive when it is not empty\&.
The unbuffered channel is ready for receive if there exists a sender waiting with a message on this channel\&.
.TP
\fIready for send\fR
The buffered channel is ready for send when it is not full\&.
The unbuffered channel is ready for send if there is no other sender already waiting on this channel\&. Note that
.RE
.RE
.sp
Channel is created with:
.sp
\fB::csp::channel\fR \fIchanVar\fR \fI?size?\fR
.sp
Where the optional parameter \fIsize\fR specifies the maximum number of messages that can be stored in the channel\&. When the channel is full the sender trying to send more messages to it must wait until any receiver offloads the channel\&. Waiting means that the sender gives way to other coroutines\&.
.sp
If the size is zero (default) the created channel is unbuffered which means that the sender coroutine always waits for the receiver to collect the message\&.
.sp
Channel may be closed with:
.sp
\fBchannelObj\fR \fIclose\fR
.sp
and is destroyed automatically when all messages are received (the channel is drained)\&.
.sp
.RS
.TP
Channel lifecycle is described by 3 possible states:
.RS
.TP
\fIcreated\fR
Once the channel is created you can send to and receive from the channel\&.
.TP
\fIclosed\fR
When the channel is closed you can still receive from the channel but you cannot send to it\&.
Trying to send to the closed channel throws an error\&.
It is responsibility of the library user to close the unused channels\&.
.TP
\fIdestroyed\fR
The channel does not exist\&.
After receiving all messages from the closed channel, the channel is destroyed\&.
Trying to send to or receive from the destroyed channel throws an error\&.
.RE
.RE
.IP
Note that creating a large number of channels that are properly closed but not emptied may result in a memory leak\&.
.TP
\fBcoroutine\fR
.sp
\fICoroutine\fR is a procedure executing concurrently with other coroutines\&.
\fICoroutine\fR may send messages to and receive messages from \fIchannels\fR\&. Any coroutine may act as a sender or receiver at different times\&. If \fIchannel\fR is not ready a coroutine waits by giving way to other coroutines\&. This makes the coroutine execution multiplexed at the points of sending to or receiving from channels\&.
.sp
\fICoroutine\fR is created with:
.sp
\fB::csp::go\fR \fIprocName\fR \fI?args?\fR
.sp
where \fIprocName\fR is the name of the existing Tcl procedure that will be run as a coroutine\&.
You can create many coroutines from a single Tcl procedure, possibly called with different arguments\&.
.sp
Coroutine is destroyed when its execution ends\&.
.sp
We reuse the term \fIcoroutine\fR known from Tcl (modelled on Lua) coroutines, but they are are not equivalent\&. \fBcsp\fR coroutines are implemented in terms of Tcl coroutines and it's better not to mix \fBcsp\fR and Tcl coroutines in a single program\&.
.PP
.SH COMMANDS
.PP
.TP
\fB::csp::go\fR \fIprocName\fR ?\fIargs\fR?
Create a coroutine by calling \fIprocName\fR with arguments \fIargs\fR\&. Returns internal name of the coroutine\&.
.TP
\fB::csp::channel\fR \fIchannelVar\fR ?\fIsize\fR?
Create a channel object that will be further referred as \fBchannelObj\fR\&. The name of the object is contained in variable \fIchannelVar\fR\&.
.RS
.TP
var \fIchannelVar\fR
Variable channelVar that will be created and will contiain the channel object name\&.
.TP
number \fIsize\fR
Size of the channel buffer understood as the maximum number of messages that can be buffered in the channel\&. If size is zero (default) the channel is unbuffered\&.
.RE
.IP
Returns channel object name\&.
.TP
\fBchannelObj\fR \fIclose\fR
Close the channel \fIchannelObj\fR\&. Returns empty string\&.
.TP
\fBchannelObj\fR \fI<-\fR \fImsg\fR
Send \fImsg\fR to channel \fIchannelObj\fR in a coroutine\&. Returns empty string\&.
.TP
\fBchannelObj\fR \fI<-!\fR \fImsg\fR
Send \fImsg\fR to channel \fIchannelObj\fR in a script (in the Tcl program main control flow)\&. It is implemented using vwait and has many limitations\&. Use with care and only in simple scenarios\&. Returns empty string\&.
.TP
\fB::csp::<-\fR \fIchannelObj\fR
Receive from channel \fIchannelObj\fR in a coroutine\&. Returns the message received from the channel\&.
.TP
\fB::csp::<-!\fR \fIchannelObj\fR
Receive from channel \fIchannelObj\fR in a script (in the Tcl program main control flow)\&. Returns the message received from the channel\&.
.TP
\fB::csp::select\fR \fIoperation\fR \fIbody\fR
Evaluate set of channels to find which channels are ready and run corresponding block of code\&. Returns the result of evaluation of the block of code\&.
.sp
.RS
.TP
list \fIoperation\fR
Operation takes one of 3 forms:
.sp
\fB<-\fR \fIchannelObj\fR
.sp
for evaluating whether the \fIchannelObj\fR is ready for receive, or
.sp
\fIchannelObj\fR \fB<-\fR
.sp
for evaluating whether the \fIchannelObj\fR is ready for send, or
.sp
default
.sp
for evaluating default case if no channel is ready\&.
.TP
block \fIbody\fR
Block of code to be evaluated\&.
.RE
.sp
The select command provides a way to handle multiple channels\&. It is a switch like statement where channels are evaluated for readiness\&. The select command makes the coroutine wait until at least one channel is ready\&. If multiple channels can proceed, \fBselect\fR chooses pseudo-randomly\&. A default clause, if present, executes immediately if no channel is ready\&.
.TP
\fB::csp::range\fR \fIvarName\fR \fIchannelObj\fR \fIbody\fR
Receive from channel until closed in a coroutine\&.
.sp
This is a \fBforeach\fR like construct that iterates by receiving messages from channel one by one until channel is closed\&. If channel is not ready for receive, \fBrange\fR waits\&.
.TP
\fB::csp::range!\fR \fIvarName\fR \fIchannelObj\fR \fIbody\fR
Receive from channel until closed in the main control flow\&.
.sp
A version of \fBrange\fR command that can be used outside of a coroutine\&. It is implemented using vwait and has many limitations\&. Use with care and only in simple scenarios\&.
.TP
\fB::csp::timer\fR \fIchannelVar\fR \fIinterval\fR
Create a receive-only channel with scheduled message in \fIinterval\fR milliseconds\&. Trying to receive from the channel will cause the coroutine to wait \fIinterval\fR milliseconds since creation\&. Eventually the received message is a Unix epoch time in microseconds\&. After receiving the message the channel is closed and destroyed\&.
.sp
Returns the created channel\&.
.TP
\fB::csp::ticker\fR \fIchannelVar\fR \fIinterval\fR \fI?closeafter?\fR
Create a receive-only channel with scheduled messages every \fIinterval\fR milliseconds\&.
.sp
Returns the created channel\&.
The optional \fIcloseafter\fR argument determines when the channel is closed\&. It may take one of the 2 forms:
.RS
.IP \(bu
\fIintegerNumber\fR that specifies the number of milliseconds after which the channel will be closed
.IP \(bu
\fI#integerNumber\fR that specifies number of messages after which the channel will be closed
.RE
.IP
If \fIcloseafter\fR argument is not provided, the \fBticker\fR channel emits messages endlessly\&.
.TP
\fB::csp::->\fR \fIchannelVar\fR
Creates a channel and returns a new coroutine that may be called with a single argument\&. The coroutine is meant for integration with callback-driven code and to be used in place of one-time callback\&. The channel is placed in \fIchannelVar\fR and will be destroyed after receiving a single message\&. The single argument passed to the callback will be available to receive from the created channel\&.
.sp
Note that there is a limitation in replacing callbacks with -> command: only a single- or zero- argument callbacks can be replaced\&. In case of zero-argument callbacks an empty string is sent to the channel\&.
.TP
\fB::csp::->>\fR \fIchannelVar\fR ?\fIsize\fR?
Creates a buffered channel of size \fIsize\fR and returns a new coroutine that may be used in place of a callback\&. The coroutine may be called many times and the callback arguments are internally sent to the created channel\&.
.sp
Note that there is a limitation in replacing callbacks with -> command: only a single- or zero- argument callbacks can be replaced\&. In case of zero-argument callbacks an empty string is sent to the channel\&.
.TP
\fB::csp::forward\fR \fIfromChannel\fR \fItoChannel\fR
Receive messages from \fIfromChannel\fR and send them to \fItoChannel\fR\&.
.sp
.PP
.SH EXAMPLES
.SS "EXAMPLE 1"
Simple message passing over an unbuffered channel
.CS


    package require csp
    namespace import csp::*

    proc sender1 {ch} {
        foreach i {1 2 3 4} {
            puts "Sending $i"
            $ch <- $i
        }
        puts "Closing channel"
        $ch close
    }

    proc receiver1 {ch} {
        while 1 {
            puts "Receiving [<- $ch]"
        }
    }

    # create unbuffered (rendez-vous) channel
    channel ch
    go sender1 $ch
    go receiver1 $ch

    vwait forever

.CE
Output:
.CS


Sending 1
Receiving 1
Sending 2
Receiving 2
Sending 3
Receiving 3
Sending 4
Receiving 4
Closing channel

.CE
The communication between the coroutines is coordinated because the channel is unbuffered\&.
The sender waits for the receiver\&.
.SS "EXAMPLE 2"
Simple message passing over a buffered channel
.CS


    package require csp
    namespace import csp::*

    proc sender1 {ch} {
        foreach i {1 2 3 4} {
            puts "Sending $i"
            $ch <- $i
        }
        puts "Closing channel"
        $ch close
    }

    proc receiver1 {ch} {
        while 1 {
            puts "Receiving [<- $ch]"
        }
    }

    # create buffered channel of size 2
    channel ch 2
    go sender1 $ch
    go receiver1 $ch

    vwait forever

.CE
Output:
.CS


Sending 1
Sending 2
Sending 3
Receiving 1
Receiving 2
Sending 4
Closing channel
Receiving 3
Receiving 4
Error: Cannot receive from a drained (empty and closed) channel ::csp::Channel#1

.CE
.PP
Since the channel is buffered of size 2, the sender waits only on the third attempt\&.
.PP
Note that the channel was closed but we still receive messages\&. Only after the channel was emptied, trying to receive from the channel throws an error\&.
.SS "EXAMPLE 3"
.PP
Using \fBrange\fR for receiving from the channel until closed\&.
.PP
We can prevent throwing the error in the previous example by using the \fBrange\fR command instead of iterating blindly with \fBwhile\fR\&.
Also if the channel is buffered we can send all messages first and iterate to receive using \fBrange\fR in a single coroutine\&.
.CS


    package require csp
    namespace import csp::*

    proc senderreceiver {ch} {
        foreach i {1 2 3 4} {
            puts "Sending $i"
            $ch <- $i
        }
        puts "Closing channel"
        $ch close
        range msg $ch {
            puts "Message $msg"
        }
        puts "Received all"
    }

    channel ch 10
    go senderreceiver $ch

    vwait forever

.CE
Output:
.CS


Sending 1
Sending 2
Sending 3
Sending 4
Closing channel
Message 1
Message 2
Message 3
Message 4
Received all

.CE
.SS "EXAMPLE 4"
.PP
Channels can be used to coordinate future events\&. We use \fBafter\fR to create coroutine that will send to the channel\&.
.PP
Instead of using direct callback which cannot keep local state we consume events in \fBadder\fR which can keep sum in local variable\&.
.PP
.CS


    package require csp
    namespace import csp::*

    proc adder {ch} {
        set sum 0
        while 1 {
            set number [<- $ch]
            incr sum $number
            puts "adder received $number\&. The sum is $sum"
        }
    }

    proc trigger {ch number} {
        $ch <- $number
    }

    channel ch
    go adder $ch
    after 1000 go trigger $ch 1
    after 3000 go trigger $ch 3
    after 5000 go trigger $ch 5
    puts "Enter event loop"

    vwait forever

.CE
Output:
.CS


Enter event loop
adder received 1\&. The sum is 1
adder received 3\&. The sum is 4
adder received 5\&. The sum is 9

.CE
.SS "EXAMPLE 5"
.PP
Use \fBtimer\fR to create a channel supplying scheduled messages in the future\&.
.PP
.CS


    package require csp
    namespace import csp::*

    proc future {ch} {
        try {
            puts "future happened at  [<- $ch]"
            puts "try to receive again:"
            puts "[<- $ch]"
        } on error {out err} {
            puts "error: $out"
        }
    }

    timer ch 2000
    go future $ch
    puts "Enter event loop at [clock microseconds]"

    vwait forever

.CE
Output:
.CS


Enter event loop at 1434472163190638
future happened at  1434472165189759
try to receive again:
error: Cannot receive from a drained (empty and closed) channel ::csp::Channel#1

.CE
.PP
Instead of scheduling events with \fBafter\fR we can use \fBtimer\fR to create a special receive only channel\&. There will be only one message send to this channel after the specified time so we can pass this channel to another coroutine that will wait for that message\&. The message from the timer channel represents unix epoch time in microseconds\&. The timer channel will be automatically destroyed after first receive so trying to receive again will throw an error\&.
.SS "EXAMPLE 6"
.PP
Using \fBticker\fR we can create receive only channel from which we can consume timestamp messages at regular intervals\&.
.CS


    package require csp
    namespace import csp::*

    proc future {ch} {
        set count 0
        while 1 {
            incr count
            puts "future $count received at [<- $ch]"
        }
    }

    ticker ch 1000
    go future $ch
    puts "Enter event loop at  [clock microseconds]"

    vwait forever

.CE
Output:
.CS


Enter event loop at  1434472822879684
future 1 received at 1434472823879110
future 2 received at 1434472824882163
future 3 received at 1434472825884246
\&.\&.\&.

.CE
.SS "EXAMPLE 7"
.PP
\fBticker\fR command returns the created channel so we can use it in place in combination with \fBrange\fR to further simplify the example
.CS


    package require csp
    namespace import csp::*

    proc counter {} {
        range t [ticker ch 1000] {
            puts "received $t"
        }
    }

    go counter

    vwait forever

.CE
Output:
.CS


received 1434474325947677
received 1434474326950822
received 1434474327952904
\&.\&.\&.

.CE
.SS "EXAMPLE 8"
.PP
Another example of using \fBticker\fR to implement the canonical countdown counter from \fITcl wiki\fR [http://wiki\&.tcl\&.tk/946]\&.
.CS


    package require Tk
    package require csp
    namespace import csp::*

    proc countdown {varName} {
        upvar $varName var
        range _ [ticker t 1000 #10] {
            incr var -1
        }
    }

    set count 10
    label \&.counter -font {Helvetica 72} -width 3 -textvariable count
    grid \&.counter -padx 100 -pady 100
    go countdown count

.CE
.SS "EXAMPLE 9"
.PP
Closing the channel by another scheduled event breaks the \fBrange\fR loop
.CS


    package require csp
    namespace import csp::*

    proc counter {ch} {
        range t $ch {
            puts "received $t"
        }
        puts "counter exit"
    }

    ticker ch 1000
    go counter $ch
    after 4500 $ch close
    puts "Enter event loop at [clock microseconds]"

    vwait forever

.CE
Output:
.CS


Enter event loop at 1434474384645704
received 1434474385644900
received 1434474386648105
received 1434474387650088
received 1434474388652345
counter exit

.CE
.SS "EXAMPLE 10"
.PP
Redirect callback call argument to a channel using \fB->\fR command\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc main {} {
        http::geturl http://securitykiss\&.com/rest/now -command [-> ch]
        puts "fetched: [http::data [<- $ch]]"
    }

    go main

    vwait forever

.CE
Output:
.CS


fetched: 1434474568

.CE
.PP
\fBcsp\fR package makes it easy to integrate channels and coroutines with existing event driven code\&.
Using the \fB->\fR utility command we can make channels work with callback driven commands and at the same time avoid callback hell\&.
.PP
\fB->\fR \fIch\fR creates a channel ch and returns a new coroutine that may be used in place of a callback\&.
The channel will be destroyed after receiving a single value\&.
The single argument passed to the callback will be available to receive from the created channel\&.
.PP
Such code organization promotes local reasoning - it helps writing linear code with local state kept in proc variables\&. Otherwise the callback would require keeping state in global variables\&.
.PP
Note that there is a limitation in replacing callbacks with \fB->\fR command: only a single- or zero- argument callbacks can be replaced\&.
In case of zero-argument callbacks an empty string is sent to the channel\&.
.PP
Note that there is no symmetry in <- <-! -> ->> commands\&. Every one of them has a different purpose\&.
.SS "EXAMPLE 11"
.PP
Use \fBselect\fR command to choose ready channels\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc main {} {
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch1]
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch2]
        select {
            <- $ch1 {
                puts "from first request: [http::data [<- $ch1]]"
            }
            <- $ch2 {
                puts "from second request: [http::data [<- $ch2]]"
            }
        }
    }

    go main

    vwait forever

.CE
Output:
.CS


from first request: 1434483100

.CE
.PP
Previous example with callback channels does not extend to making parallel http requests because one waiting channel would prevent receiving from the other\&.
The \fBselect\fR command chooses which of a set of possible send or receive operations will proceed\&. In this example \fBselect\fR command examines two callback channels and depending on which one is ready for receive first, it evaluates corresponding body block\&.
.SS "EXAMPLE 12"
.PP
Combine \fBtimer\fR created channel with \fBselect\fR to enforce timeouts\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc main {} {
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch1]
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch2]
        timer t1 400
        select {
            <- $ch1 {
                puts "from first request: [http::data [<- $ch1]]"
            }
            <- $ch2 {
                puts "from second request: [http::data [<- $ch2]]"
            }
            <- $t1 {
                puts "requests timed out at [<- $t1]"
            }
        }
    }

    go main

    vwait forever

.CE
Output:
.CS


requests timed out at 1434484003634953

.CE
.PP
Since \fBselect\fR chooses from the set of channels whichever is ready first, by adding the \fBtimer\fR created channel to select from, we can implement timeout as in the example above\&.
.SS "EXAMPLE 13"
.PP
Use \fBselect\fR with the default clause\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc DisplayResult {ch1 ch2} {
        set result [select {
            <- $ch1 {
                http::data [<- $ch1]
            }
            <- $ch2 {
                http::data [<- $ch2]
            }
            default {
                subst "no response was ready"
            }
        }]
        puts "DisplayResult: $result"
    }

    proc main {} {
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch1]
        http::geturl http://securitykiss\&.com/rest/slow/now -command [-> ch2]
        after 400 go DisplayResult $ch1 $ch2
    }

    go main

    vwait forever

.CE
Output:
.CS


DisplayResult: no response was ready

.CE
.PP
\fBselect\fR command is potentially waiting if no channel is ready\&. Sometimes we need to proceed no matter what so \fBselect\fR makes it possible to return without waiting if the \fBdefault\fR clause is provided\&. This example also shows that \fBselect\fR has a return value\&. In this case the result returned by \fBselect\fR is either HTTP response or the value specified in the default block if no channel is ready\&.
.SS "EXAMPLE 14"
.PP
Funnel multiple channels into a single channel using \fBforward\fR command\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc main {} {
        set urls {
            http://securitykiss\&.com
            http://meetup\&.com
            http://reddit\&.com
            http://google\&.com
            http://twitter\&.com
            http://bitcoin\&.org
        }
        channel f
        foreach url $urls {
            http::geturl $url -method HEAD -command [-> ch]
            forward $ch $f
        }
        after 200 $f close
        range token $f {
            upvar #0 $token state
            puts "$state(http)\\t$state(url)"
        }
        puts "main exit"
    }

    go main

    vwait forever

.CE
Output:
.CS


HTTP/1\&.1 302 Found  http://google\&.com/
HTTP/1\&.1 301 Moved Permanently  http://reddit\&.com/
HTTP/1\&.1 301 Moved Permanently  http://securitykiss\&.com/
main exit

.CE
.PP
When we want to listen on many channels, especially when they are dynamically created for example per URL as in the above example, \fBselect\fR command becomes awkward because it requires specifying logic for every channel\&.
.PP
In the example above we spawn a HTTP request for every URL and forward messages from individual "callback channels" into the single "funnel channel" \fIf\fR\&. In this way the responses are available in a single channel so we can apply common logic to the results\&. We also set the timeout for the requests by closing the "funnel channel" after some time\&. Responses that don't make it within a specified timeout are ignored\&.
.SS "EXAMPLE 15"
.PP
Redirect callback multi call argument to a long-lived channel using \fB->>\fR command\&.
.CS


    package require Tk
    package require csp
    namespace import csp::*

    proc main {} {
        set number 5
        frame \&.f
        button \&.f\&.left -text <<< -command [->> chleft]
        label \&.f\&.lbl -font {Helvetica 24} -text $number
        button \&.f\&.right -text >>> -command [->> chright]
        grid \&.f\&.left \&.f\&.lbl \&.f\&.right
        grid \&.f
        while 1 {
            select {
                <- $chleft {
                    <- $chleft
                    incr number -1
                }
                <- $chright {
                    <- $chright
                    incr number
                }
            }
            \&.f\&.lbl configure -text $number
        }
    }

    go main

.CE
.PP
In previous examples the \fB->\fR command created short-lived disposable callback channels that could be received from only once\&.
Often an existing command require a callback that will be called many times over long period of time\&. In such case \fB->>\fR comes to play\&.
It returns a coroutine that may be called many times in place of the callback\&. Callback argument is passed to the newly created buffered channel that can be later received from to consume the messages (callback arguments)\&.
.PP
In this example similar functionality could be achieved in a simpler way using \fI-textvariable\fR on \fBlabel\fR but it would require a global variable instead of local \fInumber\fR\&.
.PP
The same limitations regarding callback arguments arity apply as for the \fB->\fR command\&.
.PP
Note that there is no symmetry in <- <-! -> ->> commands\&. Every one of them has a different purpose\&.
.SS "EXAMPLE 16"
.PP
Channel operations like \fB<-\fR and \fBrange\fR can be used only in coroutines\&. Using coroutines for channel driven coordination is the recommended way of using \fBcsp\fR package\&.
.PP
It may happen that we need to use channels outside of coroutines\&. It is possible with corresponding \fB<-!\fR and \fBrange!\fR commands but there are caveats\&.
The "bang" terminated commands are implemented using vwait nested calls and have many limitations\&. Thus they should be used with extra care and only in simple scenarios\&. Especially it is not guaranteed that they will work correctly if used inside callbacks\&.
.PP
In this example we show a simple scenario where receiving from the channel in the main script control flow makes sense as a way to synchronize coroutine termination\&.
.CS


    package require http
    package require csp
    namespace import csp::*

    proc worker {ch_quit} {
        http::geturl http://securitykiss\&.com/rest/now -command [-> ch]
        puts "fetched: [http::data [<- $ch]]"
        $ch_quit <- 1
    }

    # termination coordination channel
    channel ch_quit

    go worker $ch_quit

    <-! $ch_quit

.CE
Output:
.CS


fetched: 1434556219

.CE
Without the last line the script would exit immediately without giving the coroutine a chance to fetch the url\&.
.SS "EXAMPLE 17"
.PP
Following the "bang" terminated command trail, this example shows how \fBrange!\fR command may further simplify the previous countdown counter example\&.
.CS


    package require Tk
    package require csp
    namespace import csp::*

    set count 5
    label \&.counter -font {Helvetica 72} -width 3 -textvariable count
    grid \&.counter -padx 100 -pady 100
    range! _ [ticker t 1000 #$count] {
        incr count -1
    }

.CE
.SS "EXAMPLE 18"
.PP
A more complex example using the already discussed constructs\&.
.CS


    # A simple web crawler/scraper demonstrating the csp style programming in Tcl
    # In this example we have 2 coroutines: a crawler and a parser communicating over 2 channels\&.
    # The crawler receives the url to process from the urls channel and spawns a http request
    # Immediately sends the pair: (url, callback channel from http request)
    # into a pending requests channel for further processing by the parser\&.
    # The parser receives the http token from the received callback channel
    # and fetches the page content from the url in order to extract more urls\&.
    # The new urls are sent to the urls channel where the crawler takes over again\&.

    package require http
    package require csp
    namespace import csp::*

    # The crawler coroutine is initialized with 3 channels:
    # urls - channel with urls waiting to process
    # requests - channel with pending http requests
    # quit - synchronization channel to communicate end of coroutine
    proc crawler {urls requests quit} {
        # list of visited urls
        set visited {}
        range url $urls {
            if {$url ni $visited} {
                http::geturl $url -command [-> req]
                lappend visited $url
                # note we are passing channel object over a channel
                $requests <- [list $url $req]
            }
        }
        $quit <- 1
    }


    # The parser coroutine is initialized with 3 channels:
    # urls - channel with urls waiting to process
    # requests - channel with pending http requests
    # quit - synchronization channel to communicate end of coroutine
    proc parser {urls requests quit} {
        set count 0
        range msg $requests {
            lassign $msg url req
            timer timeout 5000
            select {
                <- $req {
                    set token [<- $req]
                    set data [http::data $token]
                    puts "Fetched URL $url with content size [string length $data] bytes"
                    foreach {_ href} [regexp -nocase -all -inline {href="(\&.*?)"} $data] {
                        if {![string match http:* $href] && ![string match mailto:* $href]} {
                            # catch error if channel has been closed
                            $urls <- [create_url $url $href]
                        }
                    }
                }
                <- $timeout {
                    <- $timeout
                    puts "Request to $url timed out"
                }
            }
            # we stop after fetching 10 urls
            if {[incr count] >= 10} {
                $urls close
                $requests close
            }
        }
        $quit <- 1
    }

    # utility function to construct urls
    proc create_url {current href} {
        regexp {(http://[^/]*)(\&.*)} $current _ base path
        if {[string match /* $href]} {
            return $base$href
        } else {
            return $current$href
        }
    }


    # channel containing urls to process
    # this channel must have rather large buffer so that the urls to crawl can queue
    channel urls 10000
    # channel containing (url req) pairs representing pending http requests
    # size of this channel determines parallelism i\&.e\&. the maximum number of pending requests at the same time
    channel requests 3
    # coordination channels that make the main program wait until coroutines end
    channel crawler_quit
    channel parser_quit
    go crawler $urls $requests $crawler_quit
    go parser $urls $requests $parser_quit

    # send the seed url to initiate crawling
    $urls <-! "http://www\&.tcl\&.tk/man/tcl8\&.6/"

    # Gracefully exit - wait for coroutines to complete
    <-! $crawler_quit
    <-! $parser_quit

.CE
In particular it is worth noting:
.IP \(bu
it is possible to pass a channel object over another channel
.IP \(bu
use of \fIquit\fR synchronization channel to communicate end of coroutine
.IP \(bu
closing channels as a way to terminate \fBrange\fR iteration
.PP
.SH KEYWORDS
actors, callback, channel, concurrency, csp, golang
.SH CATEGORY
Concurrency
.SH COPYRIGHT
.nf
Copyright (c) 2015 SecurityKISS Ltd <open\&.source@securitykiss\&.com> - MIT License - Feedback and bug reports are welcome

.fi