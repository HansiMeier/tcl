#!/usr/bin/env tclsh

package require doctools

proc slurp {path} {
    set fd [open $path r]
    fconfigure $fd -encoding utf-8
    set data [read $fd]
    close $fd
    return $data
}

proc spit {path content} {
    set fd [open $path w]
    puts -nonewline $fd $content
    close $fd
}

doctools::new mydoc -format html
doctools::new myman -format nroff
set path ./csp.tcldoc
set path [file normalize $path]
set desthtml [file join [file dirname $path] [file rootname [file tail $path]].html]
set destman [file join [file dirname $path] [file rootname [file tail $path]].n]
set slurped [slurp $path]
spit $desthtml [mydoc format $slurped]
spit $destman [myman format $slurped]

