#!/usr/bin/env tclsh

set thisScript [info script]
if {[file type $thisScript] eq "link"} {
    set thisScript [file readlink $thisScript]
}

set thisScriptDir [file normalize [file dirname $thisScript]]
lappend ::auto_path $thisScriptDir
::tcl::tm::path add $thisScriptDir


package require inifile
package require dateValidation
package require Tcl 8.6-
package require osxGlob
package require tee
package require fileutil
package require lispy
namespace import ::tcl::mathop::*
namespace import ::tcl::mathfunc::*

tee::tee create teeToFile [file join [::fileutil::tempdir] manga.tcl.stderr]
chan push stderr teeToFile
proc fatalError {txt} {
    puts stderr $txt
    exit 1
}

set debugOn 0

proc debugPuts args {
    if {$::debugOn} {
        puts {*}$args
    }
}

proc getStatusFile {} {
    global argv \
        env \
        argc

    set statusFile ""
    set candidate [lindex $argv 0]
    if {[string match -nocase *.status $candidate]} {
        set argv [lreplace $argv 0 0[set argv {}]]
        incr argc -1
        set statusFile $candidate
    }

    if {$statusFile eq "" && [info exists env(CURRENT_STATUSFILE)]} {
        set statusFile $env(CURRENT_STATUSFILE)
    }

    if {$statusFile eq ""} {
        fatalError "Could not find status file, neither in \$CURRENT_STATUSFILE nor on the commandline!"
    }

    set statusFile [file normalize $statusFile]

    if {[file exists $statusFile] && ! ([file readable $statusFile] && [file writable $statusFile] && [file isfile $statusFile])} {
        fatalError "\"$statusFile\" is not a readable and writable file!"
    }

    return $statusFile
}

proc readFrom {statusFile chapterVar pageVar dirVar formatVar} {
    ::ini::commentchar {#}
    if {[catch {set handle [::ini::open $statusFile r]}]} {
        fatalError "Could not open \"$statusFile\" for reading!"
    }
    if {![::ini::exists $handle status]} {
        fatalError "No section \"status\" found in status file \"$statusFile\""
    }
    upvar 1 $dirVar dir $chapterVar chapter $pageVar page $formatVar format
    foreach var [list dir chapter page format] {
        try {
            if {$var ne "none"} {
                set $var [::ini::value $handle status $var]
            }
        } on error setErr {
            fatalError "Error trying to set property \"$var\" of status file \"$statusFile\": $setErr"
        }
    }

    if {![isURL $dir] && [string equal [file pathtype $dir] relative]} {
        set dirnameOfStatusFile [file dirname [file normalize $statusFile]]
        set dir [file normalize [file join $dirnameOfStatusFile $dir]]
    }
    ::ini::close $handle
    return 1
}

proc displayHelp argv0 {
    set basename [file tail $argv0]
    foreach line [list \
        "$basename" \
        "Writes info about something into a status file or read from a given status file or from \$CURRENT_STATUSFILE" \
        "To write: $basename \[status file\] \[chapter\] \[page\] \[dir\] \[format\]" \
        "To read: $basename \[status file\]" \
        "If you write a status file for the first time, be sure to set unneeded properties (like \"dir\" for a webcomic) to \"none\"" \
        ] {
            puts stderr "$line"
    }
    return
}

proc isURL string {
    return [expr {[string first :// $string]!=-1}]
}

proc getMatching {globPatternVar dir format chapter page} {
    if {$dir eq "none"} {
        return
    }

    upvar 1 $globPatternVar globPattern

    set globPattern [format [subst $format] $chapter $page]
    
    set dir [subst $dir]
    if {[isURL $dir]} {
        return $dir/$globPattern
    }

    set matching [glob -nocomplain -directory $dir $globPattern]
    debugPuts "format = \"$format\""
    debugPuts "matching = \"$matching\""
    debugPuts "globPattern = \"$globPattern\""
    return $matching
}


proc openMatching {dir format chapter page} {
    if {$format eq "none"} {
        return
    }


    for {set i 0} {$i < 5} {incr i} {
        set filesToOpen [getMatching globPattern $dir $format $chapter $page]
        if {[llength $filesToOpen] != 0} {
            break
        }
        incr page
    }

    switch [llength $filesToOpen] {
        0 {
            fatalError "No match for pattern \"$format\" (instantiated \"$globPattern\") in directory \"$dir\"!"
        }
        1 {
            set fileToOpen [lindex $filesToOpen 0]
            openWith $fileToOpen
        }
        default {
            fatalError "Multiple matches for pattern \"$format\" (instantiated \"$globPattern\") in directory \"$dir\"!:\n\t[join $filesToOpen \n\t]"
        }
    }

    return
}


proc countLeft {dir format chapter page} {
    set count 0
    if {[isURL $dir]} {
        return ""
    }

    debugPuts "countLeft format = \"$format\""
    set lookThroughLeft [list \
        {leftVar} {
            foreach var {dir format chapter page} {
                set $var [uplevel 1 [list set $var]]
            }
            upvar 0 $leftVar left
            set count 0
            if {$left in {none ""}} {
                return 0
            }
            incr left 1
            while {[llength [getMatching _ $dir $format $chapter $page]] == 1} {
                incr left 1
                incr count 1
            }

            return $count
        } \
        [namespace current] \
    ]


    set leftString ""
    if {$page ne "none"} {
        append leftString "[apply $lookThroughLeft page] more pages left in this chapter"
    }

    if {$page ne "none" && $chapter ne "none"} {
        append leftString " and "
    }

    if {$chapter ne "none"} {
        append leftString "[apply $lookThroughLeft chapter] more chapters left"
    }

    return $leftString
}


proc assertNatural args {
    foreach var $args {
        upvar 1 $var local
        if {! ([string is entier $local] && $local > 0)} {
            fatalError "[string totitle $var] must be a natural number, but is \"$local\" instead!"
        }
    }
}

proc writeTo {statusFile {chapter ""} {page ""} {dir ""} {format ""}} {

    ::ini::commentchar {#}
    if {[file exists $statusFile] && [catch {set handle [::ini::open $statusFile r+]}]} {
        fatalError "Could not open \"$statusFile\" for writing!"
    } elseif {[catch {set handle [::ini::open $statusFile w+]}]} {
        fatalError "Could not touch \"$statusFile\"!"
    }

    set vars [list chapter page dir format]

    foreach var $vars {
        set val [set $var]

        if {$val eq ""} {
            set val none
        }
        ::ini::set $handle status $var $val
    }
    # Clear all comments to prevent from the shebang comment writing it as an ini comment
    # Because an ini comment contains a space between # and ! which OSX doesn't like
    ::ini::comment $handle status {} {}
    ::ini::commit $handle
    ::ini::close $handle

    writeShebang $statusFile
    file attribute $statusFile -permissions +x
    return
}

proc openWith file {
    set openProg {}
    foreach prog [list gnome-open kde-open xdg-open open start] {
        set openProg [auto_execok $prog]
        if {$openProg ne ""} {
            break
        }
    }
    if {$openProg eq ""} {
        throw {no openprog} "No opening program found!"
    } elseif {[string match -nocase *start* $openProg]} {
        lappend $openProg {""}
    }
    exec {*}$openProg $file &
}

proc augment {number {increment 1}} {
    if {![::dateValidation::readISODate $number year month day sep1 sep2]} {
        tailcall + $number $increment
    }

    set timezone :America/Montreal
    set currentDate [clock scan $year$month$day -format %Y%m%d -timezone $timezone]
    tailcall clock format \
        [clock add \
        $currentDate \
        $increment days \
        -timezone $timezone] \
        -format %Y${sep1}%m${sep2}%d \
}

proc findNextChapterAndPage {dir format chapter page {increment 1}} {
    if {$page ne "none"} {
        set originalPage $page
        set originalChapter $chapter

        for {set i 0} {$i < 5} {incr i} {
            set page [augment $page $increment]
            set matching [getMatching _ $dir $format $chapter $page]
            if {[llength $matching] > 0} {
                return [list $chapter $page]
            }
        }

        set matching [getMatching _ $dir $format $chapter $page]
        if {[llength $matching] > 0} {
            return [list $chapter $page]
        }
        # try looking for next chapter
        set page 0
        set chapter [augment $chapter $increment]
        for {set i 0} {$i < 5} {incr i} {
            set matching [getMatching _ $dir $format $chapter $page]
            if {[llength $matching] > 0} {
                return [list $chapter $page]
            }
            incr page
        }
        set page $originalPage
        set chapter $originalChapter
    } 
    set chapter [augment $chapter $increment]
    return [list $chapter $page]
}

proc writeShebang {statusFile} {
    ::fileutil::insertIntoFile $statusFile 0 "#!/usr/bin/env manga\n"
}


proc main {} {
    global argv0 argv argc
    set maybeOption [lindex $argv 0]
    switch -glob -- $maybeOption {
        -h -
        --help {
            displayHelp $argv0
            exit 1
        }

        -* {
            if {![string is entier $maybeOption]} {
                puts stderr "Option \"$maybeOption\" unknown!"
                exit 2
            }
        }
    }

    set statusFile [getStatusFile] ;# modifies argc, argv
    set statusFileDir [file normalize [file dirname $statusFile]]

    set noArgument [expr {$argc == 0}]

    set reopening [expr {$argc >= 1 && [string first [lindex $argv 0] reopen] == 0}]

    if {$noArgument || $reopening} {
        puts stdout "Reading from $statusFile"
        readFrom $statusFile chapter page dir format
        if {![isURL $dir] && $dir ne "none"} {
            set dir [file join $statusFileDir $dir]
        }

        puts stdout "dir = $dir"
        puts stdout "chapter = $chapter"
        debugPuts "format = \"$format\""

        if {$reopening} {
            set lastChapterAndPage [findNextChapterAndPage $dir $format $chapter $page -1]
            lassign $lastChapterAndPage chapter page
            puts "Reopening last"
        } else {
            puts [countLeft $dir $format $chapter $page]
        }

        openMatching $dir $format $chapter $page
        if {$page ne "none"} {
            puts stdout "page = $page"
        }

        if {!$reopening} {
            set nextChapterAndPage [findNextChapterAndPage $dir $format $chapter $page]
            puts "Next chapter and page = $nextChapterAndPage"
            lassign $nextChapterAndPage chapter page
            writeTo $statusFile $chapter $page $dir $format
        }
    } else {
        set vars [list chapter page dir format]
        if {[file exists $statusFile]} {
            readFrom $statusFile {*}$vars
        }

        # handle numeric chapter and page
        # accept relative changes (with sign prefix)
        set argIndex 0
        foreach var [list chapter page] {
            if {$argIndex >= $argc} {
                break
            }
            set arg [lindex $argv $argIndex]
            if {[string index $arg 0] in {- +} || $arg == 0} {
                # relative change
                incr $var $arg
            } else {
                # absolute change
                set $var $arg
            }
            incr argIndex
        }

        # handle nonnumeric dir and format
        set argIndex 2
        foreach var [list dir format] {
            if {$argIndex >= $argc} {
                break
            }
            set arg [lindex $argv $argIndex]
            set $var $arg
            incr argIndex
        }

        writeTo $statusFile $chapter $page $dir $format
        foreach var $vars {
            set val [set $var]
            if {$val ne ""} {
                puts stdout "${var} = ${val}"
            }
        }
    }
    puts stdout "status file = $statusFile"

    return
}

main
