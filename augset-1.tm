#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval augset {
   proc £ args {
      set namespaceOfCaller [uplevel 1 namespace current]
      set arguments [lindex $args 0]
      set body [lrange $args 1 end]
      if {[llength $body] == 1} {
         tailcall list apply [list $arguments {*}$body $namespaceOfCaller]
      } else {
         tailcall list apply [list $arguments $body $namespaceOfCaller]
      }
   }
   proc augset {var cmdPrefix} {
      tailcall set $var [list \[ {*}$cmdPrefix $var \]]
   }
   foreach operator {+ - * / %} name {add sub mul div mod}  {
      proc ${name} {var operand} [subst -nocommands {
         upvar 1 \$var toAugment
         set toAugment [expr {\$toAugment $operator \$operand}]
         return \$toAugment
      }]
   }
   namespace export {[a-z]*}
}


