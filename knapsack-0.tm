#!/usr/bin/env tclsh

# Load the dynamic library providing the extension
namespace eval loadLibraryTmp {

   variable sharedLib {}
   variable name knapsack.tclext[info sharedlibextension]
   variable searchIn [linsert $::env(TCLLIBPATH) 0 [file dirname [info script]]]

   foreach dir $::loadLibraryTmp::searchIn {
      if {[file exists [file join $dir $::loadLibraryTmp::name]]} {
         set ::loadLibraryTmp::sharedLib [file join $dir $::loadLibraryTmp::name]
         break
      }
   }

   if {[string equal $::loadLibraryTmp::sharedLib {}]} {
      throw {NO SHARED LIB} "The library \"$::loadLibraryTmp::name\" has not been found in \"$::loadLibraryTmp::searchIn\"!"
   }

   uplevel #0 load $::loadLibraryTmp::sharedLib
}
namespace delete loadLibraryTmp

namespace eval knapsack {

   proc knapsack {weights values weightLimit {errorMargin 0} {sort 1}} {
      set taken [::knapsack::KnapsackC++ $weights $values $weightLimit $errorMargin]
      if {$sort} {
         set taken [lsort -integer -increasing $taken]
      }
      return $taken
   }

   namespace export {[a-z]*}
}

