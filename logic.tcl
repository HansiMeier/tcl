#!/usr/bin/env tclsh

package require functional
package require Tcl 8.6- ;# for %b
package require logic

####################################################################################################
#                                           STRING COUNT                                           #
####################################################################################################

# Add the count procedure to the string ensemble command
set stringNamespace [namespace ensemble configure string -namespace]

namespace eval $stringNamespace {
   proc count {string countString} {
      return [expr {([string length $string] \
                    -[string length [string map [list $countString ""] $string]]) \
                    /[string length $countString]}]
   }

   namespace export count
}

namespace ensemble configure string -subcommands [
   lmap  [info commands ${stringNamespace}::*] {namespace tail}
]


####################################################################################################
#                                            DICT INCR                                             #
####################################################################################################

set dictNamespace [namespace ensemble configure dict -namespace]

namespace eval $dictNamespace {
   proc incr {dictVar keyVar {increment 1}} {
      upvar 1 $dictVar d
      if {! [dict exists $d $keyVar]} {
         return [dict set d $keyVar $increment]
      }
      
      return [dict set d $keyVar [expr {[dict get $d $keyVar] + $increment}]]
   }

   namespace export incr
}

namespace ensemble configure dict -subcommands [
   map {namespace tail} [
      info commands ${dictNamespace}::*
   ]
]


####################################################################################################
#                                            ALGORITHM                                             #
####################################################################################################

proc getAppearances dnf {
   set mult [dict create]
   set len [llength $dnf]

   for {set i 0} {$i != $len} {incr i 1} {
      set conj [lindex $dnf $i]

      foreach literal $conj {
         dict lappend mult $literal $i
      }
   }

   return $mult
}

proc countNeg minTerm {
   return [string count $minTerm -]
}
