#!/usr/bin/env tclsh

package require Tcl 8.6-
package require fileutil

namespace eval debugLog {
   variable indentLevel -1
   variable indent ""
   variable procsToReplaceWithLoggingProcs [list for foreach while if]
   variable loggingProcs  [lmap proc $procsToReplaceWithLoggingProcs {return -level 0 "::debugLog::Logging[string totitle ${proc}]"}]


   variable procCommand ::proc


   variable procsNotToLog $loggingProcs

   proc IncrIndent {amount} {
      variable indent
      variable indentLevel
      incr indentLevel [expr {$amount*3}]
      set indent [string repeat { } $indentLevel]
   }



####################################################################################################
#                                         PUBLIC FUNCTIONS                                         #
####################################################################################################



   proc setUpLogging {
      loggingEnabled
      {logFileName defaultLogFileName}
      {scriptName defaultScriptName}
      {procsToLog logAllProcsDefinedAfterThisCommand}} {
      variable logFile
      variable logFd
      variable procCommand

      if {!$loggingEnabled} {
         return
      }

      if {[string equal -nocase $scriptName defaultScriptName]} {
          set scriptName [uplevel 1 {info script}]
      }

      set scriptName [file normalize $scriptName]
      set scriptDir [file dirname $scriptName]
      set scriptBasename [file rootname [file tail $scriptName]]
      if {[string equal -nocase $logFileName defaultLogFileName]} {
         set logFileName [file join $scriptDir "$scriptBasename.debug.log"]
      }


      try {
         set logFd [open $logFileName a]
      } on error err {
         set logFd [open $logFileName w]
      }

      set timestamp [clock format [clock seconds]]
      puts $logFd "####### DebugLog of file $scriptName on $timestamp #######"

      if {[string equal -nocase $procsToLog logAllProcsDefinedAfterThisCommand]} {
         rename $procCommand ::debugLog::theRealProc
         set procCommand ::debugLog::theRealProc
         {*}$procCommand ::proc args {
            tailcall ::debugLog::defineAndLogProc {*}$args
         }
      } else {
         tailcall logTheseProcs {*}$procsToLog
      }
   }

   proc defineAndLogProc {name arguments body} {
      set namespaceOfCaller [uplevel 1 [list namespace current]]
      ::debugLog::theRealProc ${namespaceOfCaller}::$name $arguments $body
      tailcall logTheseProcs $name
   }

   proc getProcDefinition name {
      set name [uplevel 1 [list namespace which -command $name]]

      if {[info procs $name] eq ""} {
         throw {NO PROC} "\"$name\" is not a proc!"
      }

      set arguments [info args $name]
      set argDescription {}
      foreach arg $arguments {
         if {[info default $name $arg default]} {
            append argDescription "{$arg {$default}} "
         } else {
            append argDescription "$arg "
         }
      }
      set argDescription [string trimright $argDescription]
      set body [info body $name]
      return "proc {$name} {$argDescription} {$body}"
   }


   proc logTheseProcs {args} {

      foreach name $args {
         set arguments [uplevel 1 info args $name]
         set body [string trim [uplevel 1 info body $name]]
         uplevel 1 [list ::debugLog::loggingProc $name $arguments $body]
      }
   }

   proc loggingProc {name arguments body} {
      variable procCommand
      variable loggingProcs
      set commands [GetCommands $body]
      uplevel 1 [list {*}$procCommand $name $arguments "
         variable ::debugLog::indent
         puts \$::debugLog::logFd \"\n\$indent------- $name {$arguments} -------\"
         ::debugLog::IncrIndent 1
         foreach arg {$arguments} {
            puts \$::debugLog::logFd \"\$indent~~ \$arg = \[set \[lindex \$arg 0\]\]\"
         }
         ::debugLog::StepCommands \[list $commands\]"
      ]

      if {[string index $name 0] eq ":"} {
         set fullyQualifiedName $name
      } else {
         set namespaceOfCaller [uplevel 1 [list namespace current]]
         set fullyQualifiedName ${namespaceOfCaller}::${name}
      }
      lappend loggingProcs $fullyQualifiedName
   }




####################################################################################################
#                                           MAIN THINGS                                            #
####################################################################################################

   proc GetCommands body {
      set toAdd ""
      set commands {}
      foreach line [split $body \n] {
         append toAdd $line\n
         if {[info complete $toAdd]} {
            set toAppend [string trim $toAdd]
            if {$toAppend ne ""} {
               foreach proc $::debugLog::procsToReplaceWithLoggingProcs {
                  if {[string match ${proc}* $toAppend]} {
                     set toAppend "::debugLog::Logging[string totitle $proc] [string range $toAppend [expr {[string length $proc] + 1}] end]"
                     break
                  }
               }
               lappend commands $toAppend
            }
            set toAdd {}
         }
      }
      return $commands
   }

   proc StepCommands {commands {implicitReturn 1}} {
      variable ::debugLog::indent
      variable ::debugLog::doNotLog
      set no 0

      foreach line $commands {
         trace remove execution return enter ::debugLog::ReturnHandler
         trace remove execution tailcall enter ::debugLog::ReturnHandler
         trace add execution return enter ::debugLog::ReturnHandler
         trace add execution tailcall enter ::debugLog::ReturnHandler
         set doNotLog 0

         foreach proc $::debugLog::procsNotToLog {
            if {[string match *${proc}* $line]} {
               set doNotLog 1
               break
            }
         }

         incr no 1
         if {! $doNotLog} {
            puts -nonewline $::debugLog::logFd "$indent$no $line"
         } else {
            puts -nonewline $::debugLog::logFd "$indent$no "
         }
         try {
            set retVal [uplevel 1 $line]
            if {! $doNotLog} {
               puts $::debugLog::logFd " --> {$retVal}"
            }
         } on error evalErr {
            puts $::debugLog::logFd "\n$indent$no Error $evalErr on line $line"
            trace remove execution return enter ::debugLog::ReturnHandler
            trace remove execution tailcall enter ::debugLog::ReturnHandler
            throw {error eval} "Error $evalErr on line $line"

         }
      }

      if {$implicitReturn && !$doNotLog} {
         ::debugLog::OutdentHandler
         puts $::debugLog::logFd "$indent------- implicit return {$retVal} -------"
      }

      trace remove execution return enter ::debugLog::ReturnHandler
      trace remove execution tailcall enter ::debugLog::ReturnHandler
      return $retVal
   }


####################################################################################################
#                              LOGGING VERSIONS OF CONTROL STRUCTURES                              #
####################################################################################################

   proc LoggingIf {args} {
      variable ::debugLog::indent
      ::debugLog::IncrIndent 1
      set condition [lindex $args 0]
      puts $::debugLog::logFd "\n$indent------- if {[string trim $condition]} -------"
      if {[uplevel 1 [list expr $condition]]} {
         puts $::debugLog::logFd "${indent}~~ branch \"if {[string trim $condition]}\""
         set commands [::debugLog::GetCommands [string trim [lindex $args 1]]]
         return [uplevel 1 [list ::debugLog::StepCommands $commands]]
      }

      set len [llength $args]
      for {set i 2} {$i < $len} {incr i 1} {
         switch -exact -- [lindex $args $i] {
            else {
               set elseCommands [::debugLog::GetCommands [string trim [lindex $args [incr i 1]]]]
               puts $::debugLog::logFd "${indent}~~ branch \"else\""
               set result [uplevel 1 [list ::debugLog::StepCommands $elseCommands]]
               puts $::debugLog::logFd {}
               return $result

            }
            elseif {
               set elseIfCondition [lindex $args [incr i 1]]
               if $elseIfCondition {
                  puts $::debugLog::logFd "${indent}~~ branch \"elseif {[string trim $elseIfCondition]}\""
                  set elseIfCommands [::debugLog::GetCommands [string trim [lindex $args [incr i 1]]]]
                  set result [uplevel 1 [list ::debugLog::StepCommands $elseIfCommands 0]]
                  puts $::debugLog::logFd {}
                  return $result
               }
            }
         }
      }

      return
   }

   proc LoggingForeach {args} {
      variable ::debugLog::indent
      ::debugLog::IncrIndent 1

      set len [llength $args]
      set lastInd [expr {$len - 1}]
      set body [lindex $args end]
      set commands [::debugLog::GetCommands [string trim $body]]
      set otherArgs [lrange $args 0 end-1]

      set allVars [list]
      for {set i 0} {$i < $lastInd} {incr i 2} {
         set varList [lindex $args $i]
         set allVars [list {*}$allVars {*}$varList]
      }


      puts $::debugLog::logFd "\n$indent------- foreach [string trim $otherArgs] -------"
      set doubleAllVars [list]
      foreach var $allVars {
         lappend doubleAllVars $var
         lappend doubleAllVars $var
      }
      upvar 1 {*}$doubleAllVars


      foreach {*}$otherArgs {
         foreach var $allVars {
            puts $::debugLog::logFd "${indent}~~ $var = [set $var]"
         }
         uplevel 1 [list ::debugLog::StepCommands $commands 0]
         puts $::debugLog::logFd {}
      }

      return
   }

   proc LoggingFor {init condition step body} {
      variable ::debugLog::indent
      ::debugLog::IncrIndent 1
      puts $::debugLog::logFd "\n$indent------- for {$init} {$condition} {$step} -------"
      set putLoopVar 0
      if {[string first set $init] == 0} {
         # traditional for loop
         set loopVar [lindex $init 1]
         set putLoopVar 1
         upvar 1 $loopVar $loopVar
      }

      set commands [::debugLog::GetCommands $body]

      set forLoop {for {uplevel 1 $init} {[uplevel 1 [list expr $condition]]} {uplevel 1 $step}}

      if {$putLoopVar} {
         {*}$forLoop {
            puts $::debugLog::logFd "$indent~~ $loopVar = [set $loopVar]"
            uplevel 1 [list ::debugLog::StepCommands $commands 0]
         }
      } else {
         {*}$forLoop {
            uplevel 1 [list ::debugLog::StepCommands $commands 0]
         }
      }

      return
   }

   proc LoggingWhile {condition body} {
      variable ::debugLog::indent
      ::debugLog::IncrIndent 1
      puts $::debugLog::logFd "\n$indent------- while {[string trim $condition]} -------"
      set count 0
      set condition [list uplevel 1 [list expr $condition]]
      set commands [::debugLog::GetCommands [string trim $body]]
      while {[{*}$condition]} {
         puts $::debugLog::logFd "$indent~~ times = [incr count 1]"
         uplevel 1 [list ::debugLog::StepCommands $commands 0]
         puts $::debugLog::logFd {}
      }
      return
   }


####################################################################################################
#                                             HANDLERS                                             #
####################################################################################################

   proc OutdentHandler args {
      ::debugLog::IncrIndent -1
   }

   foreach proc $loggingProcs {
      trace add execution $proc leave ::debugLog::OutdentHandler
   }



   proc ReturnHandler {commandString op} {
      trace remove execution return enter ::debugLog::ReturnHandler
      trace remove execution tailcall enter ::debugLog::ReturnHandler
      variable ::debugLog::doNotLog

      set distanceToTop [info level]
      for {set i 1} {$i < $distanceToTop} {incr i} {
         set callerlevel [expr {$distanceToTop - $i}]
         puts "CALLER $callerlevel: [info level $callerlevel]"
      }


      ::debugLog::OutdentHandler
      if {! $doNotLog} {
         puts $::debugLog::logFd "\n$::debugLog::indent------- $commandString -------"
      } else {
         puts $::debugLog::logFd {}
      }
      set doNotLog 1
   }

   proc ExitHandler {commandString op} {
      puts $::debugLog::logFd {}
      catch {chan close $::debugLog::logFd}
   }

   trace add execution exit enter ::debugLog::ExitHandler


   namespace export {[a-z]*}
} ;# namespace finished
