#!/usr/bin/env tclsh
package require http

set UPDATEFILE "$::env(HOME)/Dropbox/.sinfestLast"
set BASEURL {http://www.sinfest.net/btphp/comics}
set LINUX_VIEWER {eom} ;# needed because xdg-open works strangely on Linux (at least mine)
set TODAY [clock format [clock seconds] -format "%Y-%m-%d"]

proc downloadToFile {date file} {
   global TODAY
   while {[string compare $date $TODAY] <= 0 || [reachedTomorrow $date]} {
      set url [getComicUrl $date]
      set token [::http::geturl $url]
      if {[::http::ncode $token] == 200} {
         break
      }
      puts "No comic for date \"$date\""
      set date [getNextDate $date]
   }


   set fd [open $file w]
   chan configure $fd -translation binary
   puts -nonewline $fd [::http::data $token]
   close $fd
   ::http::cleanup $token
   return $date
}

proc reachedTomorrow date {
   global TODAY
   puts "The date $date is in the future (today is $TODAY), exiting."
   exit 0
}

proc which {prog} {
   global env
   foreach {dir} [split $::env(PATH) {:}] {
      if {[file executable "$dir/$prog"]} {
         return "$dir/$prog"
      }
   }
   throw {NOT_IN_PATH} "\"$prog\" not in \$PATH"
}

proc openWith {file} {
   global OPENPROG
   global LINUX_VIEWER
   if {! [info exists OPENPROG]} {
      if {[catch {set OPENPROG [which $LINUX_VIEWER]}] && [catch {set OPENPROG [which "open"]}]} {
         error "No suitable open program found!"
      }
   }
   puts "Opening $file with $OPENPROG"
   exec $OPENPROG $file &
   return
}

proc killViewerOnLinux {} {
   global LINUX_VIEWER
   global tcl_platform
   if {! [string equal $tcl_platform(os) Linux]} {
      return
   }
   puts "We're on Linux, killing $LINUX_VIEWER if running!"
   catch {exec killall $LINUX_VIEWER}
   return
}

proc closeXeeWindows {} {
   global tcl_platform
   if {! [string equal $tcl_platform(os) Darwin]} {
      return
   }
   # No space after Xee³ because iTerm already outputs one (bug?)
   puts stderr {We're on OSX, use ugly GUI AppleScript to close windows of Xee³if any are open.}

   exec osascript -e {
      if application "Xee³" is running then
         tell application "System Events"
            set allWindows to name of window of process "Xee³"
            repeat with i in allWindows
               click button 1 of window i of process "Xee³"
            end repeat
         end tell
      end if
   } >& /dev/null
   return
}

proc getNextDate {{date {}}} {
   global UPDATEFILE
   global TODAY

   set dateLen [llength $date]
   if {$dateLen || [file readable $UPDATEFILE]} {

      if {$dateLen} {
         set line $date
      } else {
         set fd [open $UPDATEFILE]
         set line [gets $fd]
         close $fd
      }

      if {! [regexp {([0-9]{4})-([0-9]{2})-([0-9]{2})} $line _ year month day] \
            || [catch {set then [clock scan "$year-$month-$day" -format %Y-%m-%d]}]} {
               puts stderr "No valid date in string \"$date\" recognized, returning today"
               return $TODAY
            }
      return [clock format [clock add $then 1 day] -format "%Y-%m-%d"]
   }
   puts stderr "No valid date in \"$UPDATEFILE\" recognized, returning today"
   return $TODAY
}

proc writeDate {date} {
   global UPDATEFILE
   if {[catch {set fp [open $UPDATEFILE w]}]} {
      error "Could not open \"$UPDATEFILE\" for writing!"
   }
   puts $fp $date
   close $fp
   return
}

proc getComicUrl date {
   global BASEURL
   puts "Downloading strip of $date"
   set url "$BASEURL/$date.gif"
   puts "URL: $url"
   return $url
}

proc main {} {
   set stripFile "/tmp/sinfest.gif"
   set realDate [downloadToFile [getNextDate] $stripFile]
   closeXeeWindows
   killViewerOnLinux
   openWith $stripFile
   writeDate $realDate
   exit 0
   return
}

main

