#!/usr/bin/env tclsh

package require debugLog
::debugLog::setUpLogging yes

::debugLog::defineAndLogProc there {} {
    puts f
    if {2 > 1} {
        puts ff
    }
    return
}

proc hello {} {
    puts hi
    set x 1
    incr x
    there
}

hello
