#!/usr/bin/env tclsh

package require Tcl 8.6-


namespace eval misc {
   namespace eval Tmp {
      variable VarCounter [expr {wide(-1)}]
      variable ProcCounter $VarCounter
      variable DefaultLoopVar 0
   }

   proc escape string {
      # do not simply put braces around $string, because if $string contains unmatched braces it doesn't work
      # string map substitutes backslashes inside the char map
      set string [string map {\x7b \x5c\x7b \x7d \x5c\x7d \x5c \x5c\x5c \x5b \x5c\x5b \x5d \x5c\x5d \x22 \x5c\x22} $string]
      return \"$string\"
   }

   # like uplevel but for a single command, without double substitution
   # also the level is mandatory
   proc upcmd {level args} {
      tailcall uplevel $level $args
   }
      
   proc varIdentifier var {
      set var [uplevel 1 [list namespace which -variable $var]]
      if {$var eq ""} {
         throw {var not found} "Variable \"$var\" not found!"
      }
      tailcall string map {" " ' \" ' \[ -* \] *- \{ -^ \} ^- : _} $var
   }

   proc makeVarList args {
      if {[llength $args] == 0} {
         tailcall list
      } else {
         set result [list]
         foreach var $args {
            lappend result "\${${var}}"
         }
         return $result
      }
   }


   proc getArgsAndDefaults proc {
      set proc [::misc::normalizeProcName $proc]
      set arguments [info args $proc]
      set fullList [list]
      foreach arg $arguments {
         if {[info default $proc $arg default]} {
            lappend fullList [list $arg $default]
         } else {
            lappend fullList $arg
         }
      }
      return $fullList
   }

   proc getBody proc {
      set proc [::misc::normalizeProcName $proc]
      tailcall info body $proc
   }

   proc getMyName {} {
      return [uplevel 1 {::misc::normalizeProcName [lindex [info level 0] 0]}]
   }

   proc getCallersName {} {
      return [uplevel 2 {::misc::normalizeProcName [lindex [info level 0] 0]}]
   }

   proc wrongNumArgs {{proc ""} {arguments ""}} {
      if {$proc eq ""} {
         set proc [::misc::getCallersName]
      }

      if {$arguments == ""} {
         set arguments [::misc::getArgsAndDefaults $proc]
      }
      tailcall throw {wrong # args} "wrong # args: should be \"$proc $arguments\""
   }

   proc cmdIdentifier cmd {
      set cmd [uplevel 1 [list namespace which -command $cmd]]
      if {$cmd eq ""} {
         throw {cmd not found} "Command \"$cmd\" not found!"
      }
      tailcall string map {" " ' \" ' \[ -* \] *- \{ -^ \} ^- : _} $cmd
   }

   proc lreplaceInplace {listVar from to args} {
      upvar 1 $listVar list
      set list [lreplace $list $from $to {*}$args[set list {}]]
      return $list
   }

   proc genVarName {} {
      return ::misc::Tmp::Var[incr ::misc::Tmp::VarCounter 1]
   }

   proc genProcName {} {
      return ::misc::Tmp::Proc[incr ::misc::Tmp::ProcCounter 1]
   }

   proc dereferenceAlias {procName {nocase 0}} {
      set derefAlias ""
      if {$nocase} {
         foreach alias [interp aliases] {
            if {[string equal -nocase $alias $procName]} {
               set derefAlias [lindex [interp alias {} $alias] 0]
               break
            }
         }
      } else {
         set derefAlias [lindex [interp alias {} $procName] 0]
      }

      if {$derefAlias eq ""} {
         return $procName
      }

      tailcall dereferenceAlias $derefAlias $nocase
   }

   proc normalizeProcName {procName} {
      set procName [dereferenceAlias $procName]
      set result [uplevel 1 [list namespace which -command $procName]]
      if {$result eq ""} {
         throw {no such name} "No proc or alias named \"$procName\"!"
      }
      return $result
   }

   proc unsetAll args {
      set tmpVar [::misc::genVarName]
      uplevel 1 [subst -nocommands -nobackslashes {
         foreach $tmpVar [list $args] {
            unset [set $tmpVar]
         }
         unset $tmpVar
      }]
   }

   proc gentleLassign {values args} {
      set numOfValues [llength $values]
      set args [lrange $args[set args {}] 0 [expr {$numOfValues - 1}]]
      tailcall lassign $values {*}$args
   }


   proc doTimes {varSpec body} {
      switch -exact -- [llength $varSpec] {
         1 {
            set loopRange [lindex $varSpec 0]
            set loopVar ::misc::Tmp::DefaultLoopVar
         }

         2 {
            set loopVar [lindex $varSpec 0]
            set loopRange [lindex $varSpec 1]
         }

         default {
            ::misc::wrongNumArgs
         }
      }

      tailcall \
         for [list set $loopVar 0] [subst {\$${loopVar} < $loopRange}] [list incr $loopVar 1] \
            $body
   }


   proc do {body whileOrUntil condition} {
      switch -exact -- $whileOrUntil {
         while {
            set maybeNegation !
         }
         until {
            set maybeNegation {}
         }
         default {
            throw {unknown option} "Unknown option \"$whileOrUntil\" to a \"do\" loop!"
         }
      }

      append body "\nif {${maybeNegation}(${condition})} break"
      tailcall while 1 $body
   }

   proc until {condition body} {
      tailcall while !($condition) $body
   }


   namespace export {[a-z]*}
}


