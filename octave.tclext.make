UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	SHAREDLIBSUFFIX := dylib
else
	SHAREDLIBSUFFIX := so
endif

ifdef DEBUG
	EXT := debug.$(SHAREDLIBSUFFIX)
	DEBUGFLAGS := -ggdb
else
	EXT := $(SHAREDLIBSUFFIX)
endif

LDFLAGS := -L$(TCLLIB) -ltclstub$(TCLVERSION) -L$(shell pkg-config octave --libs) -loctinterp
INCLUDES := $(shell pkg-config octave --cflags) -I$(TCLINCLUDE) -I$(CPPTCLINCLUDE)


CPPSTD := -std=c++14
CPPFLAGS := -Os -shared -fPIC -DUSE_TCL_STUBS -Wl,-rpath,$(OCTAVELIBDIR) -Wall

ifndef CPPC
	CPPC := g++
endif


octave: octave.tclext.cpp
	$(CPPC) $(CPPSTD) $(DEBUGFLAGS) $(CPPFLAGS) $(ADDTL_CPPFLAGS) $(INCLUDES) $(ADDTL_INCLUDES) $< $(CPPTCLINCLUDE)/cpptcl.cc -o octave.tclext.$(EXT) $(LDFLAGS) $(ADDTL_LDFLAGS)
