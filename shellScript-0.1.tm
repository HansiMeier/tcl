#!/usr/bin/env tclsh

package require Tcl 8.6-

namespace eval shellScript {
    proc requirePrograms {args} {
        foreach prog $args {
            if {[auto_execok $prog] eq ""} {
                throw {REQUIRED_PROGRAM_NOT_FOUND} "Required program \"$prog\" not found!"
            }
        }
    }

    namespace export {[a-z]*}
}


