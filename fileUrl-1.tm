#!/usr/bin/env tclsh

package require Tcl 8.5

namespace eval fileUrl {
   namespace import ::tcl::mathop::+

   proc signaliseError {type message {noComplain 0}} {
      if {$noComplain} {
         # act as if nothing happened :)
         return -level 2 -code ok {}
      } else {
         throw $type $message
      }
   }

   proc percentEncode str {
      set uStr [encoding convertto utf-8 $str]
      set toReplace {[^-A-Za-z0-9._~\n\t]}; # Newline is a special case
      set replacementGenCode {[format %%%02X [scan {\0} %c]]}
      return [string map {\n %0A} [subst [regsub -all $toReplace $uStr $replacementGenCode]]]
   }

   proc getUtf8HexBytes str {
      set uStr [encoding convertto utf-8 $str]
      set chars [split $uStr ""]
      return [lmap char $chars {
         set char [scan $char %c]
         format %02X $char
      }]
   }

   # percentDecode ?-nocomplain? string
   # if -nocomplain is given, don't throw errors, instead return the empty string
   proc percentDecode args {

      set argsLen [llength $args]
      if {$argsLen < 1 || $argsLen > 2} {
         throw {WRONG NUMBER OF ARGS} {Wrong # args: should be "percentDecode ?-nocomplain? string"}
      }

      set noComplain [expr {[string first [string tolower [lindex $args 0]] -nocomplain]} == 0]
      set string [lindex $args $noComplain]
      set stringLen [string length $string]
      set result {}

      for {set i 0} {$i < $stringLen} {incr i} {
         set char [string index $string $i]

         # If not percent encoded, just add normal char
         if {! [string equal $char %]} {
            append result $char
            continue
         }

         # read first byte
         set bytesToConvert [string range $string [+ $i 1] [+ $i 2]]

         # How many bytes follow?
         # loops == 1 => no byte follows
         # loops == 2 => 1 byte follows
         # etc.
         if "0x$bytesToConvert <= 0x7F" {
            set loops 1
         } elseif "0xC2 <= 0x$bytesToConvert && 0x$bytesToConvert <= 0xDF" {
            set loops 2
         } elseif "0xE0 <= 0x$bytesToConvert && 0x$bytesToConvert <= 0xEF" {
            set loops 3
         } elseif  "0xF0 <= 0x$bytesToConvert && 0x$bytesToConvert <= 0xF4" {
            set loops 4
         } else {
            signaliseError {ILLEGAL bytesToConvert} "Byte \"$bytesToConvert\" at index [+ $i 1] is no legal sequence start!" $noComplain
         }

         # Add bytes
         for {set l 1} {$l < $loops} {incr l} {
            if {! [string equal [string index $string [set nextByteStart [expr {$i + 3*$l}]]] %]} {
                  signaliseError {PERCENT MISSING} "Missing per cent sign! Encountered \"[string index $string $nextByteStart]\" instead of \"%\" at index $nextByteStart!" $noComplain
               }
            set byteToAppend [string range $string [+ $nextByteStart 1] [+ $nextByteStart 2]]

            if "!(0x80 <= 0x$byteToAppend && 0x$byteToAppend <= 0xBF)" {
               signaliseError {NO FOLLOWER BYTE} "Byte \"$byteToAppend\" at index [+ $nextByteStart 1] is not a \"follower\" byte!" $noComplain
            }

            append bytesToConvert $byteToAppend
         }

         # Move index to last char of last byte
         incr i [expr 3*$loops - 1]


         # Convert bytes to char in current locale
         append result [encoding convertfrom utf-8 [binary decode hex $bytesToConvert]]
      }
      return $result
   }

   # identity function
   proc id {thing} {
      return $thing
   }

   # Generate *PathToFileUrl and fileUrlTo*Path commands
   foreach pair {{unix /} {windows \\\\}} {
      proc [lindex $pair 0]PathToFileUrl path "
      [if {[string equal [lindex $pair 0] windows]} {
         id "set path \[split \$path [lindex $pair 1]]"
      } else {
         id "set path \[lrange \[split \$path [lindex $pair 1]] 1 end]"
      }]
         set result file://
         foreach component \$path {
            append result /\[percentEncode \$component]
         }

         return \$result
      "
      proc fileUrlTo[string totitle [lindex $pair 0]]Path args "
         set noComplain \[string equal -nocase \[lindex \$args 0] -nocomplain]
         set fileUrl \[lindex \$args \$noComplain]

         if {!\[regexp {^file:///(.*)} \$fileUrl _ components]} {
            signaliseError {BAD FILEURL} {Bad fileUrl!} \$noComplain
         }
         set result {}
         set components \[split \$components /]
         if {\$noComplain} {
            set noComplain -nocomplain
         } else {
            set noComplain {}
         }
         foreach component \$components {
            append result [lindex $pair 1]\[percentDecode {*}\$noComplain \$component]
         }
         [if {[string equal [lindex $pair 0] windows]} {
            id {return [string range $result 1 end]}
         } else {
            id {return $result}
         }]
      "
      # Create alias procs fileUrlToPath and pathToFileUrl
      set actualProcName [string tolower $::tcl_platform(platform)]PathToFileUrl
      proc pathToFileUrl [info args $actualProcName] [info body $actualProcName]

      set actualProcName fileUrlTo[string totitle $::tcl_platform(platform)]Path
      proc fileUrlToPath [info args $actualProcName] [info body $actualProcName]

   }

   namespace export fileUrlTo* *PathToFileUrl percent*
}
