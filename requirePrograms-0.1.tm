#!/usr/bin/env tclsh

package require Tcl 8.6-

proc requirePrograms {args} {
    set programsNotPresent [list]
    foreach prog $args {
        if {[auto_execok $prog] eq ""} {
            lappend programsNotPresent $prog
        }
    }

    if {[llength $programsNotPresent] != 0} {
        error "Required programs [join $programsNotPresent ", "] not found!"
    }

}
