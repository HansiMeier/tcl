#!/usr/local/bin/tclsh

####################################################################################################
#                                             PREAMBLE                                             #
####################################################################################################

package require Tcl 8.6- ;# for the {*} and other things
package require http ;# to download anything
package require inifile ;# to read the configuration file
package require parseGnuOpts ;# to parse GNU-style command line options
package require tls ;# to use https
package require osxGlob; # using this cumbersome glob to work around OSX's decomposed unicode, which tcl can't handle

::http::register https 443 tls::socket ;# enable https


# drop-in replacement for throw
proc signaliseError {ignore text} {
   global LogFd

   initialiseLog

   stdErrLog $text 1 ;# the 1 is to always display the message

   chan flush stderr
   if {[llength $LogFd]} {
      chan close $LogFd
   }

   exit 5
}

####################################################################################################
#                                        DICT PRETTYPRINTER                                        #
####################################################################################################

# convert dictionary value dict into string 
# hereby insert newlines and spaces to make 
# a nicely formatted ascii output 
# The output is a valid dict and can be read/used 
# just like the original dict 
############################# 


proc dict_format {dict} { 
   dictformat_rec $dict "" "   " 
} 


proc isdict {v} { 
   string match "value is a dict *" [::tcl::unsupported::representation $v] 
} 


## helper function - do the real work recursively 
# use accumulator for indentation 
proc dictformat_rec {dict indent indentstring} {
   # unpack this dimension 
   dict for {key value} $dict { 
      if {[isdict $value]} { 
         append result "$indent$key:\n" 
         append result "[dictformat_rec $value "$indentstring$indent" $indentstring]" 
         append result "$indent\n" 
      } else { 
         append result "$indent$key = \"$value\"\n" 
      }
   }

   return $result 
}

####################################################################################################
#                                         GLOBAL VARIABLES                                         #
####################################################################################################

set ProgName [regsub -nocase {\.tcl$} [file tail [info script]] {}]

# Adapt ProgDir if running in a starkit
if {! [info exists ::starkit::mode]} {
   set ProgDir [file dirname [info script]]
} else {
   # We are in a starkit
   set ProgDir [file dirname $::starkit::topdir]
}


if {[string equal -nocase $tcl_platform(platform) unix]} {
   set Home $::env(HOME)

   if {[info exists ::env(TMPDIR)]} {
      set TmpDir $::env(TMPDIR)
   } else {
      set TmpDir /tmp
   }

   if {[string equal -nocase $tcl_platform(os) Darwin]} {
      set OpenCommand [auto_execok open]
   } else {
      set OpenCommand [auto_execok xdg-open]
   }
   set HiddenChar .
} else {
   # We are on windows
   set Home $::env(USERPROFILE)
   set HiddenChar {}
   set TmpDir $::env(TMP)
   set OpenCommand [auto_execok start]
}

set Series [dict create]
set LogFile {}
set LogFd {}
set IniFile ""


####################################################################################################
#                                        UTILITY FUNCTIONS                                         #
####################################################################################################

# The wonderful identity
interp alias {} id {} return -level 0

# debug output
proc debugOutput {} {
   global tcl_platform

   ::parseGnuOpts::importOptArrays
   if {! $debug(present)} {
      return
   }

   set globalVarsToDisplay {ProgName ProgDir Home TmpDir HiddenChar OpenCommand LogFile IniFile}

   stdErrLog \nDEBUG\n

   foreach globalVar $globalVarsToDisplay {
      global $globalVar
      stdErrLog "$globalVar = \"[set $globalVar]\""
   }

   global Series
   stdErrLog "Series = \n[dict_format $Series]\n"
   stdErrLog "os = \"$tcl_platform(os)\"\nplatform = \"$tcl_platform(platform)\"\n\n\nOUTPUT\n"

   return
}

# Like 'puts', but outputs the text to the log
# and to stderr
# At this time, the global log must be initialized
proc stdErrLog {msg {alwaysPuts 0}} {
   global LogFd
   ::parseGnuOpts::importOptArrays
   if {$alwaysPuts || $verbose(present)} {
      puts stderr $msg
   }

   if {[llength $LogFd]} {
      puts $LogFd $msg
   }
   return
}

# Initialise the log using a nice timestamp
proc initialiseLog {} {
   global LogFd \
          LogFile \
          ProgName

   if {! [llength $LogFile]} {
      return
   }

   set LogFd [open $LogFile w]
   set timestamp [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S %z"]
   stdErrLog $timestamp\n

   return
}

# open a file using system provided open command
proc openFile file {
   global OpenCommand
   exec {*}$OpenCommand $file &
   return
}

proc format§ {formatStr§ args} {
   set scannedArgs [list]
   foreach arg $args {
      if {[regexp -- {^[0-9]+$} $arg]} {
         lappend scannedArgs [scan $arg %d]
      } else {
         lappend scannedArgs $arg
      }
   }

   set formatStr [string map {§ % % §} ${formatStr§}]
   set result [format $formatStr {*}$scannedArgs]
   return [string map {§ %} $result]
}

proc canonicaliseExistingFilename file {
   while {[file type $file] eq "link"} {
      set file [file readlink $file]
   }
   tailcall file join [pwd] $file
}


####################################################################################################
#                                       COMMANDLINE PARSING                                        #
####################################################################################################

proc parseCmdLine {} {
   global argv \
          argv0 \
          argc \
          IniFile \
          LogFile \
          Home \
          ProgName

   ::parseGnuOpts::processArgs \
      $argv0 \
      $argv \
      $argc \
      [list \
         [list verbose v verbose 0 0 {Be verbose, i.e. output info on stderr.}] \
         [list debug d debug 0 0 {Output diagnostic info.}] \
         [list iniFile i ini 1 0 "Inifile to use other than default \"[file join $Home .$ProgName.ini]\"."] \
         [list logFile l log 1 0 "Log to a file."]] \
      "[file tail $argv0]: Downloads new exercises, reading the settings from an ini file."

   ::parseGnuOpts::importOptArrays


   if {$logFile(present)} {
      # force string representation
      set logFile(passedArgs) [join $logFile(passedArgs) ""]
      if {[string equal [file pathtype $logFile(passedArgs)] absolute]} {
         set LogFile $logFile(passedArgs)
      } else {
         set LogFile [file join [pwd] $logFile(passedArgs)]
      }
   }

   if {$iniFile(present)} {
      # force string representation
      set iniFile(passedArgs) [join $iniFile(passedArgs) ""]

      if {! [file exists $iniFile(passedArgs)]} {
         signaliseError {BAD INI} "Requested ini file \"$iniFile(passedArgs)\" has not been found!"
      }

      try {
         set IniFile [canonicaliseExistingFilename $iniFile(passedArgs)]
      } on error err {
         stdErrLog "Error message was: $err"
         signaliseError {BAD INI} "Could not make canonical filename out of \"$iniFile(passedArgs)\"!"
      }

      set type [file type $IniFile]
      if {$type ne "file"} {
         stdErrLog "File type was: $type"
         signaliseError {BAD INI} "Requested ini file \"$iniFile(passedArgs)\" is not a normal file or a symlink to a normal file!"
      } elseif {![file readable $IniFile]} {
         signaliseError {BAD INI} "Requested ini file \"$iniFile(passedArgs)\" is not readable!"
      }
   }


   return
}

proc makeSureFileWritable path {
   set dirname [file dirname $path]
   try {
      file mkdir $dirname ;# no error if 4dirname already exists
   } on error err {
      signaliseError {NO OUTPUT DIR} "Could not make output directory \"$dirname\": $err"
   }

   try {
      file writable $dirname
   } on error err {
      signaliseError {NO WRITABLE OUTPUT DIR} "Output directory \"$dirname\" isn't writable: $err"
   }

   if {[file exists $path] && ! [file writable $path]} {
      signaliseError {NO WRITABLE FILE} "Output file \"$path\" exists, but isn't writable!"
   }
   return
}


####################################################################################################
#                                           INI READING                                            #
####################################################################################################

proc lookForIniFile {} {
   global IniFile \
          Home \
          ProgDir \
          ProgName \
          HiddenChar

   if {$IniFile ne ""} {
      # inifile already given by commandline
      return
   }

   set iniTail ${HiddenChar}${ProgName}.ini
   foreach dir [list $Home $ProgDir] {
      set IniFile [file join $dir $iniTail]
      if {[file readable $IniFile]} {
         break
      }

      if {! [file readable $IniFile]} {
         signaliseError "no readable ini file" \
            "No readable configuration file \"$iniTail\" found in \"$Home\" and \"$ProgDir\"!"
      }
   }
   return
}

# Checks that the ini file is semantically correct
proc checkIni ini {
   ::ini::commentchar {;}
   foreach section [::ini::sections $ini] {
      if {! [::ini::exists $ini $section Dir]} {
         signaliseError {Dir missing} "Missing 'Dir' key in section $section!"
      }
   }
   return
}



# read the global iniFile into a dictionary ('Series') suitable
# for this script
proc readIniToDict {} {
   global IniFile \
          Series

   lookForIniFile
   set ini [::ini::open $IniFile r]
   checkIni $ini

   foreach section [::ini::sections $ini] {
      foreach key [::ini::keys $ini $section] {
         set substKey [subst $key]
         set substValue [subst [::ini::value $ini $section $key]]
         switch -nocase -exact -- $substKey {
            Dir {
               dict set Series $section Dir $substValue
            }

            SearchDir {
               dict set Series $section SearchDir $substValue
            }

            SearchGlob {
               dict set Series $section SearchGlob $substValue
            }

            default {
               dict set Series $section Urls $substKey $substValue


               # grab extension of the filename in the url
               set ext .pdf ;# default extension
               regexp -- {\.[^/]+$} $substValue ext
               dict set Series $section Exts $substKey $ext
            }
         }
      }
   }

   ::ini::close $ini

   checkSeries
   return
}

proc checkSeries {} {
   global Series
   dict for {section _} $Series {
      if {! [dict exists $Series $section SearchDir]} {
         dict set Series $section SearchDir [dict get $Series $section Dir]
      }
   }
   return
}


####################################################################################################
#                                         "GET SOMETHINGS"                                         #
####################################################################################################

# increments the input number and pads it to two digits
proc incrPad {number {increment 1} {padding 2}} {
   scan $number %d number ;# prevent TCL from recognizing octals
   if {$number > 99} {
      signaliseError {TOO MANY SERIES} "Series number $number is over 99!"
   }
   return [format %0${padding}d [expr {$number + $increment}]]
}

# Get the number of a $prefix-file of subject $subject which should be downloaded
proc getNewNumber {subject prefix {padding 2}}  {
   global Series
   if {! [dict exists $Series $subject ${prefix}Num]} {
      set dir [dict get $Series $subject SearchDir]
      if {[dict exists $Series $subject SearchGlob]} {
         set searchGlob [dict get $Series $subject SearchGlob]
      } else {
         set searchGlob ${prefix}*\[0-9\]*[dict get $Series $subject Exts $prefix]
      }
      set newNumber [incrPad [getLatestNumber $searchGlob $dir] 1 $padding]
      dict set Series $subject ${prefix}Num $newNumber
   } else {
      set num [dict get $Series $subject ${prefix}Num]
      scan $num %d num
      set newNumber [format %0${padding}d $num]
   }

   if {$padding == 1} {
      stdErrLog "$subject, $prefix: Checking number $newNumber"
   }
   return $newNumber
}

# Generate an output filename for the newly downloaded file
proc getNewName {subject prefix} {
   global Series
   set ext [dict get $Series $subject Exts $prefix]
   set newNum [getNewNumber $subject ${prefix}]
   set dirTemplate [dict get $Series $subject Dir]
   set dir [file join [pwd] [format§ $dirTemplate $newNum]]
   return [file join $dir "${prefix} ${newNum}${ext}"]
}

# Determine which new URL to download
proc getNewUrl {subject prefix} {
   global Series
   set urlTemplate [dict get $Series $subject Urls ${prefix}]
   set newNumberNoPadding [getNewNumber $subject ${prefix} 1]
   return [format§ $urlTemplate {*}[lrepeat [regexp -all § $urlTemplate] $newNumberNoPadding]]
}

# Strip the $fileName of all non-numeric characters
# try to get a number from it
proc getNumberFromFilename fileName {
   return [regsub -all {[^0-9]} [file tail $fileName] {}]
}

# Look for the greatest number in any fileName which begins with $prefix in $dir
proc getLatestNumber {searchGlob dir} {
   set sortedFiles [lsort -decreasing [glob -nocomplain -directory $dir $searchGlob]]
   if {[llength $sortedFiles] == 0} {
      return 0
   }
   return [getNumberFromFilename [lindex $sortedFiles 0]]
}

# increment the current number in prefixNum
proc incrementPrefixNum {subject prefix {increment 1}} {
   global Series
   if {![dict exists $Series $subject ${prefix}Num]} {
      signaliseError {NO SUCH PREFIXNUM} "Could not find ${prefix}Num in $subject in Series dict!"
   }
   set current [dict get $Series $subject ${prefix}Num]
   dict set Series $subject ${prefix}Num [incrPad $current $increment]
   return
}


####################################################################################################
#                                             DOWNLOAD                                             #
####################################################################################################

proc downloadNewAndOpen {subject prefix} {
   global LogFd
   set url [getNewUrl $subject ${prefix}]
   # Wait 30s, else fail

   set token [::http::geturl $url -timeout 30000]

   if {[string equal [::http::status $token] timeout]} {
      stdErrLog "$subject, $prefix: Connection for \"$url\" timed out!"
      ::http::cleanup $token
      return 0
   }

   set newName [getNewName $subject ${prefix}]

   # HTTP-Code != 200 (which is HTTP_OK)
   # means that the file couldn't be downloaded
   if {[::http::ncode $token] != 200} {
      stdErrLog "$subject, $prefix: \"[file tail $newName]\" not yet available i.e."
      stdErrLog "   \"$url\" doesn't exist."
      ::http::cleanup $token
      return 0 ;# No new series
   }

   makeSureFileWritable $newName
   set fd [open $newName w]
   chan configure $fd -translation binary
   puts $fd [::http::data $token]
   stdErrLog "$subject, $prefix: Downloaded \"$url\" to \"$newName\""

   ::http::cleanup $token
   chan close $fd
   file attributes $newName -permissions 0644
   openFile $newName

   return 1
}

proc main {} {
   global Series \
          LogFd \
          ToAdd

   parseCmdLine
   initialiseLog
   readIniToDict
   debugOutput

   dict for {subject subDict} $Series {
      dict for {prefix _} [dict get $subDict Urls] {
         if {! [downloadNewAndOpen $subject $prefix]} {
            stdErrLog "$subject, $prefix: Maybe just this file doesn't exist? Trying next."
            incrementPrefixNum $subject $prefix
            downloadNewAndOpen $subject $prefix
         }
      }
   }

   if {[llength $LogFd]} {
      chan close $LogFd
   }

   return
}

main

