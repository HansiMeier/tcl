/**************************************************************************************************
 *                                     OCTAVE <-> TCL INTEROP                                     *
 **************************************************************************************************/

#include <cpptcl/cpptcl.h>
// undefine macro so that it doesn't interfere with octave's panic function
#undef panic
#include <octave/oct.h>
#include <octave/ov.h>
#include <octave/octave.h>
#include <octave/parse.h>
#include <octave/interpreter.h>
#include <iostream>
#include <string>
#include <cstdlib>

#define declareCommand(command) \
   ooInterp.def("::octave::" #command, command)


/**************************************************************************************************
 *                                           PROTOTYPES                                           *
 **************************************************************************************************/

// octave_value --> Tcl::object
Tcl::object OctVal2TclObj(const octave_value& ov);

// octave_value_list --> Tcl::object containing a list
Tcl::object OctValList2TclList(const octave_value_list& ovl);

// Tcl::object --> octave_value
octave_value TclObj2OctVal(const Tcl::object& value);

// evaluate an octave command
// return the resulting values as tcl values
Tcl::object eval(const std::string& eval_str);

// put a numeric value or a mdl to an octave variable
void put(const std::string& name, Tcl::object value);

// get the value of an octave variable
Tcl::object get(const std::string& name);

// exit handler which tcl calls at exit.
// cleans up and exists octave
void tclExitProc(ClientData cdata);

// entry point for this module
extern "C" int Octave_Init(Tcl_Interp* interp);


/**************************************************************************************************
 *                                       VALUE CONVERSIONS                                        *
 **************************************************************************************************/

// octave_value --> Tcl::object
Tcl::object OctVal2TclObj(const octave_value& ov) {
   if (ov.is_string()) {
      return Tcl::object (ov.string_value());
   } else if (ov.is_real_matrix()) {

      Matrix matrix (ov.matrix_value());

      // make dimension list
      const dim_vector dimensions (matrix.dims());
      const octave_idx_type numberOfDimensions (dimensions.length());
      Tcl::object dimensionList;
      octave_idx_type numberOfEntries (1);
      octave_idx_type dimension;
      for (octave_idx_type i (0); i != numberOfDimensions; ++i) {
         dimension = dimensions(i);
         numberOfEntries *= dimension;
         dimensionList.append(Tcl::object (long(dimension)));
      }

      // add the dimensions to the start of the matrix description list (mdl)
      Tcl::object mdl;
      mdl.append(dimensionList);

      for (octave_idx_type i (0); i != numberOfEntries; ++i) {
         mdl.append(Tcl::object (matrix(i)));
      }

      return mdl;
   } else if (ov.is_real_type()) {
      // if possible, get an integer value
      double doubleVal (ov.double_value());
      long floorVal (doubleVal);
      if (floorVal == doubleVal) {
         return Tcl::object (floorVal);
      } else {
         return Tcl::object (doubleVal);
      }
   } else {
      // Unknown type, return an empty object
      return Tcl::object ();
   }

}

// octave_value_list --> Tcl::object containing a list
Tcl::object OctValList2TclList(const octave_value_list& ovl) {
   Tcl::object list;
   octave_idx_type length (ovl.length());
   for (octave_idx_type i (0); i != length; ++i) {
      list.append(OctVal2TclObj(ovl(i)));
   }

   return list;
}

// Tcl::object --> octave_value
octave_value TclObj2OctVal(const Tcl::object& value) {
   long length (value.size());
   if (length != 1) {
      // value must be a matrix description list
      Tcl::object dimensions (value.at(0));
      long numberOfDimensions (dimensions.size());
      // get number of elements in the mdl
      // and make a dim_vector to pass to octave later on
      long numberOfElements (1);
      dim_vector dv (dim_vector::alloc(numberOfDimensions));
      long dimSize;
      for (long i (0); i != numberOfDimensions; ++i) {
         dimSize = dimensions.at(i).get<long>();
         numberOfElements *= dimSize;
         dv.elem(i) = dimSize;
      }

      // watch out for size mismatches in the mdl
      if (numberOfDimensions != length - 1) {
         throw Tcl::tcl_error (std::string ("Size mismatch in matrix description list{")
               + std::string (Tcl_GetString(value.get_object()))
               + std::string ("}: matrix has ") + std::to_string(length - 1)
               + " entries, but should have "
               + std::to_string(numberOfElements)
               + " entries!");
      }

      Matrix m (dv);
      for (long i (0); i != numberOfElements; ++i) {
         m.elem(i) = value.at(i+1).get<double>();
      }

      return m;
   }

   // Try to get the right scalar value
   // Using the Tcl C API instead of cpptcl
   // because value.get<type> raises an error in the interpreter
   Tcl_Obj* raw (value.get_object());
   long longVal;
   double doubleVal;
   int retval (Tcl_GetDoubleFromObj(NULL, raw, &doubleVal));
   if (retval == TCL_OK) {
      retval = Tcl_GetLongFromObj(NULL, raw, &longVal);
      if (retval == TCL_OK) {
         if (double (longVal) == doubleVal) {
            octave_value ov (longVal);
            return ov;
         }
      }
      octave_value ov (doubleVal);
      return ov;
   }

   octave_value ov (Tcl_GetString(raw));
   return ov;
}


/**************************************************************************************************
 *                                       BASIC TCL COMMANDS                                       *
 **************************************************************************************************/
// there are more commands in the tm file of this module

// evaluate an octave command
// return the resulting values as tcl values
Tcl::object eval(const std::string& eval_str) {
   int parse_status;
   octave_value_list retovl (eval_string(eval_str, false, parse_status));
   if (parse_status != 0) {
      // make octave recover from the error
      reset_error_handler();
      throw Tcl::tcl_error (std::string ("Error evaluating \"") + eval_str + std::string ("\"!"));
   }
   return OctValList2TclList(retovl);
}

// put a numeric value or a mdl to an octave variable
void put(const std::string& name, Tcl::object value) {
   set_top_level_value(name, TclObj2OctVal(value));
   return;
}

// get the value of an octave variable
Tcl::object get(const std::string& name) {
   octave_value ov (get_top_level_value(name, true)); // true means silent
   if (ov.is_undefined()) {
      // make octave recover from the error
      reset_error_handler();
      throw Tcl::tcl_error (std::string ("Error getting value of ") + name);
   }
   return OctVal2TclObj(ov);
}


/**************************************************************************************************
 *                                              INIT                                              *
 **************************************************************************************************/

// exit handler which tcl calls at exit.
// cleans up and exists octave
void tclExitProc(ClientData cdata) {
   // cleanup octave gracefully
   clean_up_and_exit(0, true);
}

CPPTCL_MODULE(Octave, ooInterp) {
   Tcl::interpreter::defaultInterpreter = &ooInterp;
   Tcl_CreateExitHandler(&tclExitProc, NULL);


   // start octave with command line arguments:
   // "embedded" is important!
   char* oct_argv[] = {
         const_cast<char*>("embedded"),
         const_cast<char*>("--quiet"),
         const_cast<char*>("--no-history"),
         const_cast<char*>("--no-line-editing")
   };
   octave_main(sizeof(oct_argv)/sizeof(oct_argv[0]), oct_argv, true);
   ooInterp.eval("namespace eval octave {}");
   declareCommand(eval);
   declareCommand(get);
   declareCommand(put);
   ooInterp.eval("namespace eval octave {namespace export {[a-z]*}}");

}
