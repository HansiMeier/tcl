#!/usr/bin/env tclsh

package require Tcl 8.5
package require tclreadline

regexp {^([^\.]+)\.?} [file tail [info script]] _ basename
set HIST_FILE /dev/null

set LIST_FILE "$::env(HOME)/Dropbox/${basename}.txt"

proc readInLines {file} {
   set fd [open $file]

   # Do not trip on terminating newline
   set lines [lsearch -all -inline -not -exact [split [read $fd] \n] {}]
   close $fd
   return $lines
}

proc checkFile {file} {
   set myFile $file

   if {! [file exists $myFile]} {
      close [open $myFile w]
   }
   if {! ([file readable $myFile] && [file writable $myFile])} {
      throw {BAD FILE} "\"$file\" must be readable and writable!"
   }
}

proc addToList {listName} {
   upvar 1 $listName writeList
   puts "Please enter new thigs to add to the list, delimited by a newline."
   puts "An empty line terminates the input\n"
   while {! [string equal [set input [::tclreadline::readline read ""]] {}]} {
      lappend writeList $input
   }
   set writeList [lsort -dictionary -unique $writeList]
}

proc displayNumberedList {list} {
   set counter 0
   foreach obj $list {
      if {[string equal $obj {}]} continue
      puts "$counter – $obj"
      incr counter
   }
   return $counter
}

proc removeFromList {listName} {
   upvar $listName writeList
   while 1 {
      set length [displayNumberedList $writeList]
      set number [::tclreadline::readline read "\nPlease select a line number to delete\nAn invalid number terminates the input.\n"]
      puts ""
      if {(![string is integer $number]) || $number < 0 || $number >= $length} break
      set writeList [lreplace $writeList $number $number]
   }
}

proc writeNewList {list file} {
   set fd [open $file w]
   puts $fd [join $list \n]
   close $fd
}
   
      

proc main {historyFile listFile} {
   global argv
   global argc
   global basename

   # Initialise readline
   ::tclreadline::readline initialize $historyFile

   checkFile $listFile
   set contents [readInLines $listFile]

   switch -glob -nocase -- [string trim [lindex $argv 0] -] {
      a* {
         addToList contents
         writeNewList $contents $listFile
      }
      r* {
         removeFromList contents
         writeNewList $contents $listFile
      }
      h* {
         puts stderr "Usage: $basename \[add | rm]"
         exit 1
      }
      default {
         displayNumberedList $contents
      }
   }
}

main $HIST_FILE $LIST_FILE


