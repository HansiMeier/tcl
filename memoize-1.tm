#!/usr/bin/env tclsh
package require misc

namespace eval memoize {

   namespace eval internal {
      variable counter -1
   }

   proc memoize {intendedName lambdaAsForApply} {
      set hashmapName "::memoize::internal::[incr internal::counter]"
      set $hashmapName [dict create]
      set evaluationNamespace ::
      ::misc::gentleLassign $lambdaAsForApply arguments body evaluationNamespace
      set numberOfArguments [llength $arguments]

      set memoizedBody [subst -nocommands {
         if {[llength \$args] != $numberOfArguments} {
            puts [llength \$args]
            puts $numberOfArguments
            throw {WRONG_NUM_ARGS} "wrong # args: should be \\"$intendedName $arguments\\""
         }

         upvar 0 $hashmapName hashmap
         if {[dict exist hashmap \$args]} {
            return [dict get \$hashmap \$args]
         } else {
            set value [apply  {$lambdaAsForApply} {*}\$args]
            dict set hashmap \$args \$value
            return \$value
         }
      }]

      set memoizedLambda [list args $memoizedBody $evaluationNamespace]
      return $memoizedLambda
   }

   proc memoizedProc {name arguments body} {
      set name [namespace tail $name]
      set callersNamespace [uplevel 1 [list namespace current]]
      set memoizedLambda [::memoize::memoize $name [list $arguments $body $callersNamespace]]
      proc ${callersNamespace}::${name} args [subst -nocommands {apply {$memoizedLambda} {*}\$args}]
   }

   namespace export {[a-z]*}
}
