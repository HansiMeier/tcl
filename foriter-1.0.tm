package require critcl

critcl::ccode {
   #include <stdio.h>
}

critcl::ccommand foriter {dummy interp objc objv} {
   int result;
   /* foriter loop : from start to end-1 increment by <increment> do <body> */
   Tcl_Obj *obj_body = NULL;
   Tcl_Obj *obj_counter = NULL; 
   /* the int values of the loop range */
   int start = 0;
   int end;
   int increment = 1;
   int sign = 1;
   int isSharedObjCounter = 0;
   char errorInfo[64];

   memset(errorInfo,'\0',sizeof(errorInfo));

   if (objc<4 || objc>6) {
      Tcl_WrongNumArgs(interp, 1, objv, "varname ?start? end ?increment? body");
      return TCL_ERROR;
}


if (objc == 4) {
   result = Tcl_GetIntFromObj(interp, objv[2], &end);
   if (result != TCL_OK) {
      return result;
     }
  obj_body = objv[3];

  /* the start of the number range */
  obj_counter = Tcl_NewIntObj(0);
  if (obj_counter == NULL) {
     return TCL_ERROR;
  }
} else  {
   /*  the user provided 'start' as 2nd argument,
   and the 'end' of the range is then the 3rd argument  */

   /* the start of the number range */
   obj_counter = objv[2];

   result = Tcl_GetIntFromObj(interp, objv[2], &start);
   if (result != TCL_OK) {
      return result;
  }

  result = Tcl_GetIntFromObj(interp, objv[3], &end);
  if (result != TCL_OK) {
     return result;
  }

  if (objc == 5) {
     obj_body = objv[4];
  } else  {
     obj_body = objv[5];
  }
}

if (objc == 6) {
   result = Tcl_GetIntFromObj(interp, objv[4], &increment);
   if (result != TCL_OK) {
      return result;
  }
}

/* validation tests of the number range */
if (increment == 0) {
   /*  the increment is zero so there
    **  will be no iteration             */
    Tcl_SetObjResult(interp, Tcl_NewStringObj(
    "cannot increment by zero", -1));
    return TCL_ERROR;
}

if ((end-start) * increment < 0) {
   /*  the iterating range goes the other way
   than incrementation does                 */
   Tcl_SetObjResult(interp, Tcl_NewStringObj(
   "invalid range : an endless loop would occur", -1));
   return TCL_ERROR;
}
/* sign let us iterate upper or lower */
if (increment<0) {
   sign=-1;
}


/* the cleanest way to initialize the counter */
if ( Tcl_IsShared( obj_counter ) ) {
   obj_counter = Tcl_DuplicateObj( obj_counter );
   Tcl_IncrRefCount( obj_counter );
   isSharedObjCounter=1;
}

obj_counter = Tcl_ObjSetVar2(interp, objv[1], NULL, obj_counter, TCL_LEAVE_ERR_MSG);
if ( obj_counter == NULL ) {
   return TCL_ERROR;
}
/* end initialize the counter */


/* please note that a negative increment could be used
 ** (I wonder if one shall do so ?)
 ** now, the loop begins                                */

 while (sign*start <= sign*end) {
    /* we are into the loop */
    result = Tcl_EvalObjEx(interp, obj_body, 0);
    if ((result != TCL_OK) && (result != TCL_CONTINUE)) {
       if (result == TCL_ERROR) {
          sprintf(errorInfo, "\n    (\"foriter\" body line %d)", interp->errorLine);
          Tcl_AddErrorInfo(interp, errorInfo);
          /*  when an error occurs, we quit the loop
          and clean up things like References    */
          break;
   }
   /*  when the user breaks the evaluation
   we have to break out of the loop */
   break;
  }

  obj_counter = Tcl_ObjGetVar2(interp, objv[1], NULL, TCL_LEAVE_ERR_MSG);
  if ( obj_counter == NULL ) {
     return TCL_ERROR;
  }

  if ( Tcl_IsShared( obj_counter ) ) {
     obj_counter = Tcl_DuplicateObj( obj_counter );
     Tcl_IncrRefCount( obj_counter );
     isSharedObjCounter=1;
  }

  result = Tcl_GetIntFromObj(interp, obj_counter, &start);
  if (result != TCL_OK) {
     return result;
  }

  start+=increment;
  Tcl_SetIntObj(obj_counter, start);
  /* setting the variable */
  obj_counter = Tcl_ObjSetVar2(interp, objv[1], NULL, obj_counter, TCL_LEAVE_ERR_MSG);
  if ( obj_counter == NULL ) {
     return TCL_ERROR;
  }
}
/* ending of the loop */

if ( isSharedObjCounter ) {
   Tcl_DecrRefCount( obj_counter );
}


if (result == TCL_ERROR) {
   return result;
}

Tcl_ResetResult(interp);
return TCL_OK;
}
